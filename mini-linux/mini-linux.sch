<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.3.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="SAMTEC-FTSH-120-01-X-DV">
<packages>
<package name="SAMTEC-FTSH-120-01-X-DV">
<description>translated Allegro footprint</description>
<wire x1="-13.401" y1="-1.714" x2="-19.624" y2="-1.714" width="0" layer="150"/>
<wire x1="-18.923" y1="-0.318" x2="-18.923" y2="-1.714" width="0" layer="150"/>
<wire x1="-19.082" y1="-1.08" x2="-18.923" y2="-1.714" width="0" layer="150"/>
<wire x1="-18.923" y1="-1.714" x2="-18.764" y2="-1.08" width="0" layer="150"/>
<wire x1="-18.764" y1="-1.08" x2="-18.923" y2="-1.206" width="0" layer="150"/>
<wire x1="-18.923" y1="-1.206" x2="-19.082" y2="-1.08" width="0" layer="150"/>
<wire x1="-18.923" y1="-1.714" x2="-18.844" y2="-1.143" width="0" layer="150"/>
<wire x1="-18.844" y1="-1.143" x2="-18.923" y2="-1.206" width="0" layer="150"/>
<wire x1="-18.923" y1="-1.206" x2="-18.923" y2="-1.714" width="0" layer="150"/>
<wire x1="-18.923" y1="-1.714" x2="-19.002" y2="-1.143" width="0" layer="150"/>
<wire x1="-19.002" y1="-1.143" x2="-19.082" y2="-1.08" width="0" layer="150"/>
<wire x1="-13.401" y1="1.714" x2="-19.624" y2="1.714" width="0" layer="150"/>
<wire x1="-18.923" y1="0.318" x2="-18.923" y2="1.714" width="0" layer="150"/>
<wire x1="-18.764" y1="1.08" x2="-18.923" y2="1.714" width="0" layer="150"/>
<wire x1="-18.923" y1="1.714" x2="-19.082" y2="1.08" width="0" layer="150"/>
<wire x1="-19.082" y1="1.08" x2="-18.923" y2="1.206" width="0" layer="150"/>
<wire x1="-18.923" y1="1.206" x2="-18.764" y2="1.08" width="0" layer="150"/>
<wire x1="-18.923" y1="1.714" x2="-19.002" y2="1.143" width="0" layer="150"/>
<wire x1="-19.002" y1="1.143" x2="-18.923" y2="1.206" width="0" layer="150"/>
<wire x1="-18.923" y1="1.206" x2="-18.923" y2="1.714" width="0" layer="150"/>
<wire x1="-18.923" y1="1.714" x2="-18.844" y2="1.143" width="0" layer="150"/>
<wire x1="-18.844" y1="1.143" x2="-18.764" y2="1.08" width="0" layer="150"/>
<wire x1="-12.766" y1="-2.032" x2="-17.783" y2="-2.032" width="0" layer="150"/>
<wire x1="-17.082" y1="-0.318" x2="-17.082" y2="-2.032" width="0" layer="150"/>
<wire x1="-17.24" y1="-1.397" x2="-17.082" y2="-2.032" width="0" layer="150"/>
<wire x1="-17.082" y1="-2.032" x2="-16.923" y2="-1.397" width="0" layer="150"/>
<wire x1="-16.923" y1="-1.397" x2="-17.082" y2="-1.524" width="0" layer="150"/>
<wire x1="-17.082" y1="-1.524" x2="-17.24" y2="-1.397" width="0" layer="150"/>
<wire x1="-17.082" y1="-2.032" x2="-17.002" y2="-1.46" width="0" layer="150"/>
<wire x1="-17.002" y1="-1.46" x2="-17.082" y2="-1.524" width="0" layer="150"/>
<wire x1="-17.082" y1="-1.524" x2="-17.082" y2="-2.032" width="0" layer="150"/>
<wire x1="-17.082" y1="-2.032" x2="-17.161" y2="-1.46" width="0" layer="150"/>
<wire x1="-17.161" y1="-1.46" x2="-17.24" y2="-1.397" width="0" layer="150"/>
<wire x1="-17.082" y1="0.318" x2="-17.082" y2="2.032" width="0" layer="150"/>
<wire x1="-16.923" y1="1.397" x2="-17.082" y2="2.032" width="0" layer="150"/>
<wire x1="-17.082" y1="2.032" x2="-17.24" y2="1.397" width="0" layer="150"/>
<wire x1="-17.24" y1="1.397" x2="-17.082" y2="1.524" width="0" layer="150"/>
<wire x1="-17.082" y1="1.524" x2="-16.923" y2="1.397" width="0" layer="150"/>
<wire x1="-17.082" y1="2.032" x2="-17.161" y2="1.46" width="0" layer="150"/>
<wire x1="-17.161" y1="1.46" x2="-17.082" y2="1.524" width="0" layer="150"/>
<wire x1="-17.082" y1="1.524" x2="-17.082" y2="2.032" width="0" layer="150"/>
<wire x1="-17.082" y1="2.032" x2="-17.002" y2="1.46" width="0" layer="150"/>
<wire x1="-17.002" y1="1.46" x2="-16.923" y2="1.397" width="0" layer="150"/>
<wire x1="-12.766" y1="2.032" x2="-17.783" y2="2.032" width="0" layer="150"/>
<polygon width="0" layer="39">
<vertex x="-13.385" y="-4.113"/>
<vertex x="-13.385" y="4.113"/>
<vertex x="13.385" y="4.113"/>
<vertex x="13.385" y="-4.113"/>
</polygon>
<wire x1="-12.7" y1="-1.714" x2="-11.43" y2="-1.714" width="0.1" layer="51"/>
<wire x1="-12.268" y1="-2.921" x2="-12.268" y2="-1.714" width="0.1" layer="51"/>
<wire x1="-11.862" y1="-1.714" x2="-11.862" y2="-2.921" width="0.1" layer="51"/>
<wire x1="-11.862" y1="-2.921" x2="-12.268" y2="-2.921" width="0.1" layer="51"/>
<wire x1="-12.7" y1="1.714" x2="-12.7" y2="-1.714" width="0.1" layer="51"/>
<wire x1="-11.43" y1="-1.714" x2="-10.16" y2="-1.714" width="0.1" layer="51"/>
<wire x1="-13.15" y1="3.878" x2="-13.15" y2="-3.878" width="0.2" layer="21"/>
<wire x1="-13.15" y1="3.878" x2="-13.15" y2="-3.878" width="0.1" layer="152"/>
<wire x1="-13.15" y1="-3.878" x2="13.15" y2="-3.878" width="0.2" layer="21"/>
<wire x1="-13.15" y1="-3.878" x2="13.15" y2="-3.878" width="0.1" layer="152"/>
<wire x1="13.15" y1="-3.878" x2="13.15" y2="3.878" width="0.2" layer="21"/>
<wire x1="13.15" y1="-3.878" x2="13.15" y2="3.878" width="0.1" layer="152"/>
<wire x1="13.15" y1="3.878" x2="-13.15" y2="3.878" width="0.2" layer="21"/>
<wire x1="13.15" y1="3.878" x2="-13.15" y2="3.878" width="0.1" layer="152"/>
<wire x1="-11.862" y1="0.432" x2="-12.268" y2="0.432" width="0.1" layer="51"/>
<wire x1="-12.268" y1="1.714" x2="-12.268" y2="2.921" width="0.1" layer="51"/>
<wire x1="-11.862" y1="2.921" x2="-11.862" y2="1.714" width="0.1" layer="51"/>
<wire x1="-11.862" y1="-0.838" x2="-12.268" y2="-0.838" width="0.1" layer="51"/>
<wire x1="-11.862" y1="-0.432" x2="-12.268" y2="-0.432" width="0.1" layer="51"/>
<wire x1="-11.862" y1="0.838" x2="-12.268" y2="0.838" width="0.1" layer="51"/>
<wire x1="-11.862" y1="0.432" x2="-11.862" y2="0.838" width="0.1" layer="51"/>
<wire x1="-12.268" y1="-0.838" x2="-12.268" y2="-0.432" width="0.1" layer="51"/>
<wire x1="-12.268" y1="0.432" x2="-12.268" y2="0.838" width="0.1" layer="51"/>
<wire x1="-11.862" y1="-0.838" x2="-11.862" y2="-0.432" width="0.1" layer="51"/>
<wire x1="-12.268" y1="-0.851" x2="-11.862" y2="-0.851" width="0.1" layer="51"/>
<wire x1="-11.43" y1="1.714" x2="-12.7" y2="1.714" width="0.1" layer="51"/>
<wire x1="-10.16" y1="1.714" x2="-11.43" y2="1.714" width="0.1" layer="51"/>
<wire x1="-12.7" y1="-1.013" x2="-12.7" y2="1.971" width="0" layer="150"/>
<wire x1="-0.991" y1="1.27" x2="-12.7" y2="1.27" width="0" layer="150"/>
<wire x1="-12.065" y1="1.429" x2="-12.7" y2="1.27" width="0" layer="150"/>
<wire x1="-12.7" y1="1.27" x2="-12.065" y2="1.111" width="0" layer="150"/>
<wire x1="-12.065" y1="1.111" x2="-12.192" y2="1.27" width="0" layer="150"/>
<wire x1="-12.192" y1="1.27" x2="-12.065" y2="1.429" width="0" layer="150"/>
<wire x1="-12.7" y1="1.27" x2="-12.128" y2="1.191" width="0" layer="150"/>
<wire x1="-12.128" y1="1.191" x2="-12.192" y2="1.27" width="0" layer="150"/>
<wire x1="-12.192" y1="1.27" x2="-12.7" y2="1.27" width="0" layer="150"/>
<wire x1="-12.7" y1="1.27" x2="-12.128" y2="1.349" width="0" layer="150"/>
<wire x1="-12.128" y1="1.349" x2="-12.065" y2="1.429" width="0" layer="150"/>
<wire x1="-11.862" y1="2.921" x2="-12.268" y2="2.921" width="0.1" layer="51"/>
<wire x1="-12.065" y1="2.733" x2="-12.065" y2="5.654" width="0" layer="150"/>
<wire x1="-13.183" y1="4.953" x2="-12.065" y2="4.953" width="0" layer="150"/>
<wire x1="-12.7" y1="4.794" x2="-12.065" y2="4.953" width="0" layer="150"/>
<wire x1="-12.065" y1="4.953" x2="-12.7" y2="5.112" width="0" layer="150"/>
<wire x1="-12.7" y1="5.112" x2="-12.573" y2="4.953" width="0" layer="150"/>
<wire x1="-12.573" y1="4.953" x2="-12.7" y2="4.794" width="0" layer="150"/>
<wire x1="-12.065" y1="4.953" x2="-12.636" y2="5.032" width="0" layer="150"/>
<wire x1="-12.636" y1="5.032" x2="-12.573" y2="4.953" width="0" layer="150"/>
<wire x1="-12.573" y1="4.953" x2="-12.065" y2="4.953" width="0" layer="150"/>
<wire x1="-12.065" y1="4.953" x2="-12.636" y2="4.874" width="0" layer="150"/>
<wire x1="-12.636" y1="4.874" x2="-12.7" y2="4.794" width="0" layer="150"/>
<wire x1="-12.065" y1="2.733" x2="-12.065" y2="6.67" width="0" layer="150"/>
<wire x1="-0.991" y1="5.969" x2="-12.065" y2="5.969" width="0" layer="150"/>
<wire x1="-11.43" y1="6.128" x2="-12.065" y2="5.969" width="0" layer="150"/>
<wire x1="-12.065" y1="5.969" x2="-11.43" y2="5.81" width="0" layer="150"/>
<wire x1="-11.43" y1="5.81" x2="-11.557" y2="5.969" width="0" layer="150"/>
<wire x1="-11.557" y1="5.969" x2="-11.43" y2="6.128" width="0" layer="150"/>
<wire x1="-12.065" y1="5.969" x2="-11.494" y2="5.89" width="0" layer="150"/>
<wire x1="-11.494" y1="5.89" x2="-11.557" y2="5.969" width="0" layer="150"/>
<wire x1="-11.557" y1="5.969" x2="-12.065" y2="5.969" width="0" layer="150"/>
<wire x1="-12.065" y1="5.969" x2="-11.494" y2="6.048" width="0" layer="150"/>
<wire x1="-11.494" y1="6.048" x2="-11.43" y2="6.128" width="0" layer="150"/>
<wire x1="-10.998" y1="-2.921" x2="-10.998" y2="-1.714" width="0.1" layer="51"/>
<wire x1="-10.592" y1="-1.714" x2="-10.592" y2="-2.921" width="0.1" layer="51"/>
<wire x1="-10.592" y1="-2.921" x2="-10.998" y2="-2.921" width="0.1" layer="51"/>
<wire x1="-10.16" y1="-1.714" x2="-8.89" y2="-1.714" width="0.1" layer="51"/>
<wire x1="-9.728" y1="-2.921" x2="-9.728" y2="-1.714" width="0.1" layer="51"/>
<wire x1="-9.322" y1="-1.714" x2="-9.322" y2="-2.921" width="0.1" layer="51"/>
<wire x1="-9.322" y1="-2.921" x2="-9.728" y2="-2.921" width="0.1" layer="51"/>
<wire x1="-8.89" y1="-1.714" x2="-7.62" y2="-1.714" width="0.1" layer="51"/>
<wire x1="-8.458" y1="-2.921" x2="-8.458" y2="-1.714" width="0.1" layer="51"/>
<wire x1="-8.052" y1="-1.714" x2="-8.052" y2="-2.921" width="0.1" layer="51"/>
<wire x1="-8.052" y1="-2.921" x2="-8.458" y2="-2.921" width="0.1" layer="51"/>
<wire x1="-10.592" y1="0.432" x2="-10.998" y2="0.432" width="0.1" layer="51"/>
<wire x1="-10.998" y1="1.714" x2="-10.998" y2="2.921" width="0.1" layer="51"/>
<wire x1="-10.592" y1="2.921" x2="-10.592" y2="1.714" width="0.1" layer="51"/>
<wire x1="-10.592" y1="-0.838" x2="-10.998" y2="-0.838" width="0.1" layer="51"/>
<wire x1="-10.592" y1="-0.432" x2="-10.998" y2="-0.432" width="0.1" layer="51"/>
<wire x1="-10.592" y1="0.838" x2="-10.998" y2="0.838" width="0.1" layer="51"/>
<wire x1="-10.592" y1="0.432" x2="-10.592" y2="0.838" width="0.1" layer="51"/>
<wire x1="-10.998" y1="-0.838" x2="-10.998" y2="-0.432" width="0.1" layer="51"/>
<wire x1="-10.998" y1="0.432" x2="-10.998" y2="0.838" width="0.1" layer="51"/>
<wire x1="-10.592" y1="-0.838" x2="-10.592" y2="-0.432" width="0.1" layer="51"/>
<wire x1="-10.998" y1="-0.851" x2="-10.592" y2="-0.851" width="0.1" layer="51"/>
<wire x1="-9.322" y1="0.432" x2="-9.728" y2="0.432" width="0.1" layer="51"/>
<wire x1="-9.728" y1="1.714" x2="-9.728" y2="2.921" width="0.1" layer="51"/>
<wire x1="-9.322" y1="2.921" x2="-9.322" y2="1.714" width="0.1" layer="51"/>
<wire x1="-9.322" y1="-0.838" x2="-9.728" y2="-0.838" width="0.1" layer="51"/>
<wire x1="-9.322" y1="-0.432" x2="-9.728" y2="-0.432" width="0.1" layer="51"/>
<wire x1="-9.322" y1="0.838" x2="-9.728" y2="0.838" width="0.1" layer="51"/>
<wire x1="-9.322" y1="0.432" x2="-9.322" y2="0.838" width="0.1" layer="51"/>
<wire x1="-9.728" y1="-0.838" x2="-9.728" y2="-0.432" width="0.1" layer="51"/>
<wire x1="-9.728" y1="0.432" x2="-9.728" y2="0.838" width="0.1" layer="51"/>
<wire x1="-9.322" y1="-0.838" x2="-9.322" y2="-0.432" width="0.1" layer="51"/>
<wire x1="-9.728" y1="-0.851" x2="-9.322" y2="-0.851" width="0.1" layer="51"/>
<wire x1="-8.89" y1="1.714" x2="-10.16" y2="1.714" width="0.1" layer="51"/>
<wire x1="-8.052" y1="0.432" x2="-8.458" y2="0.432" width="0.1" layer="51"/>
<wire x1="-8.458" y1="1.714" x2="-8.458" y2="2.921" width="0.1" layer="51"/>
<wire x1="-8.052" y1="2.921" x2="-8.052" y2="1.714" width="0.1" layer="51"/>
<wire x1="-8.052" y1="-0.838" x2="-8.458" y2="-0.838" width="0.1" layer="51"/>
<wire x1="-8.052" y1="-0.432" x2="-8.458" y2="-0.432" width="0.1" layer="51"/>
<wire x1="-8.052" y1="0.838" x2="-8.458" y2="0.838" width="0.1" layer="51"/>
<wire x1="-8.052" y1="0.432" x2="-8.052" y2="0.838" width="0.1" layer="51"/>
<wire x1="-8.458" y1="-0.838" x2="-8.458" y2="-0.432" width="0.1" layer="51"/>
<wire x1="-8.458" y1="0.432" x2="-8.458" y2="0.838" width="0.1" layer="51"/>
<wire x1="-8.052" y1="-0.838" x2="-8.052" y2="-0.432" width="0.1" layer="51"/>
<wire x1="-8.458" y1="-0.851" x2="-8.052" y2="-0.851" width="0.1" layer="51"/>
<wire x1="-7.62" y1="1.714" x2="-8.89" y2="1.714" width="0.1" layer="51"/>
<wire x1="-10.592" y1="2.921" x2="-10.998" y2="2.921" width="0.1" layer="51"/>
<wire x1="-9.322" y1="2.921" x2="-9.728" y2="2.921" width="0.1" layer="51"/>
<wire x1="-8.052" y1="2.921" x2="-8.458" y2="2.921" width="0.1" layer="51"/>
<wire x1="-10.795" y1="2.733" x2="-10.795" y2="5.654" width="0" layer="150"/>
<wire x1="-9.525" y1="4.953" x2="-10.795" y2="4.953" width="0" layer="150"/>
<wire x1="-10.16" y1="5.112" x2="-10.795" y2="4.953" width="0" layer="150"/>
<wire x1="-10.795" y1="4.953" x2="-10.16" y2="4.794" width="0" layer="150"/>
<wire x1="-10.16" y1="4.794" x2="-10.287" y2="4.953" width="0" layer="150"/>
<wire x1="-10.287" y1="4.953" x2="-10.16" y2="5.112" width="0" layer="150"/>
<wire x1="-10.795" y1="4.953" x2="-10.224" y2="4.874" width="0" layer="150"/>
<wire x1="-10.224" y1="4.874" x2="-10.287" y2="4.953" width="0" layer="150"/>
<wire x1="-10.287" y1="4.953" x2="-10.795" y2="4.953" width="0" layer="150"/>
<wire x1="-10.795" y1="4.953" x2="-10.224" y2="5.032" width="0" layer="150"/>
<wire x1="-10.224" y1="5.032" x2="-10.16" y2="5.112" width="0" layer="150"/>
<wire x1="-7.62" y1="-1.714" x2="-6.35" y2="-1.714" width="0.1" layer="51"/>
<wire x1="-7.188" y1="-2.921" x2="-7.188" y2="-1.714" width="0.1" layer="51"/>
<wire x1="-6.782" y1="-1.714" x2="-6.782" y2="-2.921" width="0.1" layer="51"/>
<wire x1="-6.782" y1="-2.921" x2="-7.188" y2="-2.921" width="0.1" layer="51"/>
<wire x1="-6.35" y1="-1.714" x2="-5.08" y2="-1.714" width="0.1" layer="51"/>
<wire x1="-5.918" y1="-2.921" x2="-5.918" y2="-1.714" width="0.1" layer="51"/>
<wire x1="-5.512" y1="-1.714" x2="-5.512" y2="-2.921" width="0.1" layer="51"/>
<wire x1="-5.512" y1="-2.921" x2="-5.918" y2="-2.921" width="0.1" layer="51"/>
<wire x1="-5.08" y1="-1.714" x2="-3.81" y2="-1.714" width="0.1" layer="51"/>
<wire x1="-4.648" y1="-2.921" x2="-4.648" y2="-1.714" width="0.1" layer="51"/>
<wire x1="-4.242" y1="-2.921" x2="-4.648" y2="-2.921" width="0.1" layer="51"/>
<wire x1="-6.782" y1="0.432" x2="-7.188" y2="0.432" width="0.1" layer="51"/>
<wire x1="-7.188" y1="1.714" x2="-7.188" y2="2.921" width="0.1" layer="51"/>
<wire x1="-6.782" y1="2.921" x2="-6.782" y2="1.714" width="0.1" layer="51"/>
<wire x1="-6.782" y1="-0.838" x2="-7.188" y2="-0.838" width="0.1" layer="51"/>
<wire x1="-6.782" y1="-0.432" x2="-7.188" y2="-0.432" width="0.1" layer="51"/>
<wire x1="-6.782" y1="0.838" x2="-7.188" y2="0.838" width="0.1" layer="51"/>
<wire x1="-6.782" y1="0.432" x2="-6.782" y2="0.838" width="0.1" layer="51"/>
<wire x1="-7.188" y1="-0.838" x2="-7.188" y2="-0.432" width="0.1" layer="51"/>
<wire x1="-7.188" y1="0.432" x2="-7.188" y2="0.838" width="0.1" layer="51"/>
<wire x1="-6.782" y1="-0.838" x2="-6.782" y2="-0.432" width="0.1" layer="51"/>
<wire x1="-7.188" y1="-0.851" x2="-6.782" y2="-0.851" width="0.1" layer="51"/>
<wire x1="-6.35" y1="1.714" x2="-7.62" y2="1.714" width="0.1" layer="51"/>
<wire x1="-5.512" y1="0.432" x2="-5.918" y2="0.432" width="0.1" layer="51"/>
<wire x1="-5.918" y1="1.714" x2="-5.918" y2="2.921" width="0.1" layer="51"/>
<wire x1="-5.512" y1="2.921" x2="-5.512" y2="1.714" width="0.1" layer="51"/>
<wire x1="-5.512" y1="-0.838" x2="-5.918" y2="-0.838" width="0.1" layer="51"/>
<wire x1="-5.512" y1="-0.432" x2="-5.918" y2="-0.432" width="0.1" layer="51"/>
<wire x1="-5.512" y1="0.838" x2="-5.918" y2="0.838" width="0.1" layer="51"/>
<wire x1="-5.512" y1="0.432" x2="-5.512" y2="0.838" width="0.1" layer="51"/>
<wire x1="-5.918" y1="-0.838" x2="-5.918" y2="-0.432" width="0.1" layer="51"/>
<wire x1="-5.918" y1="0.432" x2="-5.918" y2="0.838" width="0.1" layer="51"/>
<wire x1="-5.512" y1="-0.838" x2="-5.512" y2="-0.432" width="0.1" layer="51"/>
<wire x1="-5.918" y1="-0.851" x2="-5.512" y2="-0.851" width="0.1" layer="51"/>
<wire x1="-5.08" y1="1.714" x2="-6.35" y2="1.714" width="0.1" layer="51"/>
<wire x1="-4.242" y1="0.432" x2="-4.648" y2="0.432" width="0.1" layer="51"/>
<wire x1="-4.648" y1="1.714" x2="-4.648" y2="2.921" width="0.1" layer="51"/>
<wire x1="-4.242" y1="-0.838" x2="-4.648" y2="-0.838" width="0.1" layer="51"/>
<wire x1="-4.242" y1="-0.432" x2="-4.648" y2="-0.432" width="0.1" layer="51"/>
<wire x1="-4.242" y1="0.838" x2="-4.648" y2="0.838" width="0.1" layer="51"/>
<wire x1="-4.648" y1="-0.838" x2="-4.648" y2="-0.432" width="0.1" layer="51"/>
<wire x1="-4.648" y1="0.432" x2="-4.648" y2="0.838" width="0.1" layer="51"/>
<wire x1="-4.648" y1="-0.851" x2="-4.242" y2="-0.851" width="0.1" layer="51"/>
<wire x1="-3.81" y1="1.714" x2="-5.08" y2="1.714" width="0.1" layer="51"/>
<wire x1="-6.782" y1="2.921" x2="-7.188" y2="2.921" width="0.1" layer="51"/>
<wire x1="-5.512" y1="2.921" x2="-5.918" y2="2.921" width="0.1" layer="51"/>
<wire x1="-4.242" y1="2.921" x2="-4.648" y2="2.921" width="0.1" layer="51"/>
<wire x1="-4.242" y1="-1.714" x2="-4.242" y2="-2.921" width="0.1" layer="51"/>
<wire x1="-3.81" y1="-1.714" x2="-2.54" y2="-1.714" width="0.1" layer="51"/>
<wire x1="-3.378" y1="-2.921" x2="-3.378" y2="-1.714" width="0.1" layer="51"/>
<wire x1="-2.972" y1="-1.714" x2="-2.972" y2="-2.921" width="0.1" layer="51"/>
<wire x1="-2.972" y1="-2.921" x2="-3.378" y2="-2.921" width="0.1" layer="51"/>
<wire x1="-2.54" y1="-1.714" x2="-1.27" y2="-1.714" width="0.1" layer="51"/>
<wire x1="-2.108" y1="-2.921" x2="-2.108" y2="-1.714" width="0.1" layer="51"/>
<wire x1="-1.702" y1="-1.714" x2="-1.702" y2="-2.921" width="0.1" layer="51"/>
<wire x1="-1.702" y1="-2.921" x2="-2.108" y2="-2.921" width="0.1" layer="51"/>
<wire x1="-1.27" y1="-1.714" x2="0" y2="-1.714" width="0.1" layer="51"/>
<wire x1="-4.242" y1="2.921" x2="-4.242" y2="1.714" width="0.1" layer="51"/>
<wire x1="-4.242" y1="0.432" x2="-4.242" y2="0.838" width="0.1" layer="51"/>
<wire x1="-4.242" y1="-0.838" x2="-4.242" y2="-0.432" width="0.1" layer="51"/>
<wire x1="-2.972" y1="0.432" x2="-3.378" y2="0.432" width="0.1" layer="51"/>
<wire x1="-3.378" y1="1.714" x2="-3.378" y2="2.921" width="0.1" layer="51"/>
<wire x1="-2.972" y1="2.921" x2="-2.972" y2="1.714" width="0.1" layer="51"/>
<wire x1="-2.972" y1="-0.838" x2="-3.378" y2="-0.838" width="0.1" layer="51"/>
<wire x1="-2.972" y1="-0.432" x2="-3.378" y2="-0.432" width="0.1" layer="51"/>
<wire x1="-2.972" y1="0.838" x2="-3.378" y2="0.838" width="0.1" layer="51"/>
<wire x1="-2.972" y1="0.432" x2="-2.972" y2="0.838" width="0.1" layer="51"/>
<wire x1="-3.378" y1="-0.838" x2="-3.378" y2="-0.432" width="0.1" layer="51"/>
<wire x1="-3.378" y1="0.432" x2="-3.378" y2="0.838" width="0.1" layer="51"/>
<wire x1="-2.972" y1="-0.838" x2="-2.972" y2="-0.432" width="0.1" layer="51"/>
<wire x1="-3.378" y1="-0.851" x2="-2.972" y2="-0.851" width="0.1" layer="51"/>
<wire x1="-2.54" y1="1.714" x2="-3.81" y2="1.714" width="0.1" layer="51"/>
<wire x1="-1.702" y1="0.432" x2="-2.108" y2="0.432" width="0.1" layer="51"/>
<wire x1="-2.108" y1="1.714" x2="-2.108" y2="2.921" width="0.1" layer="51"/>
<wire x1="-1.702" y1="2.921" x2="-1.702" y2="1.714" width="0.1" layer="51"/>
<wire x1="-1.702" y1="-0.838" x2="-2.108" y2="-0.838" width="0.1" layer="51"/>
<wire x1="-1.702" y1="-0.432" x2="-2.108" y2="-0.432" width="0.1" layer="51"/>
<wire x1="-1.702" y1="0.838" x2="-2.108" y2="0.838" width="0.1" layer="51"/>
<wire x1="-1.702" y1="0.432" x2="-1.702" y2="0.838" width="0.1" layer="51"/>
<wire x1="-2.108" y1="-0.838" x2="-2.108" y2="-0.432" width="0.1" layer="51"/>
<wire x1="-2.108" y1="0.432" x2="-2.108" y2="0.838" width="0.1" layer="51"/>
<wire x1="-1.702" y1="-0.838" x2="-1.702" y2="-0.432" width="0.1" layer="51"/>
<wire x1="-2.108" y1="-0.851" x2="-1.702" y2="-0.851" width="0.1" layer="51"/>
<wire x1="-1.27" y1="1.714" x2="-2.54" y2="1.714" width="0.1" layer="51"/>
<wire x1="0" y1="1.714" x2="-1.27" y2="1.714" width="0.1" layer="51"/>
<wire x1="-2.972" y1="2.921" x2="-3.378" y2="2.921" width="0.1" layer="51"/>
<wire x1="-1.702" y1="2.921" x2="-2.108" y2="2.921" width="0.1" layer="51"/>
<wire x1="-0.838" y1="-2.921" x2="-0.838" y2="-1.714" width="0.1" layer="51"/>
<wire x1="-0.432" y1="-1.714" x2="-0.432" y2="-2.921" width="0.1" layer="51"/>
<wire x1="-0.432" y1="-2.921" x2="-0.838" y2="-2.921" width="0.1" layer="51"/>
<wire x1="0" y1="-1.714" x2="1.27" y2="-1.714" width="0.1" layer="51"/>
<wire x1="0.432" y1="-2.921" x2="0.432" y2="-1.714" width="0.1" layer="51"/>
<wire x1="0.838" y1="-1.714" x2="0.838" y2="-2.921" width="0.1" layer="51"/>
<wire x1="0.838" y1="-2.921" x2="0.432" y2="-2.921" width="0.1" layer="51"/>
<wire x1="1.27" y1="-1.714" x2="2.54" y2="-1.714" width="0.1" layer="51"/>
<wire x1="1.702" y1="-2.921" x2="1.702" y2="-1.714" width="0.1" layer="51"/>
<wire x1="2.108" y1="-1.714" x2="2.108" y2="-2.921" width="0.1" layer="51"/>
<wire x1="2.108" y1="-2.921" x2="1.702" y2="-2.921" width="0.1" layer="51"/>
<wire x1="-0.432" y1="0.432" x2="-0.838" y2="0.432" width="0.1" layer="51"/>
<wire x1="-0.838" y1="1.714" x2="-0.838" y2="2.921" width="0.1" layer="51"/>
<wire x1="-0.432" y1="2.921" x2="-0.432" y2="1.714" width="0.1" layer="51"/>
<wire x1="-0.432" y1="-0.838" x2="-0.838" y2="-0.838" width="0.1" layer="51"/>
<wire x1="-0.432" y1="-0.432" x2="-0.838" y2="-0.432" width="0.1" layer="51"/>
<wire x1="-0.432" y1="0.838" x2="-0.838" y2="0.838" width="0.1" layer="51"/>
<wire x1="-0.432" y1="0.432" x2="-0.432" y2="0.838" width="0.1" layer="51"/>
<wire x1="-0.838" y1="-0.838" x2="-0.838" y2="-0.432" width="0.1" layer="51"/>
<wire x1="-0.838" y1="0.432" x2="-0.838" y2="0.838" width="0.1" layer="51"/>
<wire x1="-0.432" y1="-0.838" x2="-0.432" y2="-0.432" width="0.1" layer="51"/>
<wire x1="-0.838" y1="-0.851" x2="-0.432" y2="-0.851" width="0.1" layer="51"/>
<wire x1="0.838" y1="0.432" x2="0.432" y2="0.432" width="0.1" layer="51"/>
<wire x1="0.432" y1="1.714" x2="0.432" y2="2.921" width="0.1" layer="51"/>
<wire x1="0.838" y1="2.921" x2="0.838" y2="1.714" width="0.1" layer="51"/>
<wire x1="0.838" y1="-0.838" x2="0.432" y2="-0.838" width="0.1" layer="51"/>
<wire x1="0.838" y1="-0.432" x2="0.432" y2="-0.432" width="0.1" layer="51"/>
<wire x1="0.838" y1="0.838" x2="0.432" y2="0.838" width="0.1" layer="51"/>
<wire x1="0.838" y1="0.432" x2="0.838" y2="0.838" width="0.1" layer="51"/>
<wire x1="0.432" y1="-0.838" x2="0.432" y2="-0.432" width="0.1" layer="51"/>
<wire x1="0.432" y1="0.432" x2="0.432" y2="0.838" width="0.1" layer="51"/>
<wire x1="0.838" y1="-0.838" x2="0.838" y2="-0.432" width="0.1" layer="51"/>
<wire x1="0.432" y1="-0.851" x2="0.838" y2="-0.851" width="0.1" layer="51"/>
<wire x1="1.27" y1="1.714" x2="0" y2="1.714" width="0.1" layer="51"/>
<wire x1="2.108" y1="0.432" x2="1.702" y2="0.432" width="0.1" layer="51"/>
<wire x1="1.702" y1="1.714" x2="1.702" y2="2.921" width="0.1" layer="51"/>
<wire x1="2.108" y1="2.921" x2="2.108" y2="1.714" width="0.1" layer="51"/>
<wire x1="2.108" y1="-0.838" x2="1.702" y2="-0.838" width="0.1" layer="51"/>
<wire x1="2.108" y1="-0.432" x2="1.702" y2="-0.432" width="0.1" layer="51"/>
<wire x1="2.108" y1="0.838" x2="1.702" y2="0.838" width="0.1" layer="51"/>
<wire x1="2.108" y1="0.432" x2="2.108" y2="0.838" width="0.1" layer="51"/>
<wire x1="1.702" y1="-0.838" x2="1.702" y2="-0.432" width="0.1" layer="51"/>
<wire x1="1.702" y1="0.432" x2="1.702" y2="0.838" width="0.1" layer="51"/>
<wire x1="2.108" y1="-0.838" x2="2.108" y2="-0.432" width="0.1" layer="51"/>
<wire x1="1.702" y1="-0.851" x2="2.108" y2="-0.851" width="0.1" layer="51"/>
<wire x1="2.54" y1="1.714" x2="1.27" y2="1.714" width="0.1" layer="51"/>
<wire x1="0.991" y1="1.27" x2="12.7" y2="1.27" width="0" layer="150"/>
<wire x1="-0.432" y1="2.921" x2="-0.838" y2="2.921" width="0.1" layer="51"/>
<wire x1="0.838" y1="2.921" x2="0.432" y2="2.921" width="0.1" layer="51"/>
<wire x1="2.108" y1="2.921" x2="1.702" y2="2.921" width="0.1" layer="51"/>
<wire x1="0.991" y1="5.969" x2="12.065" y2="5.969" width="0" layer="150"/>
<wire x1="2.54" y1="-1.714" x2="3.81" y2="-1.714" width="0.1" layer="51"/>
<wire x1="2.972" y1="-2.921" x2="2.972" y2="-1.714" width="0.1" layer="51"/>
<wire x1="3.378" y1="-1.714" x2="3.378" y2="-2.921" width="0.1" layer="51"/>
<wire x1="3.378" y1="-2.921" x2="2.972" y2="-2.921" width="0.1" layer="51"/>
<wire x1="3.81" y1="-1.714" x2="5.08" y2="-1.714" width="0.1" layer="51"/>
<wire x1="4.242" y1="-2.921" x2="4.242" y2="-1.714" width="0.1" layer="51"/>
<wire x1="4.648" y1="-1.714" x2="4.648" y2="-2.921" width="0.1" layer="51"/>
<wire x1="4.648" y1="-2.921" x2="4.242" y2="-2.921" width="0.1" layer="51"/>
<wire x1="5.08" y1="-1.714" x2="6.35" y2="-1.714" width="0.1" layer="51"/>
<wire x1="5.512" y1="-2.921" x2="5.512" y2="-1.714" width="0.1" layer="51"/>
<wire x1="5.918" y1="-2.921" x2="5.512" y2="-2.921" width="0.1" layer="51"/>
<wire x1="3.378" y1="0.432" x2="2.972" y2="0.432" width="0.1" layer="51"/>
<wire x1="2.972" y1="1.714" x2="2.972" y2="2.921" width="0.1" layer="51"/>
<wire x1="3.378" y1="2.921" x2="3.378" y2="1.714" width="0.1" layer="51"/>
<wire x1="3.378" y1="-0.838" x2="2.972" y2="-0.838" width="0.1" layer="51"/>
<wire x1="3.378" y1="-0.432" x2="2.972" y2="-0.432" width="0.1" layer="51"/>
<wire x1="3.378" y1="0.838" x2="2.972" y2="0.838" width="0.1" layer="51"/>
<wire x1="3.378" y1="0.432" x2="3.378" y2="0.838" width="0.1" layer="51"/>
<wire x1="2.972" y1="-0.838" x2="2.972" y2="-0.432" width="0.1" layer="51"/>
<wire x1="2.972" y1="0.432" x2="2.972" y2="0.838" width="0.1" layer="51"/>
<wire x1="3.378" y1="-0.838" x2="3.378" y2="-0.432" width="0.1" layer="51"/>
<wire x1="2.972" y1="-0.851" x2="3.378" y2="-0.851" width="0.1" layer="51"/>
<wire x1="3.81" y1="1.714" x2="2.54" y2="1.714" width="0.1" layer="51"/>
<wire x1="4.648" y1="0.432" x2="4.242" y2="0.432" width="0.1" layer="51"/>
<wire x1="4.242" y1="1.714" x2="4.242" y2="2.921" width="0.1" layer="51"/>
<wire x1="4.648" y1="2.921" x2="4.648" y2="1.714" width="0.1" layer="51"/>
<wire x1="4.648" y1="-0.838" x2="4.242" y2="-0.838" width="0.1" layer="51"/>
<wire x1="4.648" y1="-0.432" x2="4.242" y2="-0.432" width="0.1" layer="51"/>
<wire x1="4.648" y1="0.838" x2="4.242" y2="0.838" width="0.1" layer="51"/>
<wire x1="4.648" y1="0.432" x2="4.648" y2="0.838" width="0.1" layer="51"/>
<wire x1="4.242" y1="-0.838" x2="4.242" y2="-0.432" width="0.1" layer="51"/>
<wire x1="4.242" y1="0.432" x2="4.242" y2="0.838" width="0.1" layer="51"/>
<wire x1="4.648" y1="-0.838" x2="4.648" y2="-0.432" width="0.1" layer="51"/>
<wire x1="4.242" y1="-0.851" x2="4.648" y2="-0.851" width="0.1" layer="51"/>
<wire x1="5.08" y1="1.714" x2="3.81" y2="1.714" width="0.1" layer="51"/>
<wire x1="5.918" y1="0.432" x2="5.512" y2="0.432" width="0.1" layer="51"/>
<wire x1="5.512" y1="1.714" x2="5.512" y2="2.921" width="0.1" layer="51"/>
<wire x1="5.918" y1="-0.838" x2="5.512" y2="-0.838" width="0.1" layer="51"/>
<wire x1="5.918" y1="-0.432" x2="5.512" y2="-0.432" width="0.1" layer="51"/>
<wire x1="5.918" y1="0.838" x2="5.512" y2="0.838" width="0.1" layer="51"/>
<wire x1="5.512" y1="-0.838" x2="5.512" y2="-0.432" width="0.1" layer="51"/>
<wire x1="5.512" y1="0.432" x2="5.512" y2="0.838" width="0.1" layer="51"/>
<wire x1="5.512" y1="-0.851" x2="5.918" y2="-0.851" width="0.1" layer="51"/>
<wire x1="6.35" y1="1.714" x2="5.08" y2="1.714" width="0.1" layer="51"/>
<wire x1="3.378" y1="2.921" x2="2.972" y2="2.921" width="0.1" layer="51"/>
<wire x1="4.648" y1="2.921" x2="4.242" y2="2.921" width="0.1" layer="51"/>
<wire x1="5.918" y1="2.921" x2="5.512" y2="2.921" width="0.1" layer="51"/>
<wire x1="5.918" y1="-1.714" x2="5.918" y2="-2.921" width="0.1" layer="51"/>
<wire x1="6.35" y1="-1.714" x2="7.62" y2="-1.714" width="0.1" layer="51"/>
<wire x1="6.782" y1="-2.921" x2="6.782" y2="-1.714" width="0.1" layer="51"/>
<wire x1="7.188" y1="-1.714" x2="7.188" y2="-2.921" width="0.1" layer="51"/>
<wire x1="7.188" y1="-2.921" x2="6.782" y2="-2.921" width="0.1" layer="51"/>
<wire x1="7.62" y1="-1.714" x2="8.89" y2="-1.714" width="0.1" layer="51"/>
<wire x1="8.052" y1="-2.921" x2="8.052" y2="-1.714" width="0.1" layer="51"/>
<wire x1="8.458" y1="-1.714" x2="8.458" y2="-2.921" width="0.1" layer="51"/>
<wire x1="8.458" y1="-2.921" x2="8.052" y2="-2.921" width="0.1" layer="51"/>
<wire x1="8.89" y1="-1.714" x2="10.16" y2="-1.714" width="0.1" layer="51"/>
<wire x1="5.918" y1="2.921" x2="5.918" y2="1.714" width="0.1" layer="51"/>
<wire x1="5.918" y1="0.432" x2="5.918" y2="0.838" width="0.1" layer="51"/>
<wire x1="5.918" y1="-0.838" x2="5.918" y2="-0.432" width="0.1" layer="51"/>
<wire x1="7.188" y1="0.432" x2="6.782" y2="0.432" width="0.1" layer="51"/>
<wire x1="6.782" y1="1.714" x2="6.782" y2="2.921" width="0.1" layer="51"/>
<wire x1="7.188" y1="2.921" x2="7.188" y2="1.714" width="0.1" layer="51"/>
<wire x1="7.188" y1="-0.838" x2="6.782" y2="-0.838" width="0.1" layer="51"/>
<wire x1="7.188" y1="-0.432" x2="6.782" y2="-0.432" width="0.1" layer="51"/>
<wire x1="7.188" y1="0.838" x2="6.782" y2="0.838" width="0.1" layer="51"/>
<wire x1="7.188" y1="0.432" x2="7.188" y2="0.838" width="0.1" layer="51"/>
<wire x1="6.782" y1="-0.838" x2="6.782" y2="-0.432" width="0.1" layer="51"/>
<wire x1="6.782" y1="0.432" x2="6.782" y2="0.838" width="0.1" layer="51"/>
<wire x1="7.188" y1="-0.838" x2="7.188" y2="-0.432" width="0.1" layer="51"/>
<wire x1="6.782" y1="-0.851" x2="7.188" y2="-0.851" width="0.1" layer="51"/>
<wire x1="7.62" y1="1.714" x2="6.35" y2="1.714" width="0.1" layer="51"/>
<wire x1="8.458" y1="0.432" x2="8.052" y2="0.432" width="0.1" layer="51"/>
<wire x1="8.052" y1="1.714" x2="8.052" y2="2.921" width="0.1" layer="51"/>
<wire x1="8.458" y1="2.921" x2="8.458" y2="1.714" width="0.1" layer="51"/>
<wire x1="8.458" y1="-0.838" x2="8.052" y2="-0.838" width="0.1" layer="51"/>
<wire x1="8.458" y1="-0.432" x2="8.052" y2="-0.432" width="0.1" layer="51"/>
<wire x1="8.458" y1="0.838" x2="8.052" y2="0.838" width="0.1" layer="51"/>
<wire x1="8.458" y1="0.432" x2="8.458" y2="0.838" width="0.1" layer="51"/>
<wire x1="8.052" y1="-0.838" x2="8.052" y2="-0.432" width="0.1" layer="51"/>
<wire x1="8.052" y1="0.432" x2="8.052" y2="0.838" width="0.1" layer="51"/>
<wire x1="8.458" y1="-0.838" x2="8.458" y2="-0.432" width="0.1" layer="51"/>
<wire x1="8.052" y1="-0.851" x2="8.458" y2="-0.851" width="0.1" layer="51"/>
<wire x1="8.89" y1="1.714" x2="7.62" y2="1.714" width="0.1" layer="51"/>
<wire x1="10.16" y1="1.714" x2="8.89" y2="1.714" width="0.1" layer="51"/>
<wire x1="7.188" y1="2.921" x2="6.782" y2="2.921" width="0.1" layer="51"/>
<wire x1="8.458" y1="2.921" x2="8.052" y2="2.921" width="0.1" layer="51"/>
<wire x1="9.322" y1="-2.921" x2="9.322" y2="-1.714" width="0.1" layer="51"/>
<wire x1="9.728" y1="-1.714" x2="9.728" y2="-2.921" width="0.1" layer="51"/>
<wire x1="9.728" y1="-2.921" x2="9.322" y2="-2.921" width="0.1" layer="51"/>
<wire x1="10.16" y1="-1.714" x2="11.43" y2="-1.714" width="0.1" layer="51"/>
<wire x1="10.592" y1="-2.921" x2="10.592" y2="-1.714" width="0.1" layer="51"/>
<wire x1="10.998" y1="-1.714" x2="10.998" y2="-2.921" width="0.1" layer="51"/>
<wire x1="10.998" y1="-2.921" x2="10.592" y2="-2.921" width="0.1" layer="51"/>
<wire x1="11.43" y1="-1.714" x2="12.7" y2="-1.714" width="0.1" layer="51"/>
<wire x1="11.862" y1="-2.921" x2="11.862" y2="-1.714" width="0.1" layer="51"/>
<wire x1="12.268" y1="-1.714" x2="12.268" y2="-2.921" width="0.1" layer="51"/>
<wire x1="12.268" y1="-2.921" x2="11.862" y2="-2.921" width="0.1" layer="51"/>
<wire x1="9.728" y1="0.432" x2="9.322" y2="0.432" width="0.1" layer="51"/>
<wire x1="9.322" y1="1.714" x2="9.322" y2="2.921" width="0.1" layer="51"/>
<wire x1="9.728" y1="2.921" x2="9.728" y2="1.714" width="0.1" layer="51"/>
<wire x1="9.728" y1="-0.838" x2="9.322" y2="-0.838" width="0.1" layer="51"/>
<wire x1="9.728" y1="-0.432" x2="9.322" y2="-0.432" width="0.1" layer="51"/>
<wire x1="9.728" y1="0.838" x2="9.322" y2="0.838" width="0.1" layer="51"/>
<wire x1="9.728" y1="0.432" x2="9.728" y2="0.838" width="0.1" layer="51"/>
<wire x1="9.322" y1="-0.838" x2="9.322" y2="-0.432" width="0.1" layer="51"/>
<wire x1="9.322" y1="0.432" x2="9.322" y2="0.838" width="0.1" layer="51"/>
<wire x1="9.728" y1="-0.838" x2="9.728" y2="-0.432" width="0.1" layer="51"/>
<wire x1="9.322" y1="-0.851" x2="9.728" y2="-0.851" width="0.1" layer="51"/>
<wire x1="10.998" y1="0.432" x2="10.592" y2="0.432" width="0.1" layer="51"/>
<wire x1="10.592" y1="1.714" x2="10.592" y2="2.921" width="0.1" layer="51"/>
<wire x1="10.998" y1="2.921" x2="10.998" y2="1.714" width="0.1" layer="51"/>
<wire x1="10.998" y1="-0.838" x2="10.592" y2="-0.838" width="0.1" layer="51"/>
<wire x1="10.998" y1="-0.432" x2="10.592" y2="-0.432" width="0.1" layer="51"/>
<wire x1="10.998" y1="0.838" x2="10.592" y2="0.838" width="0.1" layer="51"/>
<wire x1="10.998" y1="0.432" x2="10.998" y2="0.838" width="0.1" layer="51"/>
<wire x1="10.592" y1="-0.838" x2="10.592" y2="-0.432" width="0.1" layer="51"/>
<wire x1="10.592" y1="0.432" x2="10.592" y2="0.838" width="0.1" layer="51"/>
<wire x1="10.998" y1="-0.838" x2="10.998" y2="-0.432" width="0.1" layer="51"/>
<wire x1="10.592" y1="-0.851" x2="10.998" y2="-0.851" width="0.1" layer="51"/>
<wire x1="11.43" y1="1.714" x2="10.16" y2="1.714" width="0.1" layer="51"/>
<wire x1="12.268" y1="0.432" x2="11.862" y2="0.432" width="0.1" layer="51"/>
<wire x1="11.862" y1="1.714" x2="11.862" y2="2.921" width="0.1" layer="51"/>
<wire x1="12.268" y1="2.921" x2="12.268" y2="1.714" width="0.1" layer="51"/>
<wire x1="12.268" y1="-0.838" x2="11.862" y2="-0.838" width="0.1" layer="51"/>
<wire x1="12.268" y1="-0.432" x2="11.862" y2="-0.432" width="0.1" layer="51"/>
<wire x1="12.268" y1="0.838" x2="11.862" y2="0.838" width="0.1" layer="51"/>
<wire x1="12.268" y1="0.432" x2="12.268" y2="0.838" width="0.1" layer="51"/>
<wire x1="11.862" y1="-0.838" x2="11.862" y2="-0.432" width="0.1" layer="51"/>
<wire x1="11.862" y1="0.432" x2="11.862" y2="0.838" width="0.1" layer="51"/>
<wire x1="12.268" y1="-0.838" x2="12.268" y2="-0.432" width="0.1" layer="51"/>
<wire x1="11.862" y1="-0.851" x2="12.268" y2="-0.851" width="0.1" layer="51"/>
<wire x1="12.7" y1="1.714" x2="11.43" y2="1.714" width="0.1" layer="51"/>
<wire x1="12.065" y1="1.111" x2="12.7" y2="1.27" width="0" layer="150"/>
<wire x1="12.7" y1="1.27" x2="12.065" y2="1.429" width="0" layer="150"/>
<wire x1="12.065" y1="1.429" x2="12.192" y2="1.27" width="0" layer="150"/>
<wire x1="12.192" y1="1.27" x2="12.065" y2="1.111" width="0" layer="150"/>
<wire x1="12.7" y1="1.27" x2="12.128" y2="1.349" width="0" layer="150"/>
<wire x1="12.128" y1="1.349" x2="12.192" y2="1.27" width="0" layer="150"/>
<wire x1="12.192" y1="1.27" x2="12.7" y2="1.27" width="0" layer="150"/>
<wire x1="12.7" y1="1.27" x2="12.128" y2="1.191" width="0" layer="150"/>
<wire x1="12.128" y1="1.191" x2="12.065" y2="1.111" width="0" layer="150"/>
<wire x1="9.728" y1="2.921" x2="9.322" y2="2.921" width="0.1" layer="51"/>
<wire x1="10.998" y1="2.921" x2="10.592" y2="2.921" width="0.1" layer="51"/>
<wire x1="12.268" y1="2.921" x2="11.862" y2="2.921" width="0.1" layer="51"/>
<wire x1="12.065" y1="2.733" x2="12.065" y2="6.67" width="0" layer="150"/>
<wire x1="11.684" y1="4.003" x2="11.684" y2="4.956" width="0" layer="150"/>
<wire x1="10.414" y1="4.254" x2="11.684" y2="4.254" width="0" layer="150"/>
<wire x1="11.049" y1="4.096" x2="11.684" y2="4.254" width="0" layer="150"/>
<wire x1="11.684" y1="4.254" x2="11.049" y2="4.413" width="0" layer="150"/>
<wire x1="11.049" y1="4.413" x2="11.176" y2="4.254" width="0" layer="150"/>
<wire x1="11.176" y1="4.254" x2="11.049" y2="4.096" width="0" layer="150"/>
<wire x1="11.684" y1="4.254" x2="11.112" y2="4.334" width="0" layer="150"/>
<wire x1="11.112" y1="4.334" x2="11.176" y2="4.254" width="0" layer="150"/>
<wire x1="11.176" y1="4.254" x2="11.684" y2="4.254" width="0" layer="150"/>
<wire x1="11.684" y1="4.254" x2="11.112" y2="4.175" width="0" layer="150"/>
<wire x1="11.112" y1="4.175" x2="11.049" y2="4.096" width="0" layer="150"/>
<wire x1="11.43" y1="5.81" x2="12.065" y2="5.969" width="0" layer="150"/>
<wire x1="12.065" y1="5.969" x2="11.43" y2="6.128" width="0" layer="150"/>
<wire x1="11.43" y1="6.128" x2="11.557" y2="5.969" width="0" layer="150"/>
<wire x1="11.557" y1="5.969" x2="11.43" y2="5.81" width="0" layer="150"/>
<wire x1="12.065" y1="5.969" x2="11.494" y2="6.048" width="0" layer="150"/>
<wire x1="11.494" y1="6.048" x2="11.557" y2="5.969" width="0" layer="150"/>
<wire x1="11.557" y1="5.969" x2="12.065" y2="5.969" width="0" layer="150"/>
<wire x1="12.065" y1="5.969" x2="11.494" y2="5.89" width="0" layer="150"/>
<wire x1="11.494" y1="5.89" x2="11.43" y2="5.81" width="0" layer="150"/>
<wire x1="12.7" y1="-1.714" x2="12.7" y2="1.714" width="0.1" layer="51"/>
<wire x1="13.782" y1="0.635" x2="14.798" y2="0.635" width="0" layer="150"/>
<wire x1="14.097" y1="1.714" x2="14.097" y2="0.635" width="0" layer="150"/>
<wire x1="13.938" y1="1.27" x2="14.097" y2="0.635" width="0" layer="150"/>
<wire x1="14.097" y1="0.635" x2="14.256" y2="1.27" width="0" layer="150"/>
<wire x1="14.256" y1="1.27" x2="14.097" y2="1.143" width="0" layer="150"/>
<wire x1="14.097" y1="1.143" x2="13.938" y2="1.27" width="0" layer="150"/>
<wire x1="14.097" y1="0.635" x2="14.176" y2="1.206" width="0" layer="150"/>
<wire x1="14.176" y1="1.206" x2="14.097" y2="1.143" width="0" layer="150"/>
<wire x1="14.097" y1="1.143" x2="14.097" y2="0.635" width="0" layer="150"/>
<wire x1="14.097" y1="0.635" x2="14.018" y2="1.206" width="0" layer="150"/>
<wire x1="14.018" y1="1.206" x2="13.938" y2="1.27" width="0" layer="150"/>
<wire x1="12.7" y1="-1.013" x2="12.7" y2="1.971" width="0" layer="150"/>
<wire x1="12.446" y1="4.003" x2="12.446" y2="4.956" width="0" layer="150"/>
<wire x1="13.564" y1="4.254" x2="12.446" y2="4.254" width="0" layer="150"/>
<wire x1="13.081" y1="4.413" x2="12.446" y2="4.254" width="0" layer="150"/>
<wire x1="12.446" y1="4.254" x2="13.081" y2="4.096" width="0" layer="150"/>
<wire x1="13.081" y1="4.096" x2="12.954" y2="4.254" width="0" layer="150"/>
<wire x1="12.954" y1="4.254" x2="13.081" y2="4.413" width="0" layer="150"/>
<wire x1="12.446" y1="4.254" x2="13.018" y2="4.175" width="0" layer="150"/>
<wire x1="13.018" y1="4.175" x2="12.954" y2="4.254" width="0" layer="150"/>
<wire x1="12.954" y1="4.254" x2="12.446" y2="4.254" width="0" layer="150"/>
<wire x1="12.446" y1="4.254" x2="13.018" y2="4.334" width="0" layer="150"/>
<wire x1="13.018" y1="4.334" x2="13.081" y2="4.413" width="0" layer="150"/>
<wire x1="13.782" y1="3.429" x2="14.798" y2="3.429" width="0" layer="150"/>
<wire x1="14.097" y1="2.35" x2="14.097" y2="3.429" width="0" layer="150"/>
<wire x1="14.256" y1="2.794" x2="14.097" y2="3.429" width="0" layer="150"/>
<wire x1="14.097" y1="3.429" x2="13.938" y2="2.794" width="0" layer="150"/>
<wire x1="13.938" y1="2.794" x2="14.097" y2="2.921" width="0" layer="150"/>
<wire x1="14.097" y1="2.921" x2="14.256" y2="2.794" width="0" layer="150"/>
<wire x1="14.097" y1="3.429" x2="14.018" y2="2.858" width="0" layer="150"/>
<wire x1="14.018" y1="2.858" x2="14.097" y2="2.921" width="0" layer="150"/>
<wire x1="14.097" y1="2.921" x2="14.097" y2="3.429" width="0" layer="150"/>
<wire x1="14.097" y1="3.429" x2="14.176" y2="2.858" width="0" layer="150"/>
<wire x1="14.176" y1="2.858" x2="14.256" y2="2.794" width="0" layer="150"/>
<text x="-19.583" y="-0.127" size="0.254" layer="150">3.429</text>
<text x="-17.742" y="-0.127" size="0.254" layer="150">4.064</text>
<text x="-14.556" y="-2.845" size="1.27" layer="21">1</text>
<text x="-14.556" y="-2.845" size="1.27" layer="51">1</text>
<text x="-14.556" y="1.575" size="1.27" layer="21">2</text>
<text x="-14.556" y="1.575" size="1.27" layer="51">2</text>
<text x="-14.694" y="4.826" size="0.254" layer="150">1.270</text>
<text x="-0.8" y="1.143" size="0.254" layer="150">25.400</text>
<text x="-0.8" y="5.842" size="0.254" layer="150">24.130</text>
<text x="13.756" y="-2.845" size="1.27" layer="21">39</text>
<text x="13.756" y="-2.845" size="1.27" layer="51">39</text>
<text x="13.756" y="1.575" size="1.27" layer="21">40</text>
<text x="13.756" y="1.575" size="1.27" layer="51">40</text>
<text x="13.437" y="1.905" size="0.254" layer="150">2.794</text>
<text x="13.754" y="4.128" size="0.254" layer="150">0.762</text>
<text x="-13.756" y="-5.552" size="1.905" layer="25" align="bottom-right">&gt;NAME</text>
<text x="0" y="-0.7" size="0.635" layer="27" align="bottom-center">&gt;VALUE</text>
<rectangle x1="-12.486" y1="-3.478" x2="-11.644" y2="-0.586" layer="29"/>
<rectangle x1="-12.435" y1="-3.427" x2="-11.695" y2="-0.637" layer="31"/>
<rectangle x1="-12.486" y1="0.586" x2="-11.644" y2="3.478" layer="29"/>
<rectangle x1="-12.435" y1="0.637" x2="-11.695" y2="3.427" layer="31"/>
<rectangle x1="-11.216" y1="-3.478" x2="-10.374" y2="-0.586" layer="29"/>
<rectangle x1="-11.165" y1="-3.427" x2="-10.425" y2="-0.637" layer="31"/>
<rectangle x1="-11.216" y1="0.586" x2="-10.374" y2="3.478" layer="29"/>
<rectangle x1="-11.165" y1="0.637" x2="-10.425" y2="3.427" layer="31"/>
<rectangle x1="-9.946" y1="-3.478" x2="-9.104" y2="-0.586" layer="29"/>
<rectangle x1="-9.895" y1="-3.427" x2="-9.155" y2="-0.637" layer="31"/>
<rectangle x1="-9.946" y1="0.586" x2="-9.104" y2="3.478" layer="29"/>
<rectangle x1="-9.895" y1="0.637" x2="-9.155" y2="3.427" layer="31"/>
<rectangle x1="-8.676" y1="-3.478" x2="-7.834" y2="-0.586" layer="29"/>
<rectangle x1="-8.625" y1="-3.427" x2="-7.885" y2="-0.637" layer="31"/>
<rectangle x1="-8.676" y1="0.586" x2="-7.834" y2="3.478" layer="29"/>
<rectangle x1="-8.625" y1="0.637" x2="-7.885" y2="3.427" layer="31"/>
<rectangle x1="-7.406" y1="-3.478" x2="-6.564" y2="-0.586" layer="29"/>
<rectangle x1="-7.355" y1="-3.427" x2="-6.615" y2="-0.637" layer="31"/>
<rectangle x1="-7.406" y1="0.586" x2="-6.564" y2="3.478" layer="29"/>
<rectangle x1="-7.355" y1="0.637" x2="-6.615" y2="3.427" layer="31"/>
<rectangle x1="-6.136" y1="-3.478" x2="-5.294" y2="-0.586" layer="29"/>
<rectangle x1="-6.085" y1="-3.427" x2="-5.345" y2="-0.637" layer="31"/>
<rectangle x1="-6.136" y1="0.586" x2="-5.294" y2="3.478" layer="29"/>
<rectangle x1="-6.085" y1="0.637" x2="-5.345" y2="3.427" layer="31"/>
<rectangle x1="-4.866" y1="-3.478" x2="-4.024" y2="-0.586" layer="29"/>
<rectangle x1="-4.815" y1="-3.427" x2="-4.075" y2="-0.637" layer="31"/>
<rectangle x1="-4.866" y1="0.586" x2="-4.024" y2="3.478" layer="29"/>
<rectangle x1="-4.815" y1="0.637" x2="-4.075" y2="3.427" layer="31"/>
<rectangle x1="-3.596" y1="-3.478" x2="-2.754" y2="-0.586" layer="29"/>
<rectangle x1="-3.545" y1="-3.427" x2="-2.805" y2="-0.637" layer="31"/>
<rectangle x1="-3.596" y1="0.586" x2="-2.754" y2="3.478" layer="29"/>
<rectangle x1="-3.545" y1="0.637" x2="-2.805" y2="3.427" layer="31"/>
<rectangle x1="-2.326" y1="-3.478" x2="-1.484" y2="-0.586" layer="29"/>
<rectangle x1="-2.275" y1="-3.427" x2="-1.535" y2="-0.637" layer="31"/>
<rectangle x1="-2.326" y1="0.586" x2="-1.484" y2="3.478" layer="29"/>
<rectangle x1="-2.275" y1="0.637" x2="-1.535" y2="3.427" layer="31"/>
<rectangle x1="-1.056" y1="-3.478" x2="-0.214" y2="-0.586" layer="29"/>
<rectangle x1="-1.005" y1="-3.427" x2="-0.265" y2="-0.637" layer="31"/>
<rectangle x1="-1.056" y1="0.586" x2="-0.214" y2="3.478" layer="29"/>
<rectangle x1="-1.005" y1="0.637" x2="-0.265" y2="3.427" layer="31"/>
<rectangle x1="0.214" y1="-3.478" x2="1.056" y2="-0.586" layer="29"/>
<rectangle x1="0.265" y1="-3.427" x2="1.005" y2="-0.637" layer="31"/>
<rectangle x1="0.214" y1="0.586" x2="1.056" y2="3.478" layer="29"/>
<rectangle x1="0.265" y1="0.637" x2="1.005" y2="3.427" layer="31"/>
<rectangle x1="1.484" y1="-3.478" x2="2.326" y2="-0.586" layer="29"/>
<rectangle x1="1.535" y1="-3.427" x2="2.275" y2="-0.637" layer="31"/>
<rectangle x1="1.484" y1="0.586" x2="2.326" y2="3.478" layer="29"/>
<rectangle x1="1.535" y1="0.637" x2="2.275" y2="3.427" layer="31"/>
<rectangle x1="2.754" y1="-3.478" x2="3.596" y2="-0.586" layer="29"/>
<rectangle x1="2.805" y1="-3.427" x2="3.545" y2="-0.637" layer="31"/>
<rectangle x1="2.754" y1="0.586" x2="3.596" y2="3.478" layer="29"/>
<rectangle x1="2.805" y1="0.637" x2="3.545" y2="3.427" layer="31"/>
<rectangle x1="4.024" y1="-3.478" x2="4.866" y2="-0.586" layer="29"/>
<rectangle x1="4.075" y1="-3.427" x2="4.815" y2="-0.637" layer="31"/>
<rectangle x1="4.024" y1="0.586" x2="4.866" y2="3.478" layer="29"/>
<rectangle x1="4.075" y1="0.637" x2="4.815" y2="3.427" layer="31"/>
<rectangle x1="5.294" y1="-3.478" x2="6.136" y2="-0.586" layer="29"/>
<rectangle x1="5.345" y1="-3.427" x2="6.085" y2="-0.637" layer="31"/>
<rectangle x1="5.294" y1="0.586" x2="6.136" y2="3.478" layer="29"/>
<rectangle x1="5.345" y1="0.637" x2="6.085" y2="3.427" layer="31"/>
<rectangle x1="6.564" y1="-3.478" x2="7.406" y2="-0.586" layer="29"/>
<rectangle x1="6.615" y1="-3.427" x2="7.355" y2="-0.637" layer="31"/>
<rectangle x1="6.564" y1="0.586" x2="7.406" y2="3.478" layer="29"/>
<rectangle x1="6.615" y1="0.637" x2="7.355" y2="3.427" layer="31"/>
<rectangle x1="7.834" y1="-3.478" x2="8.676" y2="-0.586" layer="29"/>
<rectangle x1="7.885" y1="-3.427" x2="8.625" y2="-0.637" layer="31"/>
<rectangle x1="7.834" y1="0.586" x2="8.676" y2="3.478" layer="29"/>
<rectangle x1="7.885" y1="0.637" x2="8.625" y2="3.427" layer="31"/>
<rectangle x1="9.104" y1="-3.478" x2="9.946" y2="-0.586" layer="29"/>
<rectangle x1="9.155" y1="-3.427" x2="9.895" y2="-0.637" layer="31"/>
<rectangle x1="9.104" y1="0.586" x2="9.946" y2="3.478" layer="29"/>
<rectangle x1="9.155" y1="0.637" x2="9.895" y2="3.427" layer="31"/>
<rectangle x1="10.374" y1="-3.478" x2="11.216" y2="-0.586" layer="29"/>
<rectangle x1="10.425" y1="-3.427" x2="11.165" y2="-0.637" layer="31"/>
<rectangle x1="10.374" y1="0.586" x2="11.216" y2="3.478" layer="29"/>
<rectangle x1="10.425" y1="0.637" x2="11.165" y2="3.427" layer="31"/>
<rectangle x1="11.644" y1="-3.478" x2="12.486" y2="-0.586" layer="29"/>
<rectangle x1="11.695" y1="-3.427" x2="12.435" y2="-0.637" layer="31"/>
<rectangle x1="11.644" y1="0.586" x2="12.486" y2="3.478" layer="29"/>
<rectangle x1="11.695" y1="0.637" x2="12.435" y2="3.427" layer="31"/>
<smd name="1" x="-12.065" y="-2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="2" x="-12.065" y="2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="3" x="-10.795" y="-2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="4" x="-10.795" y="2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="5" x="-9.525" y="-2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="6" x="-9.525" y="2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="7" x="-8.255" y="-2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="8" x="-8.255" y="2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="9" x="-6.985" y="-2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="10" x="-6.985" y="2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="11" x="-5.715" y="-2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="12" x="-5.715" y="2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="13" x="-4.445" y="-2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="14" x="-4.445" y="2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="15" x="-3.175" y="-2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="16" x="-3.175" y="2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="17" x="-1.905" y="-2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="18" x="-1.905" y="2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="19" x="-0.635" y="-2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="20" x="-0.635" y="2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="21" x="0.635" y="-2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="22" x="0.635" y="2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="23" x="1.905" y="-2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="24" x="1.905" y="2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="25" x="3.175" y="-2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="26" x="3.175" y="2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="27" x="4.445" y="-2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="28" x="4.445" y="2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="29" x="5.715" y="-2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="30" x="5.715" y="2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="31" x="6.985" y="-2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="32" x="6.985" y="2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="33" x="8.255" y="-2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="34" x="8.255" y="2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="35" x="9.525" y="-2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="36" x="9.525" y="2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="37" x="10.795" y="-2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="38" x="10.795" y="2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="39" x="12.065" y="-2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
<smd name="40" x="12.065" y="2.032" dx="0.74" dy="2.79" layer="1" stop="no"/>
</package>
</packages>
<symbols>
<symbol name="SAMTEC-FTSH-120-01-X-DVA">
<text x="0" y="5.08" size="2.54" layer="95">&gt;NAME</text>
<wire x1="0" y1="5.08" x2="20.3" y2="5.08" width="0.254" layer="94"/>
<wire x1="20.3" y1="5.08" x2="20.3" y2="-53.34" width="0.254" layer="94"/>
<wire x1="20.3" y1="-53.34" x2="0" y2="-53.34" width="0.254" layer="94"/>
<wire x1="0" y1="-53.34" x2="0" y2="5.08" width="0.254" layer="94"/>
<text x="0" y="-55.88" size="2.286" layer="96">&gt;PARTNO</text>
<text x="0" y="-58.42" size="2.286" layer="96">&gt;VALUE</text>
<text x="0" y="-60.96" size="2.286" layer="94">SAMTEC-FTSH-120-01-X-DV</text>
<pin name="1" x="-5.08" y="0" length="middle" direction="pas"/>
<pin name="2" x="25.4" y="0" length="middle" direction="pas" rot="R180"/>
<pin name="3" x="-5.08" y="-2.54" length="middle" direction="pas"/>
<pin name="4" x="25.4" y="-2.54" length="middle" direction="pas" rot="R180"/>
<pin name="5" x="-5.08" y="-5.08" length="middle" direction="pas"/>
<pin name="6" x="25.4" y="-5.08" length="middle" direction="pas" rot="R180"/>
<pin name="7" x="-5.08" y="-7.62" length="middle" direction="pas"/>
<pin name="8" x="25.4" y="-7.62" length="middle" direction="pas" rot="R180"/>
<pin name="9" x="-5.08" y="-10.16" length="middle" direction="pas"/>
<pin name="10" x="25.4" y="-10.16" length="middle" direction="pas" rot="R180"/>
<pin name="11" x="-5.08" y="-12.7" length="middle" direction="pas"/>
<pin name="12" x="25.4" y="-12.7" length="middle" direction="pas" rot="R180"/>
<pin name="13" x="-5.08" y="-15.24" length="middle" direction="pas"/>
<pin name="14" x="25.4" y="-15.24" length="middle" direction="pas" rot="R180"/>
<pin name="15" x="-5.08" y="-17.78" length="middle" direction="pas"/>
<pin name="16" x="25.4" y="-17.78" length="middle" direction="pas" rot="R180"/>
<pin name="17" x="-5.08" y="-20.32" length="middle" direction="pas"/>
<pin name="18" x="25.4" y="-20.32" length="middle" direction="pas" rot="R180"/>
<pin name="19" x="-5.08" y="-22.86" length="middle" direction="pas"/>
<pin name="20" x="25.4" y="-22.86" length="middle" direction="pas" rot="R180"/>
<pin name="21" x="-5.08" y="-25.4" length="middle" direction="pas"/>
<pin name="22" x="25.4" y="-25.4" length="middle" direction="pas" rot="R180"/>
<pin name="23" x="-5.08" y="-27.94" length="middle" direction="pas"/>
<pin name="24" x="25.4" y="-27.94" length="middle" direction="pas" rot="R180"/>
<pin name="25" x="-5.08" y="-30.48" length="middle" direction="pas"/>
<pin name="26" x="25.4" y="-30.48" length="middle" direction="pas" rot="R180"/>
<pin name="27" x="-5.08" y="-33.02" length="middle" direction="pas"/>
<pin name="28" x="25.4" y="-33.02" length="middle" direction="pas" rot="R180"/>
<pin name="29" x="-5.08" y="-35.56" length="middle" direction="pas"/>
<pin name="30" x="25.4" y="-35.56" length="middle" direction="pas" rot="R180"/>
<pin name="31" x="-5.08" y="-38.1" length="middle" direction="pas"/>
<pin name="32" x="25.4" y="-38.1" length="middle" direction="pas" rot="R180"/>
<pin name="33" x="-5.08" y="-40.64" length="middle" direction="pas"/>
<pin name="34" x="25.4" y="-40.64" length="middle" direction="pas" rot="R180"/>
<pin name="35" x="-5.08" y="-43.18" length="middle" direction="pas"/>
<pin name="36" x="25.4" y="-43.18" length="middle" direction="pas" rot="R180"/>
<pin name="37" x="-5.08" y="-45.72" length="middle" direction="pas"/>
<pin name="38" x="25.4" y="-45.72" length="middle" direction="pas" rot="R180"/>
<pin name="39" x="-5.08" y="-48.26" length="middle" direction="pas"/>
<pin name="40" x="25.4" y="-48.26" length="middle" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SAMTEC-FTSH-120-01-X-DV" prefix="J">
<description> &lt;a href="https://pricing.snapeda.com/search/part/FTSH-120-01-F-DV/?ref=eda"&gt;Check prices&lt;/a&gt;</description>
<gates>
<gate name="A" symbol="SAMTEC-FTSH-120-01-X-DVA" x="0" y="0"/>
</gates>
<devices>
<device name="J" package="SAMTEC-FTSH-120-01-X-DV">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="17" pad="17"/>
<connect gate="A" pin="18" pad="18"/>
<connect gate="A" pin="19" pad="19"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="20" pad="20"/>
<connect gate="A" pin="21" pad="21"/>
<connect gate="A" pin="22" pad="22"/>
<connect gate="A" pin="23" pad="23"/>
<connect gate="A" pin="24" pad="24"/>
<connect gate="A" pin="25" pad="25"/>
<connect gate="A" pin="26" pad="26"/>
<connect gate="A" pin="27" pad="27"/>
<connect gate="A" pin="28" pad="28"/>
<connect gate="A" pin="29" pad="29"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="30" pad="30"/>
<connect gate="A" pin="31" pad="31"/>
<connect gate="A" pin="32" pad="32"/>
<connect gate="A" pin="33" pad="33"/>
<connect gate="A" pin="34" pad="34"/>
<connect gate="A" pin="35" pad="35"/>
<connect gate="A" pin="36" pad="36"/>
<connect gate="A" pin="37" pad="37"/>
<connect gate="A" pin="38" pad="38"/>
<connect gate="A" pin="39" pad="39"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="40" pad="40"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Bad"/>
<attribute name="DESCRIPTION" value=" Connector Header Surface Mount 40 position 0.050 (1.27mm) "/>
<attribute name="MF" value="Samtec"/>
<attribute name="MP" value="FTSH-120-01-F-DV"/>
<attribute name="PACKAGE" value="None"/>
<attribute name="PRICE" value="None"/>
<attribute name="PURCHASE-URL" value="https://pricing.snapeda.com/search/part/FTSH-120-01-F-DV/?ref=eda"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ams1117-3.3v">
<description>&lt;1A LOW DROPOUT VOLTAGE REGULATOR, SOT-223&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="SOT229P700X180-4N">
<description>&lt;b&gt;3 Lead SOT-223 Plastic Package&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-3.35" y="2.29" dx="1.3" dy="0.95" layer="1"/>
<smd name="2" x="-3.35" y="0" dx="1.3" dy="0.95" layer="1"/>
<smd name="3" x="-3.35" y="-2.29" dx="1.3" dy="0.95" layer="1"/>
<smd name="4" x="3.35" y="0" dx="3.25" dy="1.3" layer="1" rot="R90"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-4.25" y1="3.605" x2="4.25" y2="3.605" width="0.05" layer="51"/>
<wire x1="4.25" y1="3.605" x2="4.25" y2="-3.605" width="0.05" layer="51"/>
<wire x1="4.25" y1="-3.605" x2="-4.25" y2="-3.605" width="0.05" layer="51"/>
<wire x1="-4.25" y1="-3.605" x2="-4.25" y2="3.605" width="0.05" layer="51"/>
<wire x1="-1.752" y1="3.252" x2="1.752" y2="3.252" width="0.1" layer="51"/>
<wire x1="1.752" y1="3.252" x2="1.752" y2="-3.252" width="0.1" layer="51"/>
<wire x1="1.752" y1="-3.252" x2="-1.752" y2="-3.252" width="0.1" layer="51"/>
<wire x1="-1.752" y1="-3.252" x2="-1.752" y2="3.252" width="0.1" layer="51"/>
<wire x1="-1.752" y1="0.962" x2="0.538" y2="3.252" width="0.1" layer="51"/>
<wire x1="-1.752" y1="3.252" x2="1.752" y2="3.252" width="0.2" layer="21"/>
<wire x1="1.752" y1="3.252" x2="1.752" y2="-3.252" width="0.2" layer="21"/>
<wire x1="1.752" y1="-3.252" x2="-1.752" y2="-3.252" width="0.2" layer="21"/>
<wire x1="-1.752" y1="-3.252" x2="-1.752" y2="3.252" width="0.2" layer="21"/>
<wire x1="-4" y1="3.115" x2="-2.7" y2="3.115" width="0.2" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="AMS1117-3.3V">
<wire x1="5.08" y1="2.54" x2="45.72" y2="2.54" width="0.254" layer="94"/>
<wire x1="45.72" y1="-7.62" x2="45.72" y2="2.54" width="0.254" layer="94"/>
<wire x1="45.72" y1="-7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<text x="46.99" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="46.99" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="GROUND/ADJUST" x="0" y="0" length="middle"/>
<pin name="VOUT" x="0" y="-2.54" length="middle"/>
<pin name="VIN" x="0" y="-5.08" length="middle"/>
<pin name="TAB_(OUTPUT)" x="50.8" y="-2.54" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AMS1117-3.3V" prefix="IC">
<description>&lt;b&gt;1A LOW DROPOUT VOLTAGE REGULATOR, SOT-223&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.advanced-monolithic.com/pdf/ds1117.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="AMS1117-3.3V" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT229P700X180-4N">
<connects>
<connect gate="G$1" pin="GROUND/ADJUST" pad="1"/>
<connect gate="G$1" pin="TAB_(OUTPUT)" pad="4"/>
<connect gate="G$1" pin="VIN" pad="3"/>
<connect gate="G$1" pin="VOUT" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="1A LOW DROPOUT VOLTAGE REGULATOR, SOT-223" constant="no"/>
<attribute name="HEIGHT" value="1.8mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Advanced Monolithic Systems" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="ams1117-3.3v" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE-STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="VCC" urn="urn:adsk.eagle:symbol:26928/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="VCC" urn="urn:adsk.eagle:component:26957/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ngspice-simulation" urn="urn:adsk.eagle:library:527439">
<description>SPICE compatible library parts</description>
<packages>
</packages>
<symbols>
<symbol name="0" urn="urn:adsk.eagle:symbol:527455/1" library_version="18">
<description>Simulation ground symbol (spice node 0)</description>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<pin name="0" x="0" y="0" visible="off" length="point" direction="sup"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:527478/1" prefix="X_" library_version="18">
<description>Simulation ground symbol (spice node 0)</description>
<gates>
<gate name="G$1" symbol="0" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="SPICEGROUND" value=""/>
<attribute name="_EXTERNAL_" value=""/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-molex" urn="urn:adsk.eagle:library:165">
<description>&lt;b&gt;Molex Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="22-23-2021" urn="urn:adsk.eagle:footprint:8078259/1" library_version="3">
<description>&lt;b&gt;KK® 254 Solid Header, Vertical, with Friction Lock, 2 Circuits, Tin (Sn) Plating&lt;/b&gt;&lt;p&gt;&lt;a href =http://www.molex.com/pdm_docs/sd/022232021_sd.pdf&gt;Datasheet &lt;/a&gt;</description>
<wire x1="-2.54" y1="3.175" x2="2.54" y2="3.175" width="0.254" layer="21"/>
<wire x1="2.54" y1="3.175" x2="2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-3.175" width="0.254" layer="21"/>
<wire x1="2.54" y1="-3.175" x2="-2.54" y2="-3.175" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-3.175" x2="-2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="3.175" width="0.254" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="1" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1" shape="long" rot="R90"/>
<text x="-2.54" y="3.81" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="22-23-2021" urn="urn:adsk.eagle:package:8078633/1" type="box" library_version="3">
<description>&lt;b&gt;KK® 254 Solid Header, Vertical, with Friction Lock, 2 Circuits, Tin (Sn) Plating&lt;/b&gt;&lt;p&gt;&lt;a href =http://www.molex.com/pdm_docs/sd/022232021_sd.pdf&gt;Datasheet &lt;/a&gt;</description>
<packageinstances>
<packageinstance name="22-23-2021"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="MV" urn="urn:adsk.eagle:symbol:8078125/1" library_version="3">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="-0.762" y="1.397" size="1.778" layer="96">&gt;VALUE</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
<symbol name="M" urn="urn:adsk.eagle:symbol:8078124/1" library_version="3">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="22-23-2021" urn="urn:adsk.eagle:component:8078938/1" prefix="X" library_version="3">
<description>.100" (2.54mm) Center Header - 2 Pin</description>
<gates>
<gate name="-1" symbol="MV" x="0" y="0" addlevel="always" swaplevel="1"/>
<gate name="-2" symbol="M" x="0" y="-2.54" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="" package="22-23-2021">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8078633/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="MOLEX" constant="no"/>
<attribute name="MPN" value="22-23-2021" constant="no"/>
<attribute name="OC_FARNELL" value="1462926" constant="no"/>
<attribute name="OC_NEWARK" value="25C3832" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="adafruit">
<packages>
<package name="1206">
<description>&lt;b&gt;CHICAGO MINIATURE LAMP, INC.&lt;/b&gt;&lt;p&gt;
7022X Series SMT LEDs 1206 Package Size</description>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.5" x2="0.55" y2="-0.5" width="0.1016" layer="21" curve="95.452622" cap="flat"/>
<wire x1="-0.55" y1="-0.5" x2="-0.55" y2="0.5" width="0.1016" layer="51" curve="-84.547378" cap="flat"/>
<wire x1="-0.55" y1="0.5" x2="0.55" y2="0.5" width="0.1016" layer="21" curve="-95.452622" cap="flat"/>
<wire x1="0.55" y1="0.5" x2="0.55" y2="-0.5" width="0.1016" layer="51" curve="-84.547378" cap="flat"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
<rectangle x1="0.45" y1="-0.7" x2="0.6" y2="-0.45" layer="21"/>
</package>
<package name="LD260">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, square, Siemens</description>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="-1.27" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.889" x2="1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="0.889" width="0.1524" layer="51"/>
<wire x1="0" y1="1.27" x2="0.9917" y2="0.7934" width="0.1524" layer="21" curve="-51.33923" cap="flat"/>
<wire x1="-0.9917" y1="0.7934" x2="0" y2="1.27" width="0.1524" layer="21" curve="-51.33923" cap="flat"/>
<wire x1="0" y1="-1.27" x2="0.9917" y2="-0.7934" width="0.1524" layer="21" curve="51.33923" cap="flat"/>
<wire x1="-0.9917" y1="-0.7934" x2="0" y2="-1.27" width="0.1524" layer="21" curve="51.33923" cap="flat"/>
<wire x1="0.9558" y1="-0.8363" x2="1.27" y2="0" width="0.1524" layer="51" curve="41.185419" cap="flat"/>
<wire x1="0.9756" y1="0.813" x2="1.2699" y2="0" width="0.1524" layer="51" curve="-39.806332" cap="flat"/>
<wire x1="-1.27" y1="0" x2="-0.9643" y2="-0.8265" width="0.1524" layer="51" curve="40.600331" cap="flat"/>
<wire x1="-1.27" y1="0" x2="-0.9643" y2="0.8265" width="0.1524" layer="51" curve="-40.600331" cap="flat"/>
<wire x1="-0.889" y1="0" x2="0" y2="0.889" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="-0.508" y1="0" x2="0" y2="0.508" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-0.508" x2="0.508" y2="0" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="0" y1="-0.889" x2="0.889" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="1.4732" size="1.016" layer="25" ratio="14">&gt;NAME</text>
<text x="-1.27" y="-2.4892" size="1.016" layer="27" ratio="14">&gt;VALUE</text>
<rectangle x1="1.27" y1="-0.635" x2="2.032" y2="0.635" layer="51"/>
<rectangle x1="1.905" y1="-0.635" x2="2.032" y2="0.635" layer="21"/>
</package>
<package name="LED2X5">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
2 x 5 mm, rectangle</description>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="0" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0.381" x2="-0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0.381" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="-0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0.381" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-0.254" x2="1.143" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.762" x2="1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.762" x2="0.9398" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="0.9398" y1="-0.6096" x2="1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.397" y1="-0.254" x2="1.651" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-0.762" x2="1.4478" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="1.4478" y1="-0.6096" x2="1.651" y2="-0.508" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.016" layer="25" ratio="14">&gt;NAME</text>
<text x="-2.54" y="-2.413" size="1.016" layer="27" ratio="14">&gt;VALUE</text>
<rectangle x1="2.159" y1="-1.27" x2="2.413" y2="1.27" layer="21"/>
</package>
<package name="LED3MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
3 mm, round</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="-1.524" y1="0" x2="-1.1708" y2="0.9756" width="0.1524" layer="51" curve="-39.80361" cap="flat"/>
<wire x1="-1.524" y1="0" x2="-1.1391" y2="-1.0125" width="0.1524" layer="51" curve="41.633208" cap="flat"/>
<wire x1="1.1571" y1="0.9918" x2="1.524" y2="0" width="0.1524" layer="51" curve="-40.601165" cap="flat"/>
<wire x1="1.1708" y1="-0.9756" x2="1.524" y2="0" width="0.1524" layer="51" curve="39.80361" cap="flat"/>
<wire x1="0" y1="1.524" x2="1.2401" y2="0.8858" width="0.1524" layer="21" curve="-54.461337" cap="flat"/>
<wire x1="-1.2192" y1="0.9144" x2="0" y2="1.524" width="0.1524" layer="21" curve="-53.130102" cap="flat"/>
<wire x1="0" y1="-1.524" x2="1.203" y2="-0.9356" width="0.1524" layer="21" curve="52.126876" cap="flat"/>
<wire x1="-1.203" y1="-0.9356" x2="0" y2="-1.524" width="0.1524" layer="21" curve="52.126876" cap="flat"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="-2.5908" y1="1.7272" x2="-1.8542" y2="1.7272" width="0.127" layer="21"/>
<wire x1="-2.2352" y1="1.3208" x2="-2.2352" y2="2.1082" width="0.127" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED5MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, round</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LSU260">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
1 mm, round, Siemens</description>
<wire x1="0" y1="-0.508" x2="-1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.508" x2="-1.143" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="0.508" x2="0" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.254" x2="-1.397" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.254" x2="-1.143" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="-0.254" x2="-1.397" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="0.254" x2="-1.143" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="0.254" x2="-1.143" y2="0.508" width="0.1524" layer="51"/>
<wire x1="0.508" y1="-0.254" x2="1.397" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="1.397" y1="-0.254" x2="1.397" y2="0.254" width="0.1524" layer="51"/>
<wire x1="1.397" y1="0.254" x2="0.508" y2="0.254" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-0.381" x2="0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.508" x2="-0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="-0.381" x2="-0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0.381" x2="0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.508" x2="-0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0.381" x2="-0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.254" x2="0.254" y2="0" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="-0.254" y1="0" x2="0" y2="0.254" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="0.381" y1="-0.381" x2="0.381" y2="0.381" width="0.1524" layer="21" curve="90"/>
<circle x="0" y="0" radius="0.508" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="0.8382" size="1.016" layer="25" ratio="14">&gt;NAME</text>
<text x="-1.27" y="-1.8542" size="1.016" layer="27" ratio="14">&gt;VALUE</text>
<rectangle x1="-1.397" y1="-0.254" x2="-1.143" y2="0.254" layer="51"/>
<rectangle x1="0.508" y1="-0.254" x2="1.397" y2="0.254" layer="51"/>
</package>
<package name="LZR181">
<description>&lt;B&gt;LED BLOCK&lt;/B&gt;&lt;p&gt;
1 LED, Siemens</description>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.889" x2="1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0" x2="0" y2="0.889" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="-0.508" y1="0" x2="0" y2="0.508" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-0.508" x2="0.508" y2="0" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="0" y1="-0.889" x2="0.889" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-0.8678" y1="0.7439" x2="0" y2="1.143" width="0.1524" layer="21" curve="-49.396139" cap="flat"/>
<wire x1="0" y1="1.143" x2="0.8678" y2="0.7439" width="0.1524" layer="21" curve="-49.396139" cap="flat"/>
<wire x1="-0.8678" y1="-0.7439" x2="0" y2="-1.143" width="0.1524" layer="21" curve="49.396139" cap="flat"/>
<wire x1="0" y1="-1.143" x2="0.8678" y2="-0.7439" width="0.1524" layer="21" curve="49.396139" cap="flat"/>
<wire x1="0.8678" y1="0.7439" x2="1.143" y2="0" width="0.1524" layer="51" curve="-40.604135" cap="flat"/>
<wire x1="0.8678" y1="-0.7439" x2="1.143" y2="0" width="0.1524" layer="51" curve="40.604135" cap="flat"/>
<wire x1="-1.143" y1="0" x2="-0.8678" y2="0.7439" width="0.1524" layer="51" curve="-40.604135" cap="flat"/>
<wire x1="-1.143" y1="0" x2="-0.8678" y2="-0.7439" width="0.1524" layer="51" curve="40.604135" cap="flat"/>
<wire x1="-1.27" y1="1.27" x2="1.27" y2="1.27" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="1.4732" size="1.016" layer="25" ratio="14">&gt;NAME</text>
<text x="-1.27" y="-2.4892" size="1.016" layer="27" ratio="14">&gt;VALUE</text>
<rectangle x1="1.27" y1="-0.889" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.27" y2="0.254" layer="51"/>
</package>
<package name="Q62902-B152">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="2.9718" y1="-1.8542" x2="-2.1082" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="1.8542" x2="2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="-1.8542" x2="-2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="-1.8542" x2="-2.54" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="1.8542" x2="-2.54" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.8542" x2="-2.54" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.8542" x2="-2.9718" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.8542" x2="-2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="0.254" x2="-2.9718" y2="-0.254" width="0.1524" layer="21" curve="180"/>
<wire x1="-1.1486" y1="0.8814" x2="0" y2="1.4478" width="0.1524" layer="21" curve="-52.498642" cap="flat"/>
<wire x1="0" y1="1.4478" x2="1.1476" y2="0.8827" width="0.1524" layer="21" curve="-52.433716" cap="flat"/>
<wire x1="-1.1351" y1="-0.8987" x2="0" y2="-1.4478" width="0.1524" layer="21" curve="51.629985" cap="flat"/>
<wire x1="0" y1="-1.4478" x2="1.1305" y2="-0.9044" width="0.1524" layer="21" curve="51.339172" cap="flat"/>
<wire x1="1.1281" y1="-0.9074" x2="1.4478" y2="0" width="0.1524" layer="51" curve="38.811177" cap="flat"/>
<wire x1="1.1401" y1="0.8923" x2="1.4478" y2="0" width="0.1524" layer="51" curve="-38.048073" cap="flat"/>
<wire x1="-1.4478" y1="0" x2="-1.1305" y2="-0.9044" width="0.1524" layer="51" curve="38.659064" cap="flat"/>
<wire x1="-1.4478" y1="0" x2="-1.1456" y2="0.8853" width="0.1524" layer="51" curve="-37.696376" cap="flat"/>
<wire x1="0" y1="1.7018" x2="1.4674" y2="0.8618" width="0.1524" layer="21" curve="-59.573488" cap="flat"/>
<wire x1="-1.4618" y1="0.8714" x2="0" y2="1.7018" width="0.1524" layer="21" curve="-59.200638" cap="flat"/>
<wire x1="0" y1="-1.7018" x2="1.4571" y2="-0.8793" width="0.1524" layer="21" curve="58.891781" cap="flat"/>
<wire x1="-1.4571" y1="-0.8793" x2="0" y2="-1.7018" width="0.1524" layer="21" curve="58.891781" cap="flat"/>
<wire x1="-1.7018" y1="0" x2="-1.4447" y2="0.8995" width="0.1524" layer="51" curve="-31.907626" cap="flat"/>
<wire x1="-1.7018" y1="0" x2="-1.4502" y2="-0.8905" width="0.1524" layer="51" curve="31.551992" cap="flat"/>
<wire x1="1.4521" y1="0.8874" x2="1.7018" y2="0" width="0.1524" layer="51" curve="-31.429586" cap="flat"/>
<wire x1="1.4459" y1="-0.8975" x2="1.7018" y2="0" width="0.1524" layer="51" curve="31.828757" cap="flat"/>
<wire x1="-2.1082" y1="1.8542" x2="-2.1082" y2="0.4826" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="0.4826" x2="-2.1082" y2="-0.4826" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="-0.4826" x2="-2.1082" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="2.9718" y1="1.8542" x2="2.9718" y2="0.4826" width="0.1524" layer="21"/>
<wire x1="2.9718" y1="0.4826" x2="2.9718" y2="-0.4826" width="0.1524" layer="21"/>
<wire x1="2.9718" y1="-0.4826" x2="2.9718" y2="-1.8542" width="0.1524" layer="21"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.905" y="2.286" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.905" y="-3.556" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="Q62902-B153">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="5.5118" y1="-3.5052" x2="-4.6482" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="3.5052" x2="5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="-3.5052" x2="-5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="-3.5052" x2="-5.08" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="3.5052" x2="-5.08" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-3.5052" x2="-5.08" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-3.5052" x2="-5.5118" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="3.5052" x2="-5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="0.254" x2="-5.5118" y2="-0.254" width="0.1524" layer="21" curve="180"/>
<wire x1="-4.6482" y1="3.5052" x2="-4.6482" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="5.5118" y1="3.5052" x2="5.5118" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.254" layer="21"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.254" layer="21"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-4.191" y="3.937" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-5.08" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="Q62902-B155">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-1.27" y1="-3.048" x2="-1.27" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.048" x2="2.921" y2="3.048" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.048" x2="10.033" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-3.048" x2="2.921" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-3.048" x2="2.921" y2="3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-3.048" x2="10.033" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="3.048" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-5.207" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.54" x2="-1.27" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.54" x2="-5.207" y2="-2.54" width="0.1524" layer="21" curve="180"/>
<wire x1="-6.985" y1="0.635" x2="-6.985" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="1.397" x2="-6.096" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="1.905" x2="-5.207" y2="-1.905" width="0.1524" layer="21"/>
<pad name="K" x="7.62" y="1.27" drill="0.8128" shape="long"/>
<pad name="A" x="7.62" y="-1.27" drill="0.8128" shape="long"/>
<text x="3.302" y="-2.794" size="1.016" layer="21" ratio="14">A+</text>
<text x="3.302" y="1.778" size="1.016" layer="21" ratio="14">K-</text>
<text x="11.684" y="-2.794" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="0.635" y="-4.445" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.921" y1="1.016" x2="6.731" y2="1.524" layer="21"/>
<rectangle x1="2.921" y1="-1.524" x2="6.731" y2="-1.016" layer="21"/>
<hole x="0" y="0" drill="0.8128"/>
</package>
<package name="Q62902-B156">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="2.54" y1="3.81" x2="3.81" y2="2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="3.81" x2="-3.81" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.1524" layer="21"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-3.302" x2="-2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-3.81" x2="2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-3.302" x2="2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-3.302" x2="-2.54" y2="-3.302" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.254" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="K" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-3.81" y="4.0894" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.7846" y="-5.3594" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-3.556" y="-3.302" size="1.016" layer="21" ratio="14">+</text>
<text x="2.794" y="-3.302" size="1.016" layer="21" ratio="14">-</text>
</package>
<package name="SFH480">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking&lt;p&gt;
Inifineon</description>
<wire x1="-2.159" y1="1.524" x2="-2.794" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="0" y1="1.778" x2="1.5358" y2="0.8959" width="0.1524" layer="21" curve="-59.743278" cap="flat"/>
<wire x1="-1.5358" y1="0.8959" x2="0" y2="1.778" width="0.1524" layer="21" curve="-59.743278" cap="flat"/>
<wire x1="-1.5358" y1="-0.8959" x2="0" y2="-1.778" width="0.1524" layer="21" curve="59.743278" cap="flat"/>
<wire x1="0" y1="-1.778" x2="1.5358" y2="-0.8959" width="0.1524" layer="21" curve="59.743278" cap="flat"/>
<wire x1="1.5142" y1="0.9318" x2="1.778" y2="0" width="0.1524" layer="51" curve="-31.606487" cap="flat"/>
<wire x1="1.5" y1="-0.9546" x2="1.778" y2="0" width="0.1524" layer="51" curve="32.472615" cap="flat"/>
<wire x1="-1.778" y1="0" x2="-1.5142" y2="-0.9318" width="0.1524" layer="51" curve="31.606487" cap="flat"/>
<wire x1="-1.778" y1="0" x2="-1.5" y2="0.9546" width="0.1524" layer="51" curve="-32.472615" cap="flat"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.667" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.413" width="0.254" layer="21"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.27" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SFH482">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking&lt;p&gt;
Inifineon</description>
<wire x1="-2.159" y1="1.524" x2="-2.794" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="0" y1="1.778" x2="1.5358" y2="0.8959" width="0.1524" layer="21" curve="-59.743278" cap="flat"/>
<wire x1="-1.5358" y1="0.8959" x2="0" y2="1.778" width="0.1524" layer="21" curve="-59.743278" cap="flat"/>
<wire x1="-1.5358" y1="-0.8959" x2="0" y2="-1.778" width="0.1524" layer="21" curve="59.743278" cap="flat"/>
<wire x1="0" y1="-1.778" x2="1.5358" y2="-0.8959" width="0.1524" layer="21" curve="59.743278" cap="flat"/>
<wire x1="1.5142" y1="0.9318" x2="1.778" y2="0" width="0.1524" layer="51" curve="-31.606487" cap="flat"/>
<wire x1="1.5" y1="-0.9546" x2="1.778" y2="0" width="0.1524" layer="51" curve="32.472615" cap="flat"/>
<wire x1="-1.778" y1="0" x2="-1.5142" y2="-0.9318" width="0.1524" layer="51" curve="31.606487" cap="flat"/>
<wire x1="-1.778" y1="0" x2="-1.5" y2="0.9546" width="0.1524" layer="51" curve="-32.472615" cap="flat"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.667" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.413" width="0.254" layer="21"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.27" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="U57X32">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
rectangle, 5.7 x 3.2 mm</description>
<wire x1="-3.175" y1="1.905" x2="3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.905" x2="3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.905" x2="-3.175" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.905" x2="-3.175" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="1.397" x2="2.667" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.667" y1="-1.397" x2="2.667" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.667" y1="-1.397" x2="-2.667" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="1.397" x2="-2.667" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="2.54" y2="1.016" width="0.1524" layer="51"/>
<wire x1="2.286" y1="1.27" x2="2.286" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="0.508" x2="2.54" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-0.508" x2="2.54" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-1.016" x2="2.54" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="1.27" x2="-1.778" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.254" y1="1.27" x2="0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.762" y1="1.27" x2="0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.778" y1="1.27" x2="1.778" y2="-1.27" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="3.683" y="0.254" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.683" y="-1.524" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="IRL80A">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
IR transmitter Siemens</description>
<wire x1="0.889" y1="2.286" x2="0.889" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.889" y1="1.778" x2="0.889" y2="0.762" width="0.1524" layer="51"/>
<wire x1="0.889" y1="0.762" x2="0.889" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-0.635" x2="0.889" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-1.778" x2="0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-2.286" x2="-0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="2.286" x2="-0.889" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.778" x2="-0.889" y2="0.762" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0.762" x2="-0.889" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-0.762" x2="-0.889" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-1.778" x2="-0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="2.286" x2="0.889" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-0.762" x2="-0.889" y2="0.762" width="0.1524" layer="21" curve="-180"/>
<wire x1="-1.397" y1="0.254" x2="-1.397" y2="-0.254" width="0.0508" layer="21"/>
<wire x1="-1.143" y1="0.508" x2="-1.143" y2="-0.508" width="0.0508" layer="21"/>
<pad name="K" x="0" y="1.27" drill="0.8128" shape="octagon"/>
<pad name="A" x="0" y="-1.27" drill="0.8128" shape="octagon"/>
<text x="1.27" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.27" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="P-LCC-2">
<description>&lt;b&gt;TOPLED® High-optical Power LED (HOP)&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... ls_t675.pdf</description>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-0.65" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="1.8" x2="1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="1.8" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-1.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.8" x2="1.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="-1.8" x2="1.1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-0.65" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<smd name="C" x="0" y="-2.75" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.75" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-0.635" y="2.54" size="1.27" layer="21">A</text>
<text x="-0.635" y="-3.81" size="1.27" layer="21">C</text>
<rectangle x1="-1.3" y1="-2.25" x2="1.3" y2="-0.75" layer="31"/>
<rectangle x1="-1.3" y1="0.75" x2="1.3" y2="2.25" layer="31"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="21"/>
<rectangle x1="-1.4" y1="0.65" x2="1.4" y2="2.35" layer="29"/>
<rectangle x1="-1.4" y1="-2.35" x2="1.4" y2="-0.65" layer="29"/>
</package>
<package name="OSRAM-MINI-TOP-LED">
<description>&lt;b&gt;BLUE LINETM Hyper Mini TOPLED® Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LB M676.pdf</description>
<wire x1="-0.6" y1="0.9" x2="-0.6" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-0.9" x2="-0.4" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="-0.4" y1="-0.9" x2="0.6" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="0.6" y1="-0.9" x2="0.6" y2="0.9" width="0.1016" layer="51"/>
<wire x1="0.6" y1="0.9" x2="-0.6" y2="0.9" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="0.95" x2="-0.45" y2="1.1" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="1.1" x2="0.45" y2="1.1" width="0.1016" layer="51"/>
<wire x1="0.45" y1="1.1" x2="0.45" y2="0.95" width="0.1016" layer="51"/>
<wire x1="-0.6" y1="-0.7" x2="-0.4" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-0.9" x2="-0.45" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-1.1" x2="0.45" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="0.45" y1="-1.1" x2="0.45" y2="-0.95" width="0.1016" layer="51"/>
<smd name="A" x="0" y="2.6" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="C" x="0" y="-2.6" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-0.635" y="1.905" size="1.27" layer="21">A</text>
<text x="-0.635" y="-3.175" size="1.27" layer="21">C</text>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.5" y1="0.6" x2="0.5" y2="1.4" layer="31"/>
<rectangle x1="-0.5" y1="-1.4" x2="0.5" y2="-0.6" layer="31"/>
<rectangle x1="-0.15" y1="-0.6" x2="0.15" y2="-0.3" layer="51"/>
<rectangle x1="-0.45" y1="0.65" x2="0.45" y2="1.35" layer="31"/>
<rectangle x1="-0.45" y1="-1.35" x2="0.45" y2="-0.65" layer="31"/>
</package>
<package name="OSRAM-SIDELED">
<description>&lt;b&gt;Super SIDELED® High-Current LED&lt;/b&gt;&lt;p&gt;
LG A672, LP A672 &lt;br&gt;
Source: http://www.osram.convergy.de/ ... LG_LP_A672.pdf (2004.05.13)</description>
<wire x1="-1.85" y1="-2.05" x2="-1.85" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="-0.75" x2="-1.7" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="-0.75" x2="-1.7" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="0.75" x2="-1.85" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="0.75" x2="-1.85" y2="2.05" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="2.05" x2="0.9" y2="2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="2.05" x2="0.9" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="-2.05" x2="-1.85" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="-2.05" x2="1.05" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="-2.05" x2="1.85" y2="-1.85" width="0.1016" layer="51"/>
<wire x1="1.85" y1="-1.85" x2="1.85" y2="1.85" width="0.1016" layer="51"/>
<wire x1="1.85" y1="1.85" x2="1.05" y2="2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="2.05" x2="0.9" y2="2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="2.05" x2="1.05" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.9" x2="-0.55" y2="0.9" width="0.1016" layer="51" curve="-167.319617"/>
<wire x1="-0.55" y1="-0.9" x2="0.85" y2="-1.2" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.55" y1="0.9" x2="0.85" y2="1.2" width="0.1016" layer="51" style="shortdash"/>
<smd name="C" x="0" y="-2.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="0.635" y="-3.175" size="1.27" layer="21" rot="R90">C</text>
<text x="0.635" y="2.54" size="1.27" layer="21" rot="R90">A</text>
<text x="-2.54" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.1" y1="-2.2" x2="2.1" y2="-0.4" layer="29"/>
<rectangle x1="-2.1" y1="0.4" x2="2.1" y2="2.2" layer="29"/>
<rectangle x1="-1.9" y1="-2.1" x2="1.9" y2="-0.6" layer="31"/>
<rectangle x1="-1.9" y1="0.6" x2="1.9" y2="2.1" layer="31"/>
<rectangle x1="-1.85" y1="-2.05" x2="-0.7" y2="-1" layer="51"/>
</package>
<package name="SMART-LED">
<description>&lt;b&gt;SmartLEDTM Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY L896.pdf</description>
<wire x1="-0.35" y1="0.6" x2="0.35" y2="0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.35" y1="0.6" x2="0.35" y2="-0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.6" x2="-0.35" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.35" y1="-0.6" x2="-0.35" y2="0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.4" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<smd name="A" x="0" y="0.725" dx="0.35" dy="0.35" layer="1"/>
<smd name="B" x="0" y="-0.725" dx="0.35" dy="0.35" layer="1"/>
<text x="-0.635" y="-0.635" size="1.016" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-0.635" size="1.016" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.15" y1="-0.35" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.15" y1="0.6" x2="0.15" y2="0.85" layer="51"/>
<rectangle x1="-0.15" y1="-0.85" x2="0.15" y2="-0.6" layer="51"/>
</package>
<package name="P-LCC-2-TOPLED-RG">
<description>&lt;b&gt;Hyper TOPLED® RG Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY T776.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="2.45" width="0.1016" layer="51"/>
<wire x1="1.1" y1="2.45" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-1.1" y2="-2.45" width="0.1016" layer="51"/>
<wire x1="1.1" y1="-2.45" x2="1.1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="21"/>
<smd name="C" x="0" y="-3.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="3.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-0.635" y="3.29" size="1.27" layer="21">A</text>
<text x="-0.635" y="-4.56" size="1.27" layer="21">C</text>
<rectangle x1="-1.3" y1="-3" x2="1.3" y2="-1.5" layer="31"/>
<rectangle x1="-1.3" y1="1.5" x2="1.3" y2="3" layer="31"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="21"/>
<rectangle x1="-1.15" y1="2.4" x2="1.15" y2="2.7" layer="51"/>
<rectangle x1="-1.15" y1="-2.7" x2="1.15" y2="-2.4" layer="51"/>
<rectangle x1="-1.5" y1="1.5" x2="1.5" y2="3.2" layer="29"/>
<rectangle x1="-1.5" y1="-3.2" x2="1.5" y2="-1.5" layer="29"/>
<hole x="0" y="0" drill="2.8"/>
</package>
<package name="MICRO-SIDELED">
<description>&lt;b&gt;Hyper Micro SIDELED®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY Y876.pdf</description>
<wire x1="0.65" y1="1.1" x2="-0.1" y2="1.1" width="0.1016" layer="51"/>
<wire x1="-0.1" y1="1.1" x2="-0.35" y2="1" width="0.1016" layer="51"/>
<wire x1="-0.35" y1="1" x2="-0.35" y2="-0.9" width="0.1016" layer="21"/>
<wire x1="-0.35" y1="-0.9" x2="-0.1" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="-0.1" y1="-1.1" x2="0.65" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="0.65" y1="-1.1" x2="0.65" y2="1.1" width="0.1016" layer="21"/>
<wire x1="0.6" y1="0.9" x2="0.25" y2="0.7" width="0.0508" layer="21"/>
<wire x1="0.25" y1="0.7" x2="0.25" y2="-0.7" width="0.0508" layer="21"/>
<wire x1="0.25" y1="-0.7" x2="0.6" y2="-0.9" width="0.0508" layer="21"/>
<smd name="A" x="0" y="1.95" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<smd name="C" x="0" y="-1.95" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<text x="-1.27" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.4" y1="1.1" x2="0.4" y2="1.8" layer="29"/>
<rectangle x1="-0.4" y1="-1.8" x2="0.4" y2="-1.1" layer="29"/>
<rectangle x1="-0.35" y1="-1.75" x2="0.35" y2="-1.15" layer="31"/>
<rectangle x1="-0.35" y1="1.15" x2="0.35" y2="1.75" layer="31"/>
<rectangle x1="-0.125" y1="1.125" x2="0.125" y2="1.75" layer="51"/>
<rectangle x1="-0.125" y1="-1.75" x2="0.125" y2="-1.125" layer="51"/>
</package>
<package name="P-LCC-4">
<description>&lt;b&gt;Power TOPLED®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LA_LY E67B.pdf</description>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-0.65" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="-1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="1.8" x2="-0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="1.8" x2="-0.5" y2="1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.65" x2="0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.8" x2="1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="1.8" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.8" x2="-0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="-1.8" x2="-0.5" y2="-1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.65" x2="0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.8" x2="1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="1" y1="-1.8" x2="1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-0.65" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<smd name="A" x="-2" y="3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@3" x="2" y="3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@4" x="2" y="-3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@1" x="-2" y="-3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<text x="-3.81" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-1.905" y="-3.81" size="1.27" layer="21">C</text>
<text x="-1.905" y="2.54" size="1.27" layer="21">A</text>
<text x="1.27" y="2.54" size="1.27" layer="21">C</text>
<text x="1.27" y="-3.81" size="1.27" layer="21">C</text>
<rectangle x1="-1.15" y1="0.75" x2="-0.35" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="0.75" x2="1.15" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="-1.85" x2="1.15" y2="-0.75" layer="29"/>
<rectangle x1="-1.15" y1="-1.85" x2="-0.35" y2="-0.75" layer="29"/>
<rectangle x1="-1.1" y1="-1.8" x2="-0.4" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="-1.8" x2="1.1" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="0.8" x2="1.1" y2="1.8" layer="31"/>
<rectangle x1="-1.1" y1="0.8" x2="-0.4" y2="1.8" layer="31"/>
<rectangle x1="-0.2" y1="-0.2" x2="0.2" y2="0.2" layer="21"/>
</package>
<package name="CHIP-LED0603">
<description>&lt;b&gt;Hyper CHIPLED Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
LB Q993&lt;br&gt;
Source: http://www.osram.convergy.de/ ... Lb_q993.pdf</description>
<wire x1="-0.4" y1="0.45" x2="-0.4" y2="-0.45" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.45" x2="0.4" y2="-0.45" width="0.1016" layer="51"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.635" y="-0.635" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-0.635" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.45" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="0.45" y2="-0.45" layer="51"/>
<rectangle x1="-0.45" y1="0" x2="-0.3" y2="0.3" layer="21"/>
<rectangle x1="0.3" y1="0" x2="0.45" y2="0.3" layer="21"/>
<rectangle x1="-0.15" y1="0" x2="0.15" y2="0.3" layer="21"/>
</package>
<package name="CHIP-LED0805">
<description>&lt;b&gt;Hyper CHIPLED Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
LB R99A&lt;br&gt;
Source: http://www.osram.convergy.de/ ... lb_r99a.pdf</description>
<wire x1="-0.625" y1="0.45" x2="-0.625" y2="-0.45" width="0.1016" layer="51"/>
<wire x1="0.625" y1="0.45" x2="0.625" y2="-0.475" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.05" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="0" y="-1.05" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-1.27" y="-2.54" size="1.016" layer="21" font="vector">+</text>
<rectangle x1="-0.675" y1="0" x2="-0.525" y2="0.3" layer="21"/>
<rectangle x1="0.525" y1="0" x2="0.675" y2="0.3" layer="21"/>
<rectangle x1="-0.15" y1="0" x2="0.15" y2="0.3" layer="21"/>
<rectangle x1="-0.675" y1="0.45" x2="0.675" y2="1.05" layer="51"/>
<rectangle x1="-0.675" y1="-1.05" x2="0.675" y2="-0.45" layer="51"/>
</package>
<package name="MINI-TOPLED-SANTANA">
<description>&lt;b&gt;Mini TOPLED Santana®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG M470.pdf</description>
<wire x1="0.7" y1="-1" x2="0.35" y2="-1" width="0.1016" layer="21"/>
<wire x1="0.35" y1="-1" x2="-0.45" y2="-1" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="-1" x2="-0.7" y2="-1" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="-1" x2="-0.7" y2="-0.75" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="-0.75" x2="-0.7" y2="1" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="1" x2="0.7" y2="1" width="0.1016" layer="21"/>
<wire x1="0.7" y1="1" x2="0.7" y2="-0.65" width="0.1016" layer="21"/>
<wire x1="0.7" y1="-0.65" x2="0.7" y2="-1" width="0.1016" layer="21"/>
<wire x1="0.45" y1="-0.7" x2="-0.45" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="-0.7" x2="-0.45" y2="0.7" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="0.7" x2="0.45" y2="0.7" width="0.1016" layer="21"/>
<wire x1="0.45" y1="0.7" x2="0.45" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="0.7" y1="-0.65" x2="0.35" y2="-1" width="0.1016" layer="21"/>
<smd name="C" x="0" y="-2.2" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.2" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<text x="-1.27" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.55" y1="1.5" x2="0.55" y2="2.1" layer="29"/>
<rectangle x1="-0.55" y1="-2.1" x2="0.55" y2="-1.5" layer="29"/>
<rectangle x1="-0.5" y1="-2.05" x2="0.5" y2="-1.55" layer="31"/>
<rectangle x1="-0.5" y1="1.55" x2="0.5" y2="2.05" layer="31"/>
<rectangle x1="-0.2" y1="-0.4" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.5" y1="-2.1" x2="0.5" y2="-1.4" layer="51"/>
<rectangle x1="-0.5" y1="1.4" x2="0.5" y2="2.05" layer="51"/>
<rectangle x1="-0.5" y1="1" x2="0.5" y2="1.4" layer="21"/>
<rectangle x1="-0.5" y1="-1.4" x2="0.5" y2="-1.05" layer="21"/>
<hole x="0" y="0" drill="2.7"/>
</package>
<package name="CHIPLED_0805">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_R971.pdf</description>
<wire x1="-0.35" y1="1" x2="0.35" y2="1" width="0.1016" layer="51" curve="180" cap="flat"/>
<wire x1="-0.35" y1="-1" x2="0.35" y2="-1" width="0.1016" layer="51" curve="-180" cap="flat"/>
<wire x1="0.575" y1="0.525" x2="0.575" y2="-0.525" width="0.1016" layer="51"/>
<wire x1="-0.575" y1="-0.5" x2="-0.575" y2="0.925" width="0.1016" layer="51"/>
<circle x="-0.45" y="0.85" radius="0.103" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.05" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="0" y="-1.05" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="0.3" y1="0.5" x2="0.625" y2="1" layer="51"/>
<rectangle x1="-0.325" y1="0.5" x2="-0.175" y2="0.75" layer="51"/>
<rectangle x1="0.175" y1="0.5" x2="0.325" y2="0.75" layer="51"/>
<rectangle x1="-0.2" y1="0.5" x2="0.2" y2="0.675" layer="51"/>
<rectangle x1="0.3" y1="-1" x2="0.625" y2="-0.5" layer="51"/>
<rectangle x1="-0.625" y1="-1" x2="-0.3" y2="-0.5" layer="51"/>
<rectangle x1="0.175" y1="-0.75" x2="0.325" y2="-0.5" layer="51"/>
<rectangle x1="-0.325" y1="-0.75" x2="-0.175" y2="-0.5" layer="51"/>
<rectangle x1="-0.2" y1="-0.675" x2="0.2" y2="-0.5" layer="51"/>
<rectangle x1="-0.1" y1="0" x2="0.1" y2="0.2" layer="21"/>
<rectangle x1="-0.6" y1="0.5" x2="-0.3" y2="0.8" layer="51"/>
<rectangle x1="-0.625" y1="0.925" x2="-0.4" y2="1" layer="51"/>
</package>
<package name="CHIPLED_1206">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_LY N971.pdf</description>
<wire x1="-0.4" y1="1.625" x2="0.4" y2="1.625" width="0.1016" layer="51" curve="180" cap="flat"/>
<wire x1="-0.8" y1="-0.95" x2="-0.8" y2="0.95" width="0.1016" layer="51"/>
<wire x1="0.8" y1="0.95" x2="0.8" y2="-0.95" width="0.1016" layer="51"/>
<circle x="-0.55" y="1.425" radius="0.1" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.75" dx="1.5" dy="1.5" layer="1"/>
<smd name="A" x="0" y="-1.75" dx="1.5" dy="1.5" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.85" y1="1.525" x2="-0.35" y2="1.65" layer="51"/>
<rectangle x1="-0.85" y1="1.225" x2="-0.625" y2="1.55" layer="51"/>
<rectangle x1="-0.45" y1="1.225" x2="-0.325" y2="1.45" layer="51"/>
<rectangle x1="-0.65" y1="1.225" x2="-0.225" y2="1.35" layer="51"/>
<rectangle x1="0.35" y1="1.3" x2="0.85" y2="1.65" layer="51"/>
<rectangle x1="0.25" y1="1.225" x2="0.85" y2="1.35" layer="51"/>
<rectangle x1="-0.85" y1="0.95" x2="0.85" y2="1.25" layer="51"/>
<rectangle x1="-0.85" y1="-1.65" x2="0.85" y2="-0.95" layer="51"/>
<rectangle x1="-0.85" y1="0.35" x2="-0.525" y2="0.775" layer="21"/>
<rectangle x1="0.525" y1="0.35" x2="0.85" y2="0.775" layer="21"/>
<rectangle x1="-0.175" y1="0" x2="0.175" y2="0.35" layer="21"/>
</package>
<package name="CHIPLED_0603">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_LY Q971.pdf</description>
<wire x1="-0.3" y1="0.825" x2="0.3" y2="0.825" width="0.1016" layer="51" curve="180" cap="flat"/>
<wire x1="-0.275" y1="-0.825" x2="0.275" y2="-0.825" width="0.0508" layer="51" curve="-180" cap="flat"/>
<wire x1="-0.4" y1="0.375" x2="-0.4" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.35" x2="0.4" y2="-0.35" width="0.1016" layer="51"/>
<circle x="-0.35" y="0.625" radius="0.075" width="0.0508" layer="51"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.7" x2="-0.25" y2="0.85" layer="51"/>
<rectangle x1="-0.275" y1="0.55" x2="-0.225" y2="0.6" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="-0.4" y2="0.725" layer="51"/>
<rectangle x1="0.25" y1="0.55" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="0.45" y2="0.575" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="-0.25" y2="-0.35" layer="51"/>
<rectangle x1="0.25" y1="-0.85" x2="0.45" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.575" x2="0.275" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.65" x2="-0.175" y2="-0.55" layer="51"/>
<rectangle x1="0.175" y1="-0.65" x2="0.275" y2="-0.55" layer="51"/>
<rectangle x1="-0.125" y1="0" x2="0.125" y2="0.25" layer="21"/>
</package>
<package name="CHIPLED-0603-TTW">
<description>&lt;b&gt;CHIPLED-0603&lt;/b&gt;&lt;p&gt;
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603&lt;br&gt;
Package able to withstand TTW-soldering heat&lt;br&gt;
Package suitable for TTW-soldering&lt;br&gt;
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<wire x1="-0.3" y1="0.825" x2="0.3" y2="0.825" width="0.1016" layer="51" curve="180" cap="flat"/>
<wire x1="-0.275" y1="-0.825" x2="0.275" y2="-0.825" width="0.0508" layer="51" curve="-180" cap="flat"/>
<wire x1="-0.4" y1="0.375" x2="-0.4" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.35" x2="0.4" y2="-0.35" width="0.1016" layer="51"/>
<circle x="-0.35" y="0.625" radius="0.075" width="0.0508" layer="51"/>
<smd name="C" x="0" y="0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="-0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A@1" x="0" y="-0.5" dx="0.35" dy="0.35" layer="1"/>
<smd name="C@1" x="0" y="0.5" dx="0.35" dy="0.35" layer="1"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.7" x2="-0.25" y2="0.85" layer="51"/>
<rectangle x1="-0.275" y1="0.55" x2="-0.225" y2="0.6" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="-0.4" y2="0.725" layer="51"/>
<rectangle x1="0.25" y1="0.55" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="0.45" y2="0.575" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="-0.25" y2="-0.35" layer="51"/>
<rectangle x1="0.25" y1="-0.85" x2="0.45" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.575" x2="0.275" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.65" x2="-0.175" y2="-0.55" layer="51"/>
<rectangle x1="0.175" y1="-0.65" x2="0.275" y2="-0.55" layer="51"/>
<rectangle x1="-0.125" y1="0" x2="0.125" y2="0.25" layer="21"/>
<rectangle x1="-0.175" y1="0.325" x2="0.175" y2="0.7" layer="29"/>
<rectangle x1="-0.4" y1="0.625" x2="0.4" y2="1.125" layer="29"/>
<rectangle x1="-0.4" y1="-1.125" x2="0.4" y2="-0.625" layer="29"/>
<rectangle x1="-0.175" y1="-0.675" x2="0.175" y2="-0.325" layer="29"/>
</package>
<package name="SMARTLED-TTW">
<description>&lt;b&gt;SmartLED TTW&lt;/b&gt;&lt;p&gt;
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603&lt;br&gt;
Package able to withstand TTW-soldering heat&lt;br&gt;
Package suitable for TTW-soldering&lt;br&gt;
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<wire x1="-0.35" y1="0.6" x2="0.35" y2="0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.35" y1="0.6" x2="0.35" y2="-0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.6" x2="-0.35" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.35" y1="-0.6" x2="-0.35" y2="0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.4" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<smd name="C" x="0" y="0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="-0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A@1" x="0" y="-0.5" dx="0.35" dy="0.35" layer="1" stop="no" cream="no"/>
<smd name="C@1" x="0" y="0.5" dx="0.35" dy="0.35" layer="1" stop="no" cream="no"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.175" y1="0.325" x2="0.175" y2="0.7" layer="29"/>
<rectangle x1="-0.15" y1="-0.35" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.15" y1="0.6" x2="0.15" y2="0.85" layer="51"/>
<rectangle x1="-0.15" y1="-0.85" x2="0.15" y2="-0.6" layer="51"/>
<rectangle x1="-0.225" y1="0.3" x2="0.225" y2="0.975" layer="31"/>
<rectangle x1="-0.175" y1="-0.7" x2="0.175" y2="-0.325" layer="29" rot="R180"/>
<rectangle x1="-0.225" y1="-0.975" x2="0.225" y2="-0.3" layer="31" rot="R180"/>
</package>
<package name="SMLK34">
<wire x1="-2" y1="1" x2="1.7" y2="1" width="0.127" layer="21"/>
<wire x1="1.7" y1="1" x2="2" y2="0.7" width="0.127" layer="21"/>
<wire x1="2" y1="0.7" x2="2" y2="-1" width="0.127" layer="21"/>
<wire x1="2" y1="-1" x2="-2" y2="-1" width="0.127" layer="21"/>
<wire x1="-2" y1="-1" x2="-2" y2="1" width="0.127" layer="21"/>
<wire x1="-1.8" y1="0.8" x2="-1.8" y2="-0.8" width="0.127" layer="25"/>
<wire x1="-1.8" y1="-0.8" x2="1.8" y2="-0.8" width="0.127" layer="25"/>
<wire x1="1.8" y1="-0.8" x2="1.8" y2="0.6" width="0.127" layer="25"/>
<wire x1="1.8" y1="0.6" x2="1.6" y2="0.8" width="0.127" layer="25"/>
<wire x1="1.6" y1="0.8" x2="-1.8" y2="0.8" width="0.127" layer="25"/>
<smd name="A" x="0.9" y="0" dx="3.65" dy="1.74" layer="1" rot="R180"/>
<smd name="K" x="-2.1" y="0" dx="1.35" dy="1.74" layer="1" rot="R180"/>
<text x="-2.8" y="-3.97" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-2.8" y="-5.37" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<text x="1.73" y="-2.178" size="1.016" layer="21" font="vector">A</text>
<text x="-2.938" y="-2.178" size="1.016" layer="21" font="vector">K</text>
</package>
<package name="SOT23-W">
<description>&lt;b&gt;SOT23&lt;/b&gt; - Wave soldering</description>
<wire x1="1.5724" y1="0.6604" x2="1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.5724" y1="-0.6604" x2="-1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="-0.6604" x2="-1.5724" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="0.6604" x2="1.5724" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="-0.3984" x2="-1.5724" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="1.5724" y1="0.6604" x2="1.5724" y2="-0.3984" width="0.1524" layer="21"/>
<wire x1="0.2954" y1="-0.6604" x2="-0.3094" y2="-0.6604" width="0.1524" layer="21"/>
<smd name="3" x="0" y="1.3" dx="2.8" dy="1.4" layer="1"/>
<smd name="2" x="1.1" y="-1.3" dx="1.2" dy="1.4" layer="1"/>
<smd name="1" x="-1.1" y="-1.3" dx="1.2" dy="1.4" layer="1"/>
<text x="2.032" y="0.254" size="0.4064" layer="25">&gt;NAME</text>
<text x="2.032" y="-0.508" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
<package name="SOT23">
<description>&lt;b&gt;SOT-23&lt;/b&gt;</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<smd name="3" x="0" y="1.1" dx="1" dy="1.4" layer="1"/>
<smd name="2" x="0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<smd name="1" x="-0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
<package name="TO220V">
<description>&lt;b&gt;TO 200 vertical&lt;/b&gt;</description>
<wire x1="5.08" y1="-1.143" x2="4.953" y2="-4.064" width="0.127" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="4.953" y2="-4.064" width="0.127" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="-4.699" y2="-4.318" width="0.127" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-4.699" y2="-4.318" width="0.127" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-5.08" y2="-1.143" width="0.127" layer="21"/>
<circle x="-4.4958" y="-3.7084" radius="0.254" width="0.127" layer="21"/>
<pad name="1" x="-2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="0" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<text x="-5.08" y="-6.0452" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-3.175" y="-3.175" size="1.27" layer="51" ratio="10">1</text>
<text x="-0.635" y="-3.175" size="1.27" layer="51" ratio="10">2</text>
<text x="1.905" y="-3.175" size="1.27" layer="51" ratio="10">3</text>
<rectangle x1="-5.334" y1="-0.762" x2="5.334" y2="0" layer="21"/>
<rectangle x1="-5.334" y1="-1.27" x2="-3.429" y2="-0.762" layer="21"/>
<rectangle x1="-1.651" y1="-1.27" x2="-0.889" y2="-0.762" layer="21"/>
<rectangle x1="-3.429" y1="-1.27" x2="-1.651" y2="-0.762" layer="51"/>
<rectangle x1="0.889" y1="-1.27" x2="1.651" y2="-0.762" layer="21"/>
<rectangle x1="3.429" y1="-1.27" x2="5.334" y2="-0.762" layer="21"/>
<rectangle x1="-0.889" y1="-1.27" x2="0.889" y2="-0.762" layer="51"/>
<rectangle x1="1.651" y1="-1.27" x2="3.429" y2="-0.762" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
<symbol name="MOSFET-N">
<wire x1="0.762" y1="0.762" x2="0.762" y2="0" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="0.762" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0.762" y1="3.175" x2="0.762" y2="2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.54" x2="0.762" y2="1.905" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-1.905" x2="0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="-2.54" x2="0.762" y2="-3.175" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0.762" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.508" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="2.54" x2="3.81" y2="2.54" width="0.1524" layer="94"/>
<wire x1="4.572" y1="0.762" x2="4.318" y2="0.508" width="0.1524" layer="94"/>
<wire x1="4.318" y1="0.508" x2="3.81" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.508" x2="3.302" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.302" y1="0.508" x2="3.048" y2="0.254" width="0.1524" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.3592" width="0" layer="94"/>
<circle x="2.54" y="2.54" radius="0.3592" width="0" layer="94"/>
<text x="5.08" y="0.635" size="1.27" layer="95">&gt;NAME</text>
<text x="5.08" y="-1.27" size="1.27" layer="96">&gt;VALUE</text>
<text x="3.175" y="3.175" size="0.8128" layer="93">D</text>
<text x="3.175" y="-3.81" size="0.8128" layer="93">S</text>
<text x="-1.27" y="-1.905" size="0.8128" layer="93">G</text>
<pin name="G" x="-2.54" y="-2.54" visible="pad" length="short"/>
<pin name="S" x="2.54" y="-5.08" visible="pad" length="short" rot="R90"/>
<pin name="D" x="2.54" y="5.08" visible="pad" length="short" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="3.81" y="0.508"/>
<vertex x="3.302" y="-0.254"/>
<vertex x="4.318" y="-0.254"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="1.016" y="0"/>
<vertex x="2.032" y="0.762"/>
<vertex x="2.032" y="-0.762"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="LED" uservalue="yes">
<description>&lt;b&gt;LED&lt;/b&gt;&lt;p&gt;
&lt;u&gt;OSRAM&lt;/u&gt;:&lt;br&gt;

- &lt;u&gt;CHIPLED&lt;/u&gt;&lt;br&gt;
LG R971, LG N971, LY N971, LG Q971, LY Q971, LO R971, LY R971
LH N974, LH R974&lt;br&gt;
LS Q976, LO Q976, LY Q976&lt;br&gt;
LO Q996&lt;br&gt;


- &lt;u&gt;Hyper CHIPLED&lt;/u&gt;&lt;br&gt;
LW Q18S&lt;br&gt;
LB Q993, LB Q99A, LB R99A&lt;br&gt;

- &lt;u&gt;SideLED&lt;/u&gt;&lt;br&gt;
LS A670, LO A670, LY A670, LG A670, LP A670&lt;br&gt;
LB A673, LV A673, LT A673, LW A673&lt;br&gt;
LH A674&lt;br&gt;
LY A675&lt;br&gt;
LS A676, LA A676, LO A676, LY A676, LW A676&lt;br&gt;
LS A679, LY A679, LG A679&lt;br&gt;

-  &lt;u&gt;Hyper Micro SIDELED®&lt;/u&gt;&lt;br&gt;
LS Y876, LA Y876, LO Y876, LY Y876&lt;br&gt;
LT Y87S&lt;br&gt;

- &lt;u&gt;SmartLED&lt;/u&gt;&lt;br&gt;
LW L88C, LW L88S&lt;br&gt;
LB L89C, LB L89S, LG L890&lt;br&gt;
LS L89K, LO L89K, LY L89K&lt;br&gt;
LS L896, LA L896, LO L896, LY L896&lt;br&gt;

- &lt;u&gt;TOPLED&lt;/u&gt;&lt;br&gt;
LS T670, LO T670, LY T670, LG T670, LP T670&lt;br&gt;
LSG T670, LSP T670, LSY T670, LOP T670, LYG T670&lt;br&gt;
LG T671, LOG T671, LSG T671&lt;br&gt;
LB T673, LV T673, LT T673, LW T673&lt;br&gt;
LH T674&lt;br&gt;
LS T676, LA T676, LO T676, LY T676, LB T676, LH T676, LSB T676, LW T676&lt;br&gt;
LB T67C, LV T67C, LT T67C, LS T67K, LO T67K, LY T67K, LW E67C&lt;br&gt;
LS E67B, LA E67B, LO E67B, LY E67B, LB E67C, LV E67C, LT E67C&lt;br&gt;
LW T67C&lt;br&gt;
LS T679, LY T679, LG T679&lt;br&gt;
LS T770, LO T770, LY T770, LG T770, LP T770&lt;br&gt;
LB T773, LV T773, LT T773, LW T773&lt;br&gt;
LH T774&lt;br&gt;
LS E675, LA E675, LY E675, LS T675&lt;br&gt;
LS T776, LA T776, LO T776, LY T776, LB T776&lt;br&gt;
LHGB T686&lt;br&gt;
LT T68C, LB T68C&lt;br&gt;

- &lt;u&gt;Hyper Mini TOPLED®&lt;/u&gt;&lt;br&gt;
LB M676&lt;br&gt;

- &lt;u&gt;Mini TOPLED Santana®&lt;/u&gt;&lt;br&gt;
LG M470&lt;br&gt;
LS M47K, LO M47K, LY M47K&lt;br&gt;

&lt;p&gt;
Source: http://www.osram.convergy.de/</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="SMT1206" package="1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LD260" package="LD260">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SQR2X5" package="LED2X5">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="LED3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="LED5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LSU260" package="LSU260">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LZR181" package="LZR181">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B152" package="Q62902-B152">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B153" package="Q62902-B153">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B155" package="Q62902-B155">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B156" package="Q62902-B156">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SFH480" package="SFH480">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SFH482" package="SFH482">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SQR5.7X3.2" package="U57X32">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="IRL80A" package="IRL80A">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-2" package="P-LCC-2">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MINI-TOP" package="OSRAM-MINI-TOP-LED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIDELED" package="OSRAM-SIDELED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMART-LED" package="SMART-LED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="B"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-2-BACK" package="P-LCC-2-TOPLED-RG">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MICRO-SIDELED" package="MICRO-SIDELED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-4" package="P-LCC-4">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C@4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIP-LED0603" package="CHIP-LED0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIP-LED0805" package="CHIP-LED0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TOPLED-SANTANA" package="MINI-TOPLED-SANTANA">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_0805" package="CHIPLED_0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_1206" package="CHIPLED_1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_0603" package="CHIPLED_0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED-0603-TTW" package="CHIPLED-0603-TTW">
<connects>
<connect gate="G$1" pin="A" pad="A@1"/>
<connect gate="G$1" pin="C" pad="C@1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="SMARTLED-TTW">
<connects>
<connect gate="G$1" pin="A" pad="A@1"/>
<connect gate="G$1" pin="C" pad="C@1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMLK34" package="SMLK34">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOSFET-N" prefix="Q" uservalue="yes">
<description>&lt;b&gt;N-Channel Mosfet&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;2N7002E - 60V 260mA SOT23 [Digikey: 2N7002ET1GOSTR-ND] - &lt;b&gt;REEL&lt;/b&gt;&lt;/li&gt;
&lt;li&gt;BSH103 - 30V 850mA SOT23 [Digikey: 568-5013-1-ND]&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="MOSFET-N" x="-2.54" y="0"/>
</gates>
<devices>
<device name="WAVE" package="SOT23-W">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="REFLOW" package="SOT23">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="GDS_TO220V" package="TO220V">
<connects>
<connect gate="G$1" pin="D" pad="2"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="RHD2000_eval_board">
<description>Intan Technologies RHD2000 Eval Board</description>
<packages>
<package name="0201">
<wire x1="-0.254" y1="0.1524" x2="0.254" y2="0.1524" width="0.1016" layer="51"/>
<wire x1="0.254" y1="-0.1524" x2="-0.254" y2="-0.1524" width="0.1016" layer="51"/>
<wire x1="-0.8128" y1="0.4064" x2="0.8128" y2="0.4064" width="0.127" layer="21"/>
<wire x1="0.8128" y1="0.4064" x2="0.8128" y2="-0.4064" width="0.127" layer="21"/>
<wire x1="0.8128" y1="-0.4064" x2="-0.8128" y2="-0.4064" width="0.127" layer="21"/>
<wire x1="-0.8128" y1="-0.4064" x2="-0.8128" y2="0.4064" width="0.127" layer="21"/>
<smd name="1" x="-0.3937" y="0" dx="0.4318" dy="0.4318" layer="1"/>
<smd name="2" x="0.3937" y="0" dx="0.4318" dy="0.4318" layer="1"/>
<text x="-1.0668" y="0.5588" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1.1938" y="-1.5748" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.3048" y1="-0.1524" x2="-0.1524" y2="0.1524" layer="51"/>
<rectangle x1="0.1524" y1="-0.1524" x2="0.3048" y2="0.1524" layer="51"/>
</package>
<package name="0402">
<wire x1="-0.4572" y1="0.254" x2="0.4572" y2="0.254" width="0.1016" layer="51"/>
<wire x1="0.4572" y1="-0.254" x2="-0.4572" y2="-0.254" width="0.1016" layer="51"/>
<wire x1="-1.0668" y1="0.5588" x2="1.0668" y2="0.5588" width="0.127" layer="21"/>
<wire x1="1.0668" y1="0.5588" x2="1.0668" y2="-0.5588" width="0.127" layer="21"/>
<wire x1="1.0668" y1="-0.5588" x2="-1.0668" y2="-0.5588" width="0.127" layer="21"/>
<wire x1="-1.0668" y1="-0.5588" x2="-1.0668" y2="0.5588" width="0.127" layer="21"/>
<smd name="1" x="-0.5334" y="0" dx="0.6858" dy="0.762" layer="1"/>
<smd name="2" x="0.5334" y="0" dx="0.6858" dy="0.762" layer="1"/>
<text x="-1.143" y="0.762" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-1.778" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.508" y1="-0.254" x2="-0.254" y2="0.254" layer="51"/>
<rectangle x1="0.254" y1="-0.254" x2="0.508" y2="0.254" layer="51"/>
</package>
<package name="0603">
<wire x1="-0.762" y1="0.4064" x2="0.762" y2="0.4064" width="0.1016" layer="51"/>
<wire x1="0.762" y1="-0.4064" x2="-0.762" y2="-0.4064" width="0.1016" layer="51"/>
<wire x1="-1.524" y1="0.762" x2="1.524" y2="0.762" width="0.127" layer="21"/>
<wire x1="1.524" y1="0.762" x2="1.524" y2="-0.762" width="0.127" layer="21"/>
<wire x1="1.524" y1="-0.762" x2="-1.524" y2="-0.762" width="0.127" layer="21"/>
<wire x1="-1.524" y1="-0.762" x2="-1.524" y2="0.762" width="0.127" layer="21"/>
<smd name="1" x="-0.8636" y="0" dx="0.889" dy="1.0668" layer="1"/>
<smd name="2" x="0.8636" y="0" dx="0.889" dy="1.0668" layer="1"/>
<text x="-1.778" y="0" size="0.762" layer="25" font="vector" ratio="10" align="center-right">&gt;NAME</text>
<text x="1.778" y="0" size="0.762" layer="27" font="vector" ratio="10" align="center-left">&gt;VALUE</text>
<rectangle x1="-0.8128" y1="-0.4064" x2="-0.508" y2="0.4064" layer="51"/>
<rectangle x1="0.508" y1="-0.4064" x2="0.8128" y2="0.4064" layer="51"/>
</package>
<package name="0805">
<wire x1="-0.9652" y1="0.635" x2="0.9652" y2="0.635" width="0.1016" layer="51"/>
<wire x1="0.9652" y1="-0.635" x2="-0.9652" y2="-0.635" width="0.1016" layer="51"/>
<wire x1="-1.778" y1="1.016" x2="1.778" y2="1.016" width="0.127" layer="21"/>
<wire x1="1.778" y1="1.016" x2="1.778" y2="-1.016" width="0.127" layer="21"/>
<wire x1="1.778" y1="-1.016" x2="-1.778" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-1.778" y1="-1.016" x2="-1.778" y2="1.016" width="0.127" layer="21"/>
<smd name="1" x="-1.0414" y="0" dx="0.9652" dy="1.524" layer="1"/>
<smd name="2" x="1.0414" y="0" dx="0.9652" dy="1.524" layer="1"/>
<text x="-1.778" y="1.27" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-2.032" y="-2.286" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-1.016" y1="-0.635" x2="-0.6096" y2="0.635" layer="51"/>
<rectangle x1="0.6096" y1="-0.635" x2="1.016" y2="0.635" layer="51"/>
</package>
<package name="1206">
<wire x1="-1.5494" y1="0.8128" x2="1.5494" y2="0.8128" width="0.1016" layer="51"/>
<wire x1="1.5494" y1="-0.8128" x2="-1.5494" y2="-0.8128" width="0.1016" layer="51"/>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.127" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.2192" dy="1.8796" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.2192" dy="1.8796" layer="1"/>
<text x="-2.54" y="1.524" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-1.6002" y1="-0.8128" x2="-1.0922" y2="0.8128" layer="51"/>
<rectangle x1="1.0922" y1="-0.8128" x2="1.6002" y2="0.8128" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="R">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="R" prefix="R" uservalue="yes">
<description>Resistor</description>
<gates>
<gate name="R" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="_0201" package="0201">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0402" package="0402">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0603" package="0603">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0805" package="0805">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_1206" package="1206">
<connects>
<connect gate="R" pin="1" pad="1"/>
<connect gate="R" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="diy-modules">
<description>&lt;b&gt;DIY Modules for Arduino, Raspberry Pi, CubieBoard etc.&lt;/b&gt;
&lt;br&gt;&lt;br&gt;
The library contains a list of symbols and footprints for popular, cheap and easy-to-use electronic modules.&lt;br&gt;
The modules are intend to work with microprocessor-based platforms such as &lt;a href="http://arduino.cc"&gt;Arduino&lt;/a&gt;, &lt;a href="http://raspberrypi.org/"&gt;Raspberry Pi&lt;/a&gt;, &lt;a href="http://cubieboard.org/"&gt;CubieBoard&lt;/a&gt;, &lt;a href="http://beagleboard.org/"&gt;BeagleBone&lt;/a&gt; and many others. There are many manufacturers of the modules in the world. Almost all of them can be bought on &lt;a href="ebay.com"&gt;ebay.com&lt;/a&gt;.&lt;br&gt;
&lt;br&gt;
By using this library, you can design a PCB for devices created with usage of modules. Even if you do not need to create PCB design, you can also use the library to quickly document your work by drawing schematics of devices built by you.&lt;br&gt;
&lt;br&gt;
The latest version, examples, photos and much more can be found at: &lt;b&gt;&lt;a href="http://diymodules.org/eagle"&gt;diymodules.org/eagle&lt;/a&gt;&lt;/b&gt;&lt;br&gt;&lt;br&gt;
Comments, suggestions and bug reports please send to: &lt;b&gt;&lt;a href="mailto:eagle@diymodules.org"&gt;eagle@diymodules.org&lt;/b&gt;&lt;/a&gt;&lt;br&gt;&lt;br&gt;
&lt;i&gt;Version: 1.8.0 (2017-Jul-02)&lt;/i&gt;&lt;br&gt;
&lt;i&gt;Created by: Miroslaw Brudnowski&lt;/i&gt;&lt;br&gt;&lt;br&gt;
&lt;i&gt;Released under the Creative Commons Attribution 4.0 International License: &lt;a href="http://creativecommons.org/licenses/by/4.0"&gt;http://creativecommons.org/licenses/by/4.0&lt;/a&gt;&lt;/i&gt;
&lt;br&gt;&lt;br&gt;
&lt;center&gt;
&lt;a href="http://diymodules.org/eagle"&gt;&lt;img src="http://www.diymodules.org/img/diymodules-lbr-image.php?v=1.8.0" alt="DIYmodules.org"&gt;&lt;/a&gt;
&lt;/center&gt;</description>
<packages>
<package name="BATTERY-LI-MH12210">
<description>&lt;b&gt;Lithium-Ion MH12210 Rechargeable Battery&lt;/b&gt;</description>
<wire x1="-31.75" y1="9.271" x2="32.639" y2="9.271" width="0.127" layer="21"/>
<wire x1="32.639" y1="-9.271" x2="-31.75" y2="-9.271" width="0.127" layer="21"/>
<wire x1="-31.75" y1="-9.271" x2="-31.75" y2="-3.302" width="0.127" layer="21"/>
<pad name="BAT+" x="-33.02" y="0" drill="1.5" diameter="3" shape="square"/>
<pad name="BAT-" x="33.02" y="0" drill="1.5" diameter="3"/>
<text x="0" y="10.795" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-10.795" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<wire x1="-31.75" y1="3.302" x2="-31.75" y2="9.271" width="0.127" layer="21"/>
<wire x1="-26.67" y1="2.54" x2="-26.67" y2="-2.54" width="0.508" layer="21"/>
<wire x1="-29.21" y1="0" x2="-24.13" y2="0" width="0.508" layer="21"/>
<wire x1="29.21" y1="0" x2="24.13" y2="0" width="0.508" layer="21"/>
<text x="0" y="1.27" size="3.048" layer="21" align="bottom-center">MH1220</text>
<wire x1="-31.75" y1="3.302" x2="-32.639" y2="3.302" width="0.127" layer="21"/>
<wire x1="-32.639" y1="-3.302" x2="-31.75" y2="-3.302" width="0.127" layer="21"/>
<wire x1="-32.639" y1="3.302" x2="-32.639" y2="2.032" width="0.127" layer="21"/>
<wire x1="-32.639" y1="-2.032" x2="-32.639" y2="-3.302" width="0.127" layer="21"/>
<wire x1="-31.75" y1="3.302" x2="-31.75" y2="2.032" width="0.127" layer="21"/>
<wire x1="-31.75" y1="-2.032" x2="-31.75" y2="-3.302" width="0.127" layer="21"/>
<wire x1="32.639" y1="9.271" x2="32.639" y2="2.032" width="0.127" layer="21"/>
<wire x1="32.639" y1="-2.032" x2="32.639" y2="-9.271" width="0.127" layer="21"/>
<text x="0" y="-1.27" size="3.048" layer="21" align="top-center">3.7V</text>
</package>
</packages>
<symbols>
<symbol name="BATTERY-LI-MH12210">
<description>&lt;b&gt;MH12210 Li-ion Rechargeable Battery&lt;/b&gt;</description>
<pin name="BAT+" x="20.32" y="0" visible="pad" length="short" direction="pwr" rot="R180"/>
<pin name="BAT-" x="-20.32" y="0" visible="pad" length="short" direction="pwr"/>
<wire x1="15.24" y1="7.62" x2="-15.24" y2="7.62" width="0.254" layer="94"/>
<rectangle x1="-4.953" y1="-0.635" x2="0.127" y2="0.635" layer="94" rot="R270"/>
<rectangle x1="-6.096" y1="-0.254" x2="4.572" y2="0.254" layer="94" rot="R270"/>
<rectangle x1="-1.651" y1="-0.635" x2="3.429" y2="0.635" layer="94" rot="R270"/>
<rectangle x1="-2.794" y1="-0.254" x2="7.874" y2="0.254" layer="94" rot="R270"/>
<wire x1="17.272" y1="2.032" x2="17.272" y2="0" width="0.254" layer="94"/>
<wire x1="17.272" y1="0" x2="17.272" y2="-2.032" width="0.254" layer="94"/>
<wire x1="17.272" y1="-2.032" x2="15.24" y2="-2.032" width="0.254" layer="94"/>
<wire x1="15.24" y1="-2.032" x2="13.208" y2="-2.032" width="0.254" layer="94"/>
<wire x1="13.208" y1="-2.032" x2="13.208" y2="0" width="0.254" layer="94"/>
<wire x1="13.208" y1="0" x2="13.208" y2="2.032" width="0.254" layer="94"/>
<wire x1="13.208" y1="2.032" x2="15.24" y2="2.032" width="0.254" layer="94"/>
<wire x1="15.24" y1="2.032" x2="17.272" y2="2.032" width="0.254" layer="94"/>
<wire x1="16.256" y1="0" x2="14.224" y2="0" width="0.254" layer="94"/>
<wire x1="15.24" y1="-1.016" x2="15.24" y2="1.016" width="0.254" layer="94"/>
<wire x1="15.24" y1="7.62" x2="15.24" y2="2.032" width="0.254" layer="94"/>
<wire x1="17.78" y1="0" x2="17.272" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="13.208" y2="0" width="0.1524" layer="94"/>
<text x="-15.24" y="12.7" size="1.778" layer="95">&gt;NAME</text>
<text x="-15.24" y="10.16" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="-13.208" y1="2.032" x2="-13.208" y2="0" width="0.254" layer="94"/>
<wire x1="-13.208" y1="0" x2="-13.208" y2="-2.032" width="0.254" layer="94"/>
<wire x1="-13.208" y1="-2.032" x2="-15.24" y2="-2.032" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-2.032" x2="-17.272" y2="-2.032" width="0.254" layer="94"/>
<wire x1="-17.272" y1="-2.032" x2="-17.272" y2="0" width="0.254" layer="94"/>
<wire x1="-17.272" y1="0" x2="-17.272" y2="2.032" width="0.254" layer="94"/>
<wire x1="-17.272" y1="2.032" x2="-15.24" y2="2.032" width="0.254" layer="94"/>
<wire x1="-15.24" y1="2.032" x2="-13.208" y2="2.032" width="0.254" layer="94"/>
<wire x1="-14.224" y1="0" x2="-16.256" y2="0" width="0.254" layer="94"/>
<wire x1="-3.048" y1="0" x2="-13.208" y2="0" width="0.1524" layer="94"/>
<wire x1="-15.24" y1="7.62" x2="-15.24" y2="2.032" width="0.254" layer="94"/>
<wire x1="-17.272" y1="0" x2="-17.78" y2="0" width="0.1524" layer="94"/>
<wire x1="15.24" y1="-2.032" x2="15.24" y2="-7.62" width="0.254" layer="94"/>
<wire x1="15.24" y1="-7.62" x2="-15.24" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-7.62" x2="-15.24" y2="-2.032" width="0.254" layer="94"/>
<text x="-12.7" y="-5.08" size="1.27" layer="94">MH12210</text>
<text x="12.7" y="-5.08" size="1.27" layer="94" align="bottom-right">3.7V</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="BATTERY-LI-MH12210">
<description>&lt;b&gt;Lithium-Ion MH12210 Rechargeable Battery&lt;/b&gt;
&lt;p&gt;&lt;b&gt;Panasonic NCR18650PF&lt;/b&gt; battery datasheet available here:&lt;br /&gt;&lt;a href="https://industrial.panasonic.com/cdbs/www-data/pdf2/ACI4000/ACI4000C12.pdf"&gt;https://industrial.panasonic.com/cdbs/www-data/pdf2/ACI4000/ACI4000C12.pdf&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;&lt;a href="http://www.ebay.com/sch/ncr18650b+battery"&gt;Click here to find device on ebay.com&lt;/a&gt;&lt;/b&gt;&lt;/p&gt;

&lt;p&gt;&lt;img alt="photo" src="http://www.diymodules.org/img/device-photo.php?name=BATTERY-LI-MH12210"&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="BATTERY-LI-MH12210" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BATTERY-LI-MH12210">
<connects>
<connect gate="G$1" pin="BAT+" pad="BAT+"/>
<connect gate="G$1" pin="BAT-" pad="BAT-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-kycon" urn="urn:adsk.eagle:library:157">
<description>&lt;b&gt;Connector from KYCON, Inc&lt;/b&gt;&lt;p&gt;
1810 Little Orchard Street,&lt;br&gt;
San Jose,&lt;br&gt;
CA 95125 (408)494-0330&lt;br&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/autor&gt;</description>
<packages>
<package name="GLX-S-88M" urn="urn:adsk.eagle:footprint:7673/1" library_version="1">
<description>&lt;b&gt;Mod. Jack, Right Angle, 8 posiotion, 8 contatcs&lt;/b&gt; RJ45&lt;p&gt;
Source: GLX-S-88M.pdf</description>
<wire x1="-8.532" y1="4.52" x2="8.532" y2="4.52" width="0" layer="20"/>
<wire x1="8.25" y1="6.525" x2="8.25" y2="-3.302" width="0.2032" layer="22"/>
<wire x1="8.25" y1="-6.35" x2="8.25" y2="-7.875" width="0.2032" layer="22"/>
<wire x1="8.25" y1="-7.875" x2="-8.25" y2="-7.875" width="0.2032" layer="22"/>
<wire x1="-8.25" y1="-7.875" x2="-8.25" y2="-6.35" width="0.2032" layer="22"/>
<wire x1="-8.25" y1="-3.302" x2="-8.25" y2="6.525" width="0.2032" layer="22"/>
<wire x1="-8.25" y1="6.525" x2="8.25" y2="6.525" width="0.2032" layer="22"/>
<wire x1="-8.25" y1="-6.351" x2="-8.25" y2="-3.381" width="0.2032" layer="51"/>
<wire x1="8.25" y1="-3.381" x2="8.25" y2="-6.351" width="0.2032" layer="51"/>
<pad name="1" x="-3.57" y="-2.3" drill="0.9" diameter="1.4224"/>
<pad name="2" x="-2.55" y="-4.84" drill="0.9" diameter="1.4224"/>
<pad name="3" x="-1.53" y="-2.3" drill="0.9" diameter="1.4224"/>
<pad name="4" x="-0.51" y="-4.84" drill="0.9" diameter="1.4224"/>
<pad name="5" x="0.51" y="-2.3" drill="0.9" diameter="1.4224"/>
<pad name="6" x="1.53" y="-4.84" drill="0.9" diameter="1.4224"/>
<pad name="7" x="2.55" y="-2.3" drill="0.9" diameter="1.4224"/>
<pad name="8" x="3.57" y="-4.84" drill="0.9" diameter="1.4224"/>
<pad name="S1" x="-8.125" y="-4.84" drill="1.6" diameter="2.1844"/>
<pad name="S2" x="8.125" y="-4.84" drill="1.6" diameter="2.1844"/>
<text x="-8.128" y="-9.652" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.08" y="1.27" size="1.27" layer="27">&gt;VALUE</text>
<hole x="-7.44" y="0" drill="2.55"/>
<hole x="7.44" y="0" drill="2.55"/>
</package>
</packages>
<packages3d>
<package3d name="GLX-S-88M" urn="urn:adsk.eagle:package:7680/1" type="box" library_version="1">
<description>Mod. Jack, Right Angle, 8 posiotion, 8 contatcs RJ45
Source: GLX-S-88M.pdf</description>
<packageinstances>
<packageinstance name="GLX-S-88M"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="JACK8SH" urn="urn:adsk.eagle:symbol:7672/1" library_version="1">
<wire x1="1.524" y1="10.668" x2="0" y2="10.668" width="0.254" layer="94"/>
<wire x1="0" y1="10.668" x2="0" y2="9.652" width="0.254" layer="94"/>
<wire x1="0" y1="9.652" x2="1.524" y2="9.652" width="0.254" layer="94"/>
<wire x1="1.524" y1="8.128" x2="0" y2="8.128" width="0.254" layer="94"/>
<wire x1="0" y1="8.128" x2="0" y2="7.112" width="0.254" layer="94"/>
<wire x1="0" y1="7.112" x2="1.524" y2="7.112" width="0.254" layer="94"/>
<wire x1="1.524" y1="5.588" x2="0" y2="5.588" width="0.254" layer="94"/>
<wire x1="0" y1="5.588" x2="0" y2="4.572" width="0.254" layer="94"/>
<wire x1="0" y1="4.572" x2="1.524" y2="4.572" width="0.254" layer="94"/>
<wire x1="1.524" y1="3.048" x2="0" y2="3.048" width="0.254" layer="94"/>
<wire x1="0" y1="3.048" x2="0" y2="2.032" width="0.254" layer="94"/>
<wire x1="0" y1="2.032" x2="1.524" y2="2.032" width="0.254" layer="94"/>
<wire x1="1.524" y1="0.508" x2="0" y2="0.508" width="0.254" layer="94"/>
<wire x1="0" y1="0.508" x2="0" y2="-0.508" width="0.254" layer="94"/>
<wire x1="0" y1="-0.508" x2="1.524" y2="-0.508" width="0.254" layer="94"/>
<wire x1="1.524" y1="-2.032" x2="0" y2="-2.032" width="0.254" layer="94"/>
<wire x1="0" y1="-2.032" x2="0" y2="-3.048" width="0.254" layer="94"/>
<wire x1="0" y1="-3.048" x2="1.524" y2="-3.048" width="0.254" layer="94"/>
<wire x1="1.524" y1="-4.572" x2="0" y2="-4.572" width="0.254" layer="94"/>
<wire x1="0" y1="-4.572" x2="0" y2="-5.588" width="0.254" layer="94"/>
<wire x1="0" y1="-5.588" x2="1.524" y2="-5.588" width="0.254" layer="94"/>
<wire x1="1.524" y1="-7.112" x2="0" y2="-7.112" width="0.254" layer="94"/>
<wire x1="0" y1="-7.112" x2="0" y2="-8.128" width="0.254" layer="94"/>
<wire x1="0" y1="-8.128" x2="1.524" y2="-8.128" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-10.16" x2="0.254" y2="-10.16" width="0.127" layer="94"/>
<wire x1="1.016" y1="-10.16" x2="1.524" y2="-10.16" width="0.127" layer="94"/>
<wire x1="2.286" y1="-10.16" x2="2.794" y2="-10.16" width="0.127" layer="94"/>
<wire x1="3.048" y1="-10.16" x2="3.302" y2="-10.16" width="0.127" layer="94"/>
<wire x1="3.302" y1="-10.16" x2="3.302" y2="-9.652" width="0.127" layer="94"/>
<wire x1="3.302" y1="9.906" x2="3.302" y2="10.414" width="0.127" layer="94"/>
<wire x1="3.302" y1="10.922" x2="3.302" y2="11.43" width="0.127" layer="94"/>
<wire x1="3.302" y1="11.43" x2="2.794" y2="11.43" width="0.127" layer="94"/>
<wire x1="2.286" y1="11.43" x2="1.778" y2="11.43" width="0.127" layer="94"/>
<wire x1="1.27" y1="11.43" x2="0.762" y2="11.43" width="0.127" layer="94"/>
<wire x1="0.254" y1="11.43" x2="-0.381" y2="11.43" width="0.127" layer="94"/>
<wire x1="-0.381" y1="11.43" x2="-0.381" y2="10.668" width="0.127" layer="94"/>
<wire x1="-0.381" y1="9.652" x2="-0.381" y2="8.128" width="0.127" layer="94"/>
<wire x1="-0.381" y1="7.112" x2="-0.381" y2="5.588" width="0.127" layer="94"/>
<wire x1="-0.381" y1="4.572" x2="-0.381" y2="3.048" width="0.127" layer="94"/>
<wire x1="-0.381" y1="2.032" x2="-0.381" y2="0.508" width="0.127" layer="94"/>
<wire x1="-0.381" y1="-0.508" x2="-0.381" y2="-2.032" width="0.127" layer="94"/>
<wire x1="-0.381" y1="-3.048" x2="-0.381" y2="-4.572" width="0.127" layer="94"/>
<wire x1="-0.381" y1="-5.588" x2="-0.381" y2="-7.112" width="0.127" layer="94"/>
<wire x1="-0.381" y1="-8.128" x2="-0.381" y2="-10.16" width="0.127" layer="94"/>
<wire x1="4.826" y1="4.064" x2="4.826" y2="3.048" width="0.1998" layer="94"/>
<wire x1="4.826" y1="3.048" x2="4.826" y2="2.54" width="0.1998" layer="94"/>
<wire x1="4.826" y1="2.54" x2="4.826" y2="2.032" width="0.1998" layer="94"/>
<wire x1="4.826" y1="2.032" x2="4.826" y2="1.524" width="0.1998" layer="94"/>
<wire x1="4.826" y1="1.524" x2="4.826" y2="1.016" width="0.1998" layer="94"/>
<wire x1="4.826" y1="1.016" x2="4.826" y2="0.508" width="0.1998" layer="94"/>
<wire x1="4.826" y1="0.508" x2="4.826" y2="0" width="0.1998" layer="94"/>
<wire x1="4.826" y1="0" x2="4.826" y2="-0.508" width="0.1998" layer="94"/>
<wire x1="4.826" y1="-0.508" x2="4.826" y2="-1.524" width="0.1998" layer="94"/>
<wire x1="4.826" y1="-1.524" x2="7.366" y2="-1.524" width="0.1998" layer="94"/>
<wire x1="7.366" y1="-1.524" x2="7.366" y2="-0.254" width="0.1998" layer="94"/>
<wire x1="7.366" y1="-0.254" x2="8.89" y2="-0.254" width="0.1998" layer="94"/>
<wire x1="8.89" y1="-0.254" x2="8.89" y2="2.794" width="0.1998" layer="94"/>
<wire x1="8.89" y1="2.794" x2="7.366" y2="2.794" width="0.1998" layer="94"/>
<wire x1="7.366" y1="2.794" x2="7.366" y2="4.064" width="0.1998" layer="94"/>
<wire x1="7.366" y1="4.064" x2="4.826" y2="4.064" width="0.1998" layer="94"/>
<wire x1="4.826" y1="3.048" x2="5.588" y2="3.048" width="0.1998" layer="94"/>
<wire x1="4.826" y1="2.54" x2="5.588" y2="2.54" width="0.1998" layer="94"/>
<wire x1="4.826" y1="2.032" x2="5.588" y2="2.032" width="0.1998" layer="94"/>
<wire x1="4.826" y1="1.524" x2="5.588" y2="1.524" width="0.1998" layer="94"/>
<wire x1="4.826" y1="1.016" x2="5.588" y2="1.016" width="0.1998" layer="94"/>
<wire x1="4.826" y1="0.508" x2="5.588" y2="0.508" width="0.1998" layer="94"/>
<wire x1="4.826" y1="0" x2="5.588" y2="0" width="0.1998" layer="94"/>
<wire x1="4.826" y1="-0.508" x2="5.588" y2="-0.508" width="0.1998" layer="94"/>
<wire x1="3.302" y1="8.636" x2="3.302" y2="9.144" width="0.127" layer="94"/>
<wire x1="3.302" y1="7.366" x2="3.302" y2="7.874" width="0.127" layer="94"/>
<wire x1="3.302" y1="6.096" x2="3.302" y2="6.604" width="0.127" layer="94"/>
<wire x1="3.302" y1="4.826" x2="3.302" y2="5.334" width="0.127" layer="94"/>
<wire x1="3.302" y1="3.556" x2="3.302" y2="4.064" width="0.127" layer="94"/>
<wire x1="3.302" y1="2.286" x2="3.302" y2="2.794" width="0.127" layer="94"/>
<wire x1="3.302" y1="1.016" x2="3.302" y2="1.524" width="0.127" layer="94"/>
<wire x1="3.302" y1="-0.254" x2="3.302" y2="0.254" width="0.127" layer="94"/>
<wire x1="3.302" y1="-1.524" x2="3.302" y2="-1.016" width="0.127" layer="94"/>
<wire x1="3.302" y1="-2.794" x2="3.302" y2="-2.286" width="0.127" layer="94"/>
<wire x1="3.302" y1="-4.064" x2="3.302" y2="-3.556" width="0.127" layer="94"/>
<wire x1="3.302" y1="-5.334" x2="3.302" y2="-4.826" width="0.127" layer="94"/>
<wire x1="3.302" y1="-6.604" x2="3.302" y2="-6.096" width="0.127" layer="94"/>
<wire x1="3.302" y1="-7.874" x2="3.302" y2="-7.366" width="0.127" layer="94"/>
<wire x1="3.302" y1="-9.144" x2="3.302" y2="-8.636" width="0.127" layer="94"/>
<text x="3.81" y="10.668" size="1.778" layer="95">&gt;NAME</text>
<text x="3.81" y="-10.922" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="10.16" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="3" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="4" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="5" x="-2.54" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="6" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="7" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="8" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="S2" x="2.54" y="-12.7" visible="off" length="short" direction="pas" swaplevel="2" rot="R90"/>
<pin name="S1" x="0" y="-12.7" visible="off" length="short" direction="pas" swaplevel="2" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GLX-S-88M" urn="urn:adsk.eagle:component:7684/1" prefix="X" library_version="1">
<description>&lt;b&gt;Mod. Jack, Right Angle, 8 posiotion, 8 contatcs&lt;/b&gt; RJ45&lt;p&gt;
Source: GLX-S-88M.pdf</description>
<gates>
<gate name="G$1" symbol="JACK8SH" x="0" y="0"/>
</gates>
<devices>
<device name="" package="GLX-S-88M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="S1" pad="S1"/>
<connect gate="G$1" pin="S2" pad="S2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7680/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="J2" library="SAMTEC-FTSH-120-01-X-DV" deviceset="SAMTEC-FTSH-120-01-X-DV" device="J"/>
<part name="IC1" library="ams1117-3.3v" deviceset="AMS1117-3.3V" device=""/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="X_1" library="ngspice-simulation" library_urn="urn:adsk.eagle:library:527439" deviceset="GND" device=""/>
<part name="RX,TX2" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2021" device="" package3d_urn="urn:adsk.eagle:package:8078633/1"/>
<part name="RX,TX" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2021" device="" package3d_urn="urn:adsk.eagle:package:8078633/1"/>
<part name="GPIO27(LED7)" library="adafruit" deviceset="LED" device="CHIPLED_0805"/>
<part name="R2" library="RHD2000_eval_board" deviceset="R" device="_1206"/>
<part name="GPIO26(LED8)" library="adafruit" deviceset="LED" device="CHIPLED_0805"/>
<part name="GPIO17(LED6)" library="adafruit" deviceset="LED" device="CHIPLED_0805"/>
<part name="GPIO13(LED2)" library="adafruit" deviceset="LED" device="CHIPLED_0805"/>
<part name="GPIO1(LED1)" library="adafruit" deviceset="LED" device="CHIPLED_0805"/>
<part name="GPIO0(LED0)" library="adafruit" deviceset="LED" device="CHIPLED_0805"/>
<part name="U$1" library="diy-modules" deviceset="BATTERY-LI-MH12210" device=""/>
<part name="Q1" library="adafruit" deviceset="MOSFET-N" device="REFLOW"/>
<part name="Q2" library="adafruit" deviceset="MOSFET-N" device="REFLOW"/>
<part name="Q3" library="adafruit" deviceset="MOSFET-N" device="REFLOW"/>
<part name="Q4" library="adafruit" deviceset="MOSFET-N" device="REFLOW"/>
<part name="Q5" library="adafruit" deviceset="MOSFET-N" device="REFLOW"/>
<part name="Q6" library="adafruit" deviceset="MOSFET-N" device="REFLOW"/>
<part name="LED7(-,+)" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2021" device="" package3d_urn="urn:adsk.eagle:package:8078633/1"/>
<part name="LED8(-,+)" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2021" device="" package3d_urn="urn:adsk.eagle:package:8078633/1"/>
<part name="LED6(-,+)" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2021" device="" package3d_urn="urn:adsk.eagle:package:8078633/1"/>
<part name="LED2(-,+)" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2021" device="" package3d_urn="urn:adsk.eagle:package:8078633/1"/>
<part name="LED1(-,+)" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2021" device="" package3d_urn="urn:adsk.eagle:package:8078633/1"/>
<part name="LED0(-,+)" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2021" device="" package3d_urn="urn:adsk.eagle:package:8078633/1"/>
<part name="X1" library="con-kycon" library_urn="urn:adsk.eagle:library:157" deviceset="GLX-S-88M" device="" package3d_urn="urn:adsk.eagle:package:7680/1"/>
<part name="GND,VCC" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2021" device="" package3d_urn="urn:adsk.eagle:package:8078633/1"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="116.76" y="9.62" size="1.6764" layer="91">1
3
5
7
9
11
13
15
17
19
21
23
25
27
29
31
33
35
37
39</text>
<text x="100.14" y="10.16" size="1.6764" layer="91">2
4
6
8
10
12
14
16
18
20
22
24
26
28
30
32
34
36
38
40</text>
</plain>
<instances>
<instance part="J2" gate="A" x="99.06" y="58.42" smashed="yes">
<attribute name="NAME" x="99.06" y="63.5" size="2.54" layer="95"/>
<attribute name="VALUE" x="99.06" y="0" size="2.286" layer="96"/>
</instance>
<instance part="IC1" gate="G$1" x="15.24" y="71.12" smashed="yes">
<attribute name="NAME" x="62.23" y="78.74" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="62.23" y="76.2" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="P+1" gate="VCC" x="-7.62" y="121.92" smashed="yes">
<attribute name="VALUE" x="-10.16" y="119.38" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="X_1" gate="G$1" x="-27.94" y="109.22" smashed="yes"/>
<instance part="RX,TX2" gate="-1" x="102.66" y="-19.9" smashed="yes">
<attribute name="NAME" x="105.2" y="-20.662" size="1.524" layer="95"/>
<attribute name="VALUE" x="101.898" y="-18.503" size="1.778" layer="96"/>
</instance>
<instance part="RX,TX2" gate="-2" x="102.66" y="-22.44" smashed="yes">
<attribute name="NAME" x="105.2" y="-23.202" size="1.524" layer="95"/>
</instance>
<instance part="RX,TX" gate="-1" x="120.44" y="-19.9" smashed="yes">
<attribute name="NAME" x="122.98" y="-20.662" size="1.524" layer="95"/>
<attribute name="VALUE" x="119.678" y="-18.503" size="1.778" layer="96"/>
</instance>
<instance part="RX,TX" gate="-2" x="120.44" y="-22.44" smashed="yes">
<attribute name="NAME" x="122.98" y="-23.202" size="1.524" layer="95"/>
</instance>
<instance part="GPIO27(LED7)" gate="G$1" x="175" y="49" smashed="yes">
<attribute name="NAME" x="178.556" y="44.428" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="180.715" y="44.428" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R2" gate="R" x="175" y="35" smashed="yes" rot="R90">
<attribute name="NAME" x="173.5014" y="31.19" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="178.302" y="31.19" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GPIO26(LED8)" gate="G$1" x="191" y="49" smashed="yes">
<attribute name="NAME" x="194.556" y="44.428" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="196.715" y="44.428" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GPIO17(LED6)" gate="G$1" x="206" y="49" smashed="yes">
<attribute name="NAME" x="209.556" y="44.428" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="211.715" y="44.428" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GPIO13(LED2)" gate="G$1" x="223" y="49" smashed="yes">
<attribute name="NAME" x="226.556" y="44.428" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="228.715" y="44.428" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GPIO1(LED1)" gate="G$1" x="251" y="49" smashed="yes">
<attribute name="NAME" x="254.556" y="44.428" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="256.715" y="44.428" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GPIO0(LED0)" gate="G$1" x="265" y="49" smashed="yes">
<attribute name="NAME" x="268.556" y="44.428" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="270.715" y="44.428" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="U$1" gate="G$1" x="-22" y="156" smashed="yes">
<attribute name="NAME" x="-37.24" y="168.7" size="1.778" layer="95"/>
<attribute name="VALUE" x="-37.24" y="166.16" size="1.778" layer="96"/>
</instance>
<instance part="Q1" gate="G$1" x="172.3898" y="79.1464" smashed="yes">
<attribute name="NAME" x="177.4698" y="79.7814" size="1.27" layer="95"/>
<attribute name="VALUE" x="177.4698" y="77.8764" size="1.27" layer="96"/>
</instance>
<instance part="Q2" gate="G$1" x="191.2112" y="79.3496" smashed="yes">
<attribute name="NAME" x="196.2912" y="79.9846" size="1.27" layer="95"/>
<attribute name="VALUE" x="196.2912" y="78.0796" size="1.27" layer="96"/>
</instance>
<instance part="Q3" gate="G$1" x="205.0034" y="79.3496" smashed="yes">
<attribute name="NAME" x="210.0834" y="79.9846" size="1.27" layer="95"/>
<attribute name="VALUE" x="210.0834" y="78.0796" size="1.27" layer="96"/>
</instance>
<instance part="Q4" gate="G$1" x="220.2688" y="79.1464" smashed="yes">
<attribute name="NAME" x="225.3488" y="79.7814" size="1.27" layer="95"/>
<attribute name="VALUE" x="225.3488" y="77.8764" size="1.27" layer="96"/>
</instance>
<instance part="Q5" gate="G$1" x="247.6246" y="77.8764" smashed="yes">
<attribute name="NAME" x="252.7046" y="78.5114" size="1.27" layer="95"/>
<attribute name="VALUE" x="252.7046" y="76.6064" size="1.27" layer="96"/>
</instance>
<instance part="Q6" gate="G$1" x="263.7282" y="77.8764" smashed="yes">
<attribute name="NAME" x="268.8082" y="78.5114" size="1.27" layer="95"/>
<attribute name="VALUE" x="268.8082" y="76.6064" size="1.27" layer="96"/>
</instance>
<instance part="LED7(-,+)" gate="-1" x="163.6708" y="97.6258" smashed="yes">
<attribute name="NAME" x="166.2108" y="96.8638" size="1.524" layer="95"/>
<attribute name="VALUE" x="162.9088" y="99.0228" size="1.778" layer="96"/>
</instance>
<instance part="LED7(-,+)" gate="-2" x="163.6268" y="95.0976" smashed="yes">
<attribute name="NAME" x="166.1668" y="94.3356" size="1.524" layer="95"/>
</instance>
<instance part="LED8(-,+)" gate="-1" x="182.9494" y="97.6258" smashed="yes">
<attribute name="NAME" x="185.4894" y="96.8638" size="1.524" layer="95"/>
<attribute name="VALUE" x="182.1874" y="99.0228" size="1.778" layer="96"/>
</instance>
<instance part="LED8(-,+)" gate="-2" x="183.1594" y="94.9706" smashed="yes">
<attribute name="NAME" x="185.6994" y="94.2086" size="1.524" layer="95"/>
</instance>
<instance part="LED6(-,+)" gate="-1" x="203.8028" y="97.6258" smashed="yes">
<attribute name="NAME" x="206.3428" y="96.8638" size="1.524" layer="95"/>
<attribute name="VALUE" x="203.0408" y="99.0228" size="1.778" layer="96"/>
</instance>
<instance part="LED6(-,+)" gate="-2" x="204.3938" y="94.8182" smashed="yes">
<attribute name="NAME" x="206.9338" y="94.0562" size="1.524" layer="95"/>
</instance>
<instance part="LED2(-,+)" gate="-1" x="225.596" y="97.6258" smashed="yes">
<attribute name="NAME" x="228.136" y="96.8638" size="1.524" layer="95"/>
<attribute name="VALUE" x="224.834" y="99.0228" size="1.778" layer="96"/>
</instance>
<instance part="LED2(-,+)" gate="-2" x="226.187" y="94.8182" smashed="yes">
<attribute name="NAME" x="228.727" y="94.0562" size="1.524" layer="95"/>
</instance>
<instance part="LED1(-,+)" gate="-1" x="246.0684" y="97.6258" smashed="yes">
<attribute name="NAME" x="248.6084" y="96.8638" size="1.524" layer="95"/>
<attribute name="VALUE" x="245.3064" y="99.0228" size="1.778" layer="96"/>
</instance>
<instance part="LED1(-,+)" gate="-2" x="246.6594" y="94.8182" smashed="yes">
<attribute name="NAME" x="249.1994" y="94.0562" size="1.524" layer="95"/>
</instance>
<instance part="LED0(-,+)" gate="-1" x="271.7478" y="97.4226" smashed="yes">
<attribute name="NAME" x="274.2878" y="96.6606" size="1.524" layer="95"/>
<attribute name="VALUE" x="270.9858" y="98.8196" size="1.778" layer="96"/>
</instance>
<instance part="LED0(-,+)" gate="-2" x="272.7198" y="94.4372" smashed="yes">
<attribute name="NAME" x="275.2598" y="93.6752" size="1.524" layer="95"/>
</instance>
<instance part="X1" gate="G$1" x="108.6612" y="-64.1096" smashed="yes">
<attribute name="NAME" x="112.4712" y="-53.4416" size="1.778" layer="95"/>
<attribute name="VALUE" x="112.4712" y="-75.0316" size="1.778" layer="96"/>
</instance>
<instance part="GND,VCC" gate="-1" x="150.92" y="-19.9" smashed="yes">
<attribute name="NAME" x="153.46" y="-20.662" size="1.524" layer="95"/>
<attribute name="VALUE" x="150.158" y="-18.503" size="1.778" layer="96"/>
</instance>
<instance part="GND,VCC" gate="-2" x="149.86" y="-22.86" smashed="yes">
<attribute name="NAME" x="152.4" y="-23.622" size="1.524" layer="95"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="X_1" gate="G$1" pin="0"/>
<wire x1="-27.94" y1="109.22" x2="-27.94" y2="111.76" width="0.1524" layer="91"/>
<label x="-27.94" y="111.76" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="BAT-"/>
<wire x1="-42.32" y1="156" x2="-42.343059375" y2="112.635975" width="0.1524" layer="91"/>
<wire x1="-42.343059375" y1="112.635975" x2="-27.94" y2="109.22" width="0.1524" layer="91"/>
<junction x="-27.94" y="109.22"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="GROUND/ADJUST"/>
<wire x1="15.24" y1="71.12" x2="12.7" y2="71.12" width="0.1524" layer="91"/>
<label x="10.16" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="A" pin="1"/>
<wire x1="93.98" y1="58.42" x2="91.44" y2="58.42" width="0.1524" layer="91"/>
<label x="87" y="58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="A" pin="28"/>
<wire x1="124.46" y1="25.4" x2="124.46" y2="25" width="0.1524" layer="91"/>
<wire x1="124.46" y1="25" x2="127" y2="25" width="0.1524" layer="91"/>
<label x="127" y="25" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="A" pin="19"/>
<wire x1="93.98" y1="35.56" x2="90" y2="35.56" width="0.1524" layer="91"/>
<wire x1="90" y1="35.56" x2="90" y2="36" width="0.1524" layer="91"/>
<label x="85" y="36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="A" pin="25"/>
<wire x1="93.98" y1="27.94" x2="93.98" y2="28" width="0.1524" layer="91"/>
<wire x1="93.98" y1="28" x2="90" y2="28" width="0.1524" layer="91"/>
<label x="85" y="27" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="A" pin="39"/>
<wire x1="93.98" y1="10.16" x2="93.98" y2="10" width="0.1524" layer="91"/>
<wire x1="93.98" y1="10" x2="91" y2="10" width="0.1524" layer="91"/>
<label x="85" y="10" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="A" pin="40"/>
<wire x1="124.46" y1="10.16" x2="124.46" y2="10" width="0.1524" layer="91"/>
<wire x1="124.46" y1="10" x2="127" y2="10" width="0.1524" layer="91"/>
<label x="127" y="10" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R2" gate="R" pin="1"/>
<wire x1="175" y1="29.92" x2="175" y2="29" width="0.1524" layer="91"/>
<label x="175" y="27" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q1" gate="G$1" pin="S"/>
<wire x1="174.9298" y1="74.0664" x2="174.9044" y2="72.2376" width="0.1524" layer="91"/>
<wire x1="174.9044" y1="72.2376" x2="193.929" y2="72.2376" width="0.1524" layer="91"/>
<pinref part="Q2" gate="G$1" pin="S"/>
<wire x1="193.929" y1="72.2376" x2="193.7512" y2="74.2696" width="0.1524" layer="91"/>
<pinref part="Q3" gate="G$1" pin="S"/>
<wire x1="193.7512" y1="74.2696" x2="207.5434" y2="74.2696" width="0.1524" layer="91"/>
<junction x="193.7512" y="74.2696"/>
<pinref part="Q4" gate="G$1" pin="S"/>
<wire x1="207.5434" y1="74.2696" x2="222.8088" y2="74.0664" width="0.1524" layer="91"/>
<junction x="207.5434" y="74.2696"/>
<label x="193.3448" y="69.9008" size="1.778" layer="95"/>
<wire x1="222.8088" y1="73.5076" x2="222.8088" y2="74.0664" width="0.1524" layer="91"/>
<junction x="222.8088" y="74.0664"/>
<pinref part="Q5" gate="G$1" pin="S"/>
<wire x1="250.1646" y1="72.7964" x2="222.8088" y2="74.0664" width="0.1524" layer="91"/>
<pinref part="Q6" gate="G$1" pin="S"/>
<wire x1="266.2682" y1="72.7964" x2="250.1646" y2="72.7964" width="0.1524" layer="91"/>
<junction x="250.1646" y="72.7964"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="8"/>
<wire x1="106.1212" y1="-71.7296" x2="102.7938" y2="-71.7042" width="0.1524" layer="91"/>
<label x="96.774" y="-71.9836" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="GND,VCC" gate="-1" pin="S"/>
<wire x1="148.38" y1="-19.9" x2="144.78" y2="-20.32" width="0.1524" layer="91"/>
<label x="142.24" y="-20.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="P+1" gate="VCC" pin="VCC"/>
<wire x1="-7.62" y1="119.38" x2="-7.62" y2="116.84" width="0.1524" layer="91"/>
<label x="-7.62" y="114.3" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="BAT+"/>
<wire x1="-1.68" y1="156" x2="3.5306" y2="155.956" width="0.1524" layer="91"/>
<wire x1="3.5306" y1="155.956" x2="3.5306" y2="102.7938" width="0.1524" layer="91"/>
<wire x1="3.5306" y1="102.7938" x2="-7.366" y2="102.7938" width="0.1524" layer="91"/>
<wire x1="-7.366" y1="102.7938" x2="-7.62" y2="119.38" width="0.1524" layer="91"/>
<junction x="-7.62" y="119.38"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="VIN"/>
<wire x1="15.24" y1="66.04" x2="12.7" y2="66.04" width="0.1524" layer="91"/>
<label x="10.16" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="LED7(-,+)" gate="-2" pin="S"/>
<pinref part="LED8(-,+)" gate="-2" pin="S"/>
<wire x1="161.0868" y1="95.0976" x2="180.6194" y2="94.9706" width="0.1524" layer="91"/>
<pinref part="LED6(-,+)" gate="-2" pin="S"/>
<wire x1="180.6194" y1="94.9706" x2="201.8538" y2="94.8182" width="0.1524" layer="91"/>
<junction x="180.6194" y="94.9706"/>
<pinref part="LED2(-,+)" gate="-2" pin="S"/>
<wire x1="201.8538" y1="94.8182" x2="223.647" y2="94.8182" width="0.1524" layer="91"/>
<junction x="201.8538" y="94.8182"/>
<pinref part="LED1(-,+)" gate="-2" pin="S"/>
<wire x1="223.647" y1="94.8182" x2="244.1194" y2="94.8182" width="0.1524" layer="91"/>
<junction x="223.647" y="94.8182"/>
<pinref part="LED0(-,+)" gate="-2" pin="S"/>
<wire x1="244.1194" y1="94.8182" x2="270.1798" y2="94.4372" width="0.1524" layer="91"/>
<junction x="244.1194" y="94.8182"/>
<wire x1="161.0868" y1="95.0976" x2="161.0868" y2="90.6272" width="0.1524" layer="91"/>
<junction x="161.0868" y="95.0976"/>
<label x="160.9852" y="88.5444" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="GND,VCC" gate="-2" pin="S"/>
<wire x1="147.32" y1="-22.86" x2="144.78" y2="-22.86" width="0.1524" layer="91"/>
<label x="142.24" y="-22.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="VCC33" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="VOUT"/>
<wire x1="15.24" y1="68.58" x2="7.62" y2="68.58" width="0.1524" layer="91"/>
<label x="0" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="A" pin="32"/>
<wire x1="124.46" y1="20.32" x2="124.46" y2="20" width="0.1524" layer="91"/>
<wire x1="124.46" y1="20" x2="127" y2="20" width="0.1524" layer="91"/>
<pinref part="J2" gate="A" pin="30"/>
<wire x1="124.46" y1="22.86" x2="127" y2="22.86" width="0.1524" layer="91"/>
<wire x1="127" y1="22.86" x2="127" y2="20" width="0.1524" layer="91"/>
<wire x1="127" y1="20" x2="128" y2="20" width="0.1524" layer="91"/>
<junction x="127" y="20"/>
<label x="128" y="20" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="A" pin="2"/>
<wire x1="124.46" y1="58.42" x2="128" y2="58.42" width="0.1524" layer="91"/>
<wire x1="128" y1="58.42" x2="128" y2="58" width="0.1524" layer="91"/>
<label x="128" y="58" size="1.778" layer="95"/>
</segment>
</net>
<net name="TX" class="0">
<segment>
<pinref part="RX,TX" gate="-1" pin="S"/>
<pinref part="RX,TX2" gate="-1" pin="S"/>
<wire x1="117.9" y1="-19.9" x2="100.12" y2="-19.9" width="0.1524" layer="91"/>
<wire x1="100.12" y1="-19.9" x2="95.04" y2="-19.9" width="0.1524" layer="91"/>
<junction x="100.12" y="-19.9"/>
<label x="92.5" y="-19.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="A" pin="37"/>
<wire x1="93.98" y1="12.7" x2="92" y2="12.7" width="0.1524" layer="91"/>
<wire x1="92" y1="12.7" x2="92" y2="13" width="0.1524" layer="91"/>
<label x="89" y="13" size="1.778" layer="95"/>
</segment>
</net>
<net name="RX" class="0">
<segment>
<pinref part="RX,TX" gate="-2" pin="S"/>
<pinref part="RX,TX2" gate="-2" pin="S"/>
<wire x1="117.9" y1="-22.44" x2="100.12" y2="-22.44" width="0.1524" layer="91"/>
<wire x1="100.12" y1="-22.44" x2="95.04" y2="-22.44" width="0.1524" layer="91"/>
<junction x="100.12" y="-22.44"/>
<label x="92.5" y="-22.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="A" pin="38"/>
<wire x1="124.46" y1="12.7" x2="124.46" y2="13" width="0.1524" layer="91"/>
<wire x1="124.46" y1="13" x2="127" y2="13" width="0.1524" layer="91"/>
<label x="127" y="13" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="GPIO27(LED7)" gate="G$1" pin="C"/>
<pinref part="R2" gate="R" pin="2"/>
<wire x1="175" y1="43.92" x2="175" y2="41" width="0.1524" layer="91"/>
<pinref part="GPIO26(LED8)" gate="G$1" pin="C"/>
<wire x1="175" y1="41" x2="175" y2="40.08" width="0.1524" layer="91"/>
<wire x1="191" y1="43.92" x2="191" y2="41" width="0.1524" layer="91"/>
<wire x1="191" y1="41" x2="175" y2="41" width="0.1524" layer="91"/>
<junction x="175" y="41"/>
<wire x1="191" y1="41" x2="206" y2="41" width="0.1524" layer="91"/>
<junction x="191" y="41"/>
<pinref part="GPIO17(LED6)" gate="G$1" pin="C"/>
<wire x1="206" y1="41" x2="206" y2="43.92" width="0.1524" layer="91"/>
<wire x1="206" y1="41" x2="223" y2="41" width="0.1524" layer="91"/>
<junction x="206" y="41"/>
<wire x1="223" y1="41" x2="223" y2="43.92" width="0.1524" layer="91"/>
<pinref part="GPIO13(LED2)" gate="G$1" pin="C"/>
<wire x1="237" y1="43.92" x2="237" y2="41" width="0.1524" layer="91"/>
<pinref part="GPIO1(LED1)" gate="G$1" pin="C"/>
<wire x1="237" y1="41" x2="223" y2="41" width="0.1524" layer="91"/>
<wire x1="251" y1="43.92" x2="250" y2="41" width="0.1524" layer="91"/>
<wire x1="250" y1="41" x2="237" y2="41" width="0.1524" layer="91"/>
<pinref part="GPIO0(LED0)" gate="G$1" pin="C"/>
<wire x1="265" y1="43.92" x2="265" y2="41" width="0.1524" layer="91"/>
<wire x1="265" y1="41" x2="250" y2="41" width="0.1524" layer="91"/>
<junction x="250" y="41"/>
</segment>
</net>
<net name="GPIO27(LED7)" class="0">
<segment>
<pinref part="J2" gate="A" pin="20"/>
<wire x1="124.46" y1="35.56" x2="127" y2="36" width="0.1524" layer="91"/>
<label x="127" y="36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="GPIO27(LED7)" gate="G$1" pin="A"/>
<wire x1="175" y1="51.54" x2="175" y2="53" width="0.1524" layer="91"/>
<label x="158" y="54" size="1.778" layer="95"/>
<pinref part="Q1" gate="G$1" pin="G"/>
<wire x1="169.8498" y1="76.6064" x2="165.2524" y2="76.6318" width="0.1524" layer="91"/>
<wire x1="165.2524" y1="76.6318" x2="165.2524" y2="59.2836" width="0.1524" layer="91"/>
<wire x1="165.2524" y1="59.2836" x2="174.879" y2="59.2836" width="0.1524" layer="91"/>
<wire x1="174.879" y1="59.2836" x2="175" y2="51.54" width="0.1524" layer="91"/>
<junction x="175" y="51.54"/>
</segment>
</net>
<net name="GPIO26(LED8)" class="0">
<segment>
<pinref part="J2" gate="A" pin="22"/>
<wire x1="124.46" y1="33.02" x2="128" y2="33" width="0.1524" layer="91"/>
<label x="129" y="33" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q2" gate="G$1" pin="G"/>
<pinref part="GPIO26(LED8)" gate="G$1" pin="A"/>
<wire x1="188.6712" y1="76.8096" x2="191" y2="51.54" width="0.1524" layer="91"/>
<label x="182.2196" y="65.7606" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIO17(LED6)" class="0">
<segment>
<pinref part="J2" gate="A" pin="34"/>
<wire x1="124.46" y1="17.78" x2="127" y2="18" width="0.1524" layer="91"/>
<label x="128" y="17" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q3" gate="G$1" pin="G"/>
<wire x1="202.4634" y1="76.8096" x2="202.4634" y2="51.7652" width="0.1524" layer="91"/>
<pinref part="GPIO17(LED6)" gate="G$1" pin="A"/>
<wire x1="202.4634" y1="51.7652" x2="206" y2="51.54" width="0.1524" layer="91"/>
<label x="198.3232" y="61.7474" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIO13(LED2)" class="0">
<segment>
<pinref part="J2" gate="A" pin="33"/>
<wire x1="93.98" y1="17.78" x2="91" y2="18" width="0.1524" layer="91"/>
<label x="77" y="17" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="GPIO13(LED2)" gate="G$1" pin="A"/>
<wire x1="223" y1="51.54" x2="218.5924" y2="51.5366" width="0.1524" layer="91"/>
<pinref part="Q4" gate="G$1" pin="G"/>
<wire x1="218.5924" y1="51.5366" x2="217.7288" y2="76.6064" width="0.1524" layer="91"/>
<label x="218.3892" y="67.691" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIO1(LED1)" class="0">
<segment>
<pinref part="J2" gate="A" pin="36"/>
<wire x1="124.46" y1="15.24" x2="133" y2="15" width="0.1524" layer="91"/>
<label x="134" y="14" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q5" gate="G$1" pin="G"/>
<wire x1="245.0846" y1="75.3364" x2="245.0592" y2="50.9524" width="0.1524" layer="91"/>
<pinref part="GPIO1(LED1)" gate="G$1" pin="A"/>
<wire x1="245.0592" y1="50.9524" x2="251" y2="51.54" width="0.1524" layer="91"/>
<label x="240.7412" y="61.6458" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIO0(LED0)" class="0">
<segment>
<pinref part="J2" gate="A" pin="35"/>
<wire x1="93.98" y1="15.24" x2="88" y2="15" width="0.1524" layer="91"/>
<label x="73" y="15" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q6" gate="G$1" pin="G"/>
<pinref part="GPIO0(LED0)" gate="G$1" pin="A"/>
<wire x1="261.1882" y1="75.3364" x2="265" y2="51.54" width="0.1524" layer="91"/>
<label x="261.5438" y="67.9704" size="1.778" layer="95"/>
</segment>
</net>
<net name="LED7" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="D"/>
<wire x1="174.9298" y1="84.2264" x2="174.9298" y2="87.0204" width="0.1524" layer="91"/>
<pinref part="LED7(-,+)" gate="-1" pin="S"/>
<wire x1="161.1308" y1="97.6258" x2="174.9298" y2="84.2264" width="0.1524" layer="91"/>
<junction x="174.9298" y="84.2264"/>
<label x="154.94" y="96.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="LED8" class="0">
<segment>
<pinref part="Q2" gate="G$1" pin="D"/>
<wire x1="193.7512" y1="84.4296" x2="193.7766" y2="87.0204" width="0.1524" layer="91"/>
<pinref part="LED8(-,+)" gate="-1" pin="S"/>
<wire x1="180.4094" y1="97.6258" x2="193.7512" y2="84.4296" width="0.1524" layer="91"/>
<junction x="193.7512" y="84.4296"/>
<label x="177.8" y="101.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="LED6" class="0">
<segment>
<pinref part="Q3" gate="G$1" pin="D"/>
<wire x1="207.5434" y1="84.4296" x2="207.5942" y2="86.741" width="0.1524" layer="91"/>
<pinref part="LED6(-,+)" gate="-1" pin="S"/>
<wire x1="201.2628" y1="97.6258" x2="207.5434" y2="84.4296" width="0.1524" layer="91"/>
<junction x="207.5434" y="84.4296"/>
<label x="200.66" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="LED2" class="0">
<segment>
<pinref part="Q4" gate="G$1" pin="D"/>
<wire x1="222.8088" y1="84.2264" x2="222.8088" y2="86.9442" width="0.1524" layer="91"/>
<pinref part="LED2(-,+)" gate="-1" pin="S"/>
<wire x1="223.056" y1="97.6258" x2="222.8088" y2="84.2264" width="0.1524" layer="91"/>
<junction x="222.8088" y="84.2264"/>
<label x="220.98" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="LED1" class="0">
<segment>
<pinref part="Q5" gate="G$1" pin="D"/>
<wire x1="250.1646" y1="82.9564" x2="250.1392" y2="85.3186" width="0.1524" layer="91"/>
<pinref part="LED1(-,+)" gate="-1" pin="S"/>
<wire x1="243.5284" y1="97.6258" x2="250.1646" y2="82.9564" width="0.1524" layer="91"/>
<junction x="250.1646" y="82.9564"/>
<label x="243.84" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="LED0" class="0">
<segment>
<pinref part="Q6" gate="G$1" pin="D"/>
<wire x1="266.2682" y1="82.9564" x2="266.2428" y2="84.9376" width="0.1524" layer="91"/>
<pinref part="LED0(-,+)" gate="-1" pin="S"/>
<wire x1="269.2078" y1="97.4226" x2="266.2682" y2="82.9564" width="0.1524" layer="91"/>
<junction x="266.2682" y="82.9564"/>
<label x="264.16" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="LAN_RX+" class="0">
<segment>
<pinref part="J2" gate="A" pin="11"/>
<wire x1="93.98" y1="45.72" x2="91.1098" y2="45.72" width="0.1524" layer="91"/>
<label x="81.4578" y="45.6438" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="3"/>
<wire x1="106.1212" y1="-59.0296" x2="102.235" y2="-59.1058" width="0.1524" layer="91"/>
<label x="90.9828" y="-59.4614" size="1.778" layer="95"/>
</segment>
</net>
<net name="LAN_RX-" class="0">
<segment>
<pinref part="J2" gate="A" pin="13"/>
<wire x1="93.98" y1="43.18" x2="91.4654" y2="43.2054" width="0.1524" layer="91"/>
<label x="82.5754" y="42.7482" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="6"/>
<wire x1="106.1212" y1="-66.6496" x2="102.235" y2="-66.675" width="0.1524" layer="91"/>
<label x="90.9574" y="-67.0052" size="1.778" layer="95"/>
</segment>
</net>
<net name="LAN_TX+" class="0">
<segment>
<pinref part="J2" gate="A" pin="15"/>
<wire x1="93.98" y1="40.64" x2="90.5764" y2="40.64" width="0.1524" layer="91"/>
<label x="80.8482" y="40.259" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="1"/>
<wire x1="106.1212" y1="-53.9496" x2="102.2096" y2="-53.8988" width="0.1524" layer="91"/>
<label x="91.9734" y="-53.8988" size="1.778" layer="95"/>
</segment>
</net>
<net name="LAN_TX-" class="0">
<segment>
<pinref part="J2" gate="A" pin="17"/>
<wire x1="93.98" y1="38.1" x2="78.7654" y2="38.0746" width="0.1524" layer="91"/>
<label x="68.2752" y="37.7952" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="2"/>
<wire x1="106.1212" y1="-56.4896" x2="103.2002" y2="-56.4642" width="0.1524" layer="91"/>
<label x="92.2274" y="-56.7436" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
