(.../Documents/EAGLE/ulps/pcb-gcode.ulp)
(Copyright 2005 - 2012 by John Johnson)
(See readme.txt for licensing terms.)
(This file generated from the board:)
(.../stepperDriver/stepperDriverScheme.brd)
(Current profile is .../EAGLE/ulps/profiles/generic.pp  )
(This file generated 22/11/19 16:23)
(Settings from pcb-machine.h)
(spindle on time = 0.0000)
(spindle speed = 20000.0000)
(tool change at 0.0000 0.0000 30.0000 )
(feed rate xy = F0.00  )
(feed rate z  = F20.00 )
(Z Axis Settings)
(  High     Up        Down     Drill)
(3.0000 	6.0000 	-0.0500 	-1.6700 )
(Settings from pcb-defaults.h)
(isolate min = 0.0254)
(isolate max = 1.0000)
(isolate step = 0.1000)
(Generated top outlines, top drill, top stencil, bottom outlines, bottom drill, )
(Unit of measure: mm)
( Tool|       Size       |  Min Sub |  Max Sub |   Count )
( T01  0.900mm 0.0354in 0.0000in 0.0000in )
( T02  1.016mm 0.0400in 0.0000in 0.0000in )
(Metric Mode)
G21
(Absolute Coordinates)
G90
S20000
G00 Z3.0000 
G00 X0.0000 Y0.0000 
M03
S10000
G04 P0.000000
M05
G00 Z30.0000 
M06 T01  ; 0.9000 
G00 Z6.0000 
M03
S10000
G04 P0.000000
G00 Z6.000000
G00 X-6.3500 Y12.7000 
G01 Z-1.6700 F20.00 
G01 Z6.000000
(R6.0000  P2.000000)
G00 X-8.8900 Y12.7000 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-11.4300 Y12.7000 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-13.9700 Y12.7000 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-16.5100 Y12.7000 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-19.0500 Y12.7000 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-21.5900 Y12.7000 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-24.1300 Y12.7000 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-24.1300 Y25.4000 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-21.5900 Y25.4000 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-19.0500 Y25.4000 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-16.5100 Y25.4000 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-13.9700 Y25.4000 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-11.4300 Y25.4000 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-8.8900 Y25.4000 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-6.3500 Y25.4000 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-6.3500 Y29.2100 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-8.8900 Y29.2100 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-11.4300 Y29.2100 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-13.9700 Y29.2100 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-16.5100 Y29.2100 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-19.0500 Y29.2100 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-21.5900 Y29.2100 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-24.1300 Y29.2100 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-24.1300 Y41.9100 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-21.5900 Y41.9100 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-19.0500 Y41.9100 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-16.5100 Y41.9100 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-13.9700 Y41.9100 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-11.4300 Y41.9100 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-8.8900 Y41.9100 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-6.3500 Y41.9100 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
M05
G00 Z30.0000 
G00 X0.0000 Y0.0000 
M06 T02  ; 1.0160 
G00 Z6.0000 
M03
S10000
G04 P0.000000
G00 Z6.000000
G00 X-11.4300 Y7.6200 
G01 Z-1.6700 F20.00 
G01 Z6.000000
(R6.0000  P2.000000)
G00 X-13.9700 Y7.6200 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-16.5100 Y7.6200 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-19.0500 Y7.6200 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-19.0500 Y46.9900 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-16.5100 Y46.9900 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-13.9700 Y46.9900 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
G00 X-11.4300 Y46.9900 
G01 Z-1.6700 F20.00 
G00 Z6.0000 
T01 
G00 Z3.0000 
M05
M02
