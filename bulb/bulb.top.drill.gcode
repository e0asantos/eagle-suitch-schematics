(.../Documents/EAGLE/ulps/pcb-gcode.ulp)
(Copyright 2005 - 2012 by John Johnson)
(See readme.txt for licensing terms.)
(This file generated from the board:)
(.../Documents/EAGLE/projects/bulb/bulb.brd)
(Current profile is .../EAGLE/ulps/profiles/generic.pp  )
(This file generated 10/11/19 21:01)
(Settings from pcb-machine.h)
(spindle on time = 0.0000)
(spindle speed = 20000.0000)
(tool change at 0.0000 0.0000 30.0000 )
(feed rate xy = F0.00  )
(feed rate z  = F50.00 )
(Z Axis Settings)
(  High     Up        Down     Drill)
(3.0000 	2.0000 	-0.0700 	-1.6700 )
(Settings from pcb-defaults.h)
(isolate min = 0.0254)
(isolate max = 1.0000)
(isolate step = 0.1000)
(Generated top outlines, top drill, top stencil, )
(Unit of measure: mm)
( Tool|       Size       |  Min Sub |  Max Sub |   Count )
( T01  0.800mm 0.0315in 0.0000in 0.0000in )
( T02  1.016mm 0.0400in 0.0000in 0.0000in )
(Metric Mode)
G21
(Absolute Coordinates)
G90
S20000
G00 Z3.0000 
G00 X0.0000 Y0.0000 
M03
S10000
G04 P0.000000
M05
G00 Z30.0000 
M06 T01  ; 0.8000 
G00 Z2.0000 
M03
S10000
G04 P0.000000
G00 Z2.000000
G00 X6.1455 Y4.8110 
G01 Z-1.6700 F50.00 
G01 Z2.000000
(R2.0000  P2.000000)
G00 X8.6855 Y4.8110 
G01 Z-1.6700 F50.00 
G00 Z2.0000 
G00 X8.6855 Y7.3510 
G01 Z-1.6700 F50.00 
G00 Z2.0000 
G00 X6.1455 Y7.3510 
G01 Z-1.6700 F50.00 
G00 Z2.0000 
G00 X6.1455 Y9.8910 
G01 Z-1.6700 F50.00 
G00 Z2.0000 
G00 X6.1455 Y12.4310 
G01 Z-1.6700 F50.00 
G00 Z2.0000 
G00 X8.6855 Y12.4310 
G01 Z-1.6700 F50.00 
G00 Z2.0000 
G00 X8.6855 Y9.8910 
G01 Z-1.6700 F50.00 
G00 Z2.0000 
M05
G00 Z30.0000 
G00 X0.0000 Y0.0000 
M06 T02  ; 1.0160 
G00 Z2.0000 
M03
S10000
G04 P0.000000
G00 Z2.000000
G00 X12.1920 Y10.9220 
G01 Z-1.6700 F50.00 
G01 Z2.000000
(R2.0000  P2.000000)
G00 X14.7320 Y10.9220 
G01 Z-1.6700 F50.00 
G00 Z2.0000 
G00 X17.2720 Y10.9220 
G01 Z-1.6700 F50.00 
G00 Z2.0000 
G00 X22.6060 Y10.1600 
G01 Z-1.6700 F50.00 
G00 Z2.0000 
G00 X22.6060 Y7.6200 
G01 Z-1.6700 F50.00 
G00 Z2.0000 
G00 X22.6060 Y5.0800 
G01 Z-1.6700 F50.00 
G00 Z2.0000 
G00 X26.4160 Y4.8260 
G01 Z-1.6700 F50.00 
G00 Z2.0000 
G00 X26.4160 Y7.3660 
G01 Z-1.6700 F50.00 
G00 Z2.0000 
G00 X26.4160 Y9.9060 
G01 Z-1.6700 F50.00 
G00 Z2.0000 
T01 
G00 Z3.0000 
M05
M02
