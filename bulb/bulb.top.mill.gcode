(.../Documents/EAGLE/ulps/pcb-gcode.ulp)
(Copyright 2005 - 2012 by John Johnson)
(See readme.txt for licensing terms.)
(This file generated from the board:)
(.../Documents/EAGLE/projects/bulb/bulb.brd)
(Current profile is .../EAGLE/ulps/profiles/generic.pp  )
(This file generated 10/11/19 21:01)
(Settings from pcb-machine.h)
(spindle on time = 0.0000)
(spindle speed = 30000.0000)
(milling depth = -1.6500)
(tool change at 0.0000 0.0000 30.0000 )
(feed rate xy = F60.00 )
(feed rate z  = F20.00 )
(Z Axis Settings)
(  High     Up        Down     Drill)
(3.0000 	2.0000 	-0.0700 	-1.6700 )
(Settings from pcb-defaults.h)
(isolate min = 0.0254)
(isolate max = 1.0000)
(isolate step = 0.1000)
(Generated top outlines, top drill, top stencil, )
(Unit of measure: mm)
(Metric Mode)
G21
(Absolute Coordinates)
G90
S30000
G00 Z3.0000 
G00 X0.0000 Y0.0000 
M03
S10000
G04 P0.000000
G00 Z2.0000 
G00 X3.6830 Y0.6350 
G01 Z-0.2750 F20.00 
G01 X30.9880 Y0.6350 F60.00 
G01 X30.9880 Y18.1610 
G01 X3.6830 Y18.1610 
G01 X3.6830 Y0.6350 
G00 Z2.0000 
G01 Z-0.5500 F20.00 
G01 X30.9880 Y0.6350 F60.00 
G01 X30.9880 Y18.1610 
G01 X3.6830 Y18.1610 
G01 X3.6830 Y0.6350 
G00 Z2.0000 
G01 Z-0.8250 F20.00 
G01 X30.9880 Y0.6350 F60.00 
G01 X30.9880 Y18.1610 
G01 X3.6830 Y18.1610 
G01 X3.6830 Y0.6350 
G00 Z2.0000 
G01 Z-1.1000 F20.00 
G01 X30.9880 Y0.6350 F60.00 
G01 X30.9880 Y18.1610 
G01 X3.6830 Y18.1610 
G01 X3.6830 Y0.6350 
G00 Z2.0000 
G01 Z-1.3750 F20.00 
G01 X30.9880 Y0.6350 F60.00 
G01 X30.9880 Y18.1610 
G01 X3.6830 Y18.1610 
G01 X3.6830 Y0.6350 
G00 Z2.0000 
G01 Z-1.6500 F20.00 
G01 X30.9880 Y0.6350 F60.00 
G01 X30.9880 Y18.1610 
G01 X3.6830 Y18.1610 
G01 X3.6830 Y0.6350 
G00 Z3.0000 
M05
M02
