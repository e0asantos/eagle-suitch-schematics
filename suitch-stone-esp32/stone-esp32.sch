<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.3.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="ESP-WROOM-32">
<packages>
<package name="MODULE_ESP-WROOM-32">
<wire x1="-9" y1="-9.755" x2="9" y2="-9.755" width="0.127" layer="51"/>
<wire x1="9" y1="-9.755" x2="9" y2="15.745" width="0.127" layer="51"/>
<wire x1="9" y1="15.745" x2="-9" y2="15.745" width="0.127" layer="51"/>
<wire x1="-9" y1="15.745" x2="-9" y2="-9.755" width="0.127" layer="51"/>
<wire x1="-9" y1="-9" x2="-9" y2="-9.75" width="0.127" layer="21"/>
<wire x1="-9" y1="-9.75" x2="-6.5" y2="-9.75" width="0.127" layer="21"/>
<wire x1="6.5" y1="-9.75" x2="9" y2="-9.75" width="0.127" layer="21"/>
<wire x1="9" y1="-9.75" x2="9" y2="-9" width="0.127" layer="21"/>
<wire x1="-9" y1="9.25" x2="-9" y2="15.75" width="0.127" layer="21"/>
<wire x1="-9" y1="15.75" x2="9" y2="15.75" width="0.127" layer="21"/>
<wire x1="9" y1="15.75" x2="9" y2="9.25" width="0.127" layer="21"/>
<rectangle x1="-8.02203125" y1="10.5289" x2="-7.5" y2="15" layer="21"/>
<rectangle x1="-8.009109375" y1="14.5165" x2="-5" y2="15" layer="21"/>
<rectangle x1="-5.52168125" y1="12.0474" x2="-5" y2="14.5" layer="21"/>
<rectangle x1="-5.508290625" y1="12.0181" x2="-2.5" y2="12.5" layer="21"/>
<rectangle x1="-3.01023125" y1="12.0408" x2="-2.5" y2="14.5" layer="21"/>
<rectangle x1="-3.01011875" y1="14.5488" x2="0" y2="15" layer="21"/>
<rectangle x1="-0.502034375" y1="12.0488" x2="0" y2="14.5" layer="21"/>
<rectangle x1="-0.5020875" y1="12.0501" x2="2.5" y2="12.5" layer="21"/>
<rectangle x1="2.0088" y1="12.0528" x2="2.5" y2="14.5" layer="21"/>
<rectangle x1="2.01156875" y1="14.5137" x2="7.5" y2="15" layer="21"/>
<rectangle x1="7.0207" y1="10.0296" x2="7.5" y2="15" layer="21"/>
<rectangle x1="4.513959375" y1="10.0311" x2="5" y2="15" layer="21"/>
<rectangle x1="7.01886875" y1="9.525609375" x2="7.5" y2="10" layer="21"/>
<rectangle x1="4.508090625" y1="9.51706875" x2="5" y2="10" layer="21"/>
<rectangle x1="-8.02033125" y1="10.0254" x2="-7.5" y2="10.5" layer="21"/>
<rectangle x1="2.007940625" y1="13.5536" x2="2.5" y2="15" layer="21"/>
<rectangle x1="-8.03838125" y1="10.5504" x2="-7.5" y2="15" layer="51"/>
<rectangle x1="-8.04086875" y1="14.5741" x2="-5" y2="15" layer="51"/>
<rectangle x1="-5.51816875" y1="12.0396" x2="-5" y2="14.5" layer="51"/>
<rectangle x1="-5.5155" y1="12.0339" x2="-2.5" y2="12.5" layer="51"/>
<rectangle x1="-3.01036875" y1="12.0415" x2="-2.5" y2="14.5" layer="51"/>
<rectangle x1="-3.0064" y1="14.531" x2="0" y2="15" layer="51"/>
<rectangle x1="-0.501671875" y1="12.0402" x2="0" y2="14.5" layer="51"/>
<rectangle x1="-0.500515625" y1="12.0123" x2="2.5" y2="12.5" layer="51"/>
<rectangle x1="2.007690625" y1="12.0461" x2="2.5" y2="14.5" layer="51"/>
<rectangle x1="2.015290625" y1="14.5407" x2="7.5" y2="15" layer="51"/>
<rectangle x1="7.0261" y1="10.0373" x2="7.5" y2="15" layer="51"/>
<rectangle x1="4.514609375" y1="10.0326" x2="5" y2="15" layer="51"/>
<rectangle x1="7.02153125" y1="9.52923125" x2="7.5" y2="10" layer="51"/>
<rectangle x1="4.522840625" y1="9.548240625" x2="5" y2="10" layer="51"/>
<rectangle x1="-8.02665" y1="10.0334" x2="-7.5" y2="10.5" layer="51"/>
<rectangle x1="2.007590625" y1="13.5513" x2="2.5" y2="15" layer="51"/>
<wire x1="-9.25" y1="16" x2="9.25" y2="16" width="0.05" layer="39"/>
<wire x1="9.25" y1="16" x2="9.25" y2="9" width="0.05" layer="39"/>
<wire x1="9.25" y1="9" x2="10.1" y2="9" width="0.05" layer="39"/>
<wire x1="10.1" y1="9" x2="10.1" y2="-9" width="0.05" layer="39"/>
<wire x1="10.1" y1="-9" x2="9.25" y2="-9" width="0.05" layer="39"/>
<wire x1="9.25" y1="-9" x2="9.25" y2="-10" width="0.05" layer="39"/>
<wire x1="9.25" y1="-10" x2="6.5" y2="-10" width="0.05" layer="39"/>
<wire x1="6.5" y1="-10" x2="6.5" y2="-10.855" width="0.05" layer="39"/>
<wire x1="6.5" y1="-10.855" x2="-6.5" y2="-10.855" width="0.05" layer="39"/>
<wire x1="-6.5" y1="-10.855" x2="-6.5" y2="-10" width="0.05" layer="39"/>
<wire x1="-6.5" y1="-10" x2="-9.25" y2="-10" width="0.05" layer="39"/>
<wire x1="-9.25" y1="-10" x2="-9.25" y2="-9" width="0.05" layer="39"/>
<wire x1="-9.25" y1="-9" x2="-10.1" y2="-9" width="0.05" layer="39"/>
<wire x1="-10.1" y1="-9" x2="-10.1" y2="9" width="0.05" layer="39"/>
<wire x1="-10.1" y1="9" x2="-9.25" y2="9" width="0.05" layer="39"/>
<wire x1="-9.25" y1="9" x2="-9.25" y2="16" width="0.05" layer="39"/>
<text x="-9.03255" y="16.3088" size="1.68246875" layer="25">&gt;NAME</text>
<text x="-9.037009375" y="-12.5515" size="1.683309375" layer="27">&gt;VALUE</text>
<rectangle x1="-9.02585" y1="9.02585" x2="9" y2="15.75" layer="41"/>
<rectangle x1="-9.01176875" y1="9.01176875" x2="9" y2="15.75" layer="42"/>
<rectangle x1="-9.024490625" y1="9.024490625" x2="9" y2="15.75" layer="43"/>
<circle x="-10.365" y="8.2754" radius="0.1" width="0.2" layer="21"/>
<circle x="-7.425" y="8.2754" radius="0.1" width="0.2" layer="51"/>
<smd name="38" x="9" y="8.255" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="37" x="9" y="6.985" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="36" x="9" y="5.715" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="35" x="9" y="4.445" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="34" x="9" y="3.175" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="33" x="9" y="1.905" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="32" x="9" y="0.635" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="31" x="9" y="-0.635" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="30" x="9" y="-1.905" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="29" x="9" y="-3.175" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="28" x="9" y="-4.445" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="27" x="9" y="-5.715" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="26" x="9" y="-6.985" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="25" x="9" y="-8.255" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R180"/>
<smd name="24" x="5.715" y="-9.755" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R90"/>
<smd name="23" x="4.445" y="-9.755" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R90"/>
<smd name="22" x="3.175" y="-9.755" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R90"/>
<smd name="21" x="1.905" y="-9.755" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R90"/>
<smd name="20" x="0.635" y="-9.755" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R90"/>
<smd name="19" x="-0.635" y="-9.755" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R90"/>
<smd name="18" x="-1.905" y="-9.755" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R90"/>
<smd name="17" x="-3.175" y="-9.755" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R90"/>
<smd name="16" x="-4.445" y="-9.755" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R90"/>
<smd name="15" x="-5.715" y="-9.755" dx="1.7" dy="0.9" layer="1" roundness="22" rot="R90"/>
<smd name="14" x="-9" y="-8.255" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="13" x="-9" y="-6.985" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="12" x="-9" y="-5.715" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="11" x="-9" y="-4.445" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="10" x="-9" y="-3.175" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="9" x="-9" y="-1.905" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="8" x="-9" y="-0.635" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="7" x="-9" y="0.635" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="6" x="-9" y="1.905" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="5" x="-9" y="3.175" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="4" x="-9" y="4.445" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="3" x="-9" y="5.715" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="2" x="-9" y="6.985" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="1" x="-9" y="8.255" dx="1.7" dy="0.9" layer="1" roundness="22"/>
<smd name="1.1" x="-0.7" y="0.845" dx="4" dy="4" layer="1" rot="R180"/>
</package>
</packages>
<symbols>
<symbol name="ESP-WROOM-32">
<wire x1="-15.24" y1="-27.94" x2="15.24" y2="-27.94" width="0.254" layer="94"/>
<wire x1="15.24" y1="-27.94" x2="15.24" y2="27.94" width="0.254" layer="94"/>
<wire x1="15.24" y1="27.94" x2="-15.24" y2="27.94" width="0.254" layer="94"/>
<wire x1="-15.24" y1="27.94" x2="-15.24" y2="-27.94" width="0.254" layer="94"/>
<text x="-15.2699" y="28.5037" size="2.54498125" layer="95">&gt;NAME</text>
<text x="-15.2772" y="-31.0638" size="2.5462" layer="96">&gt;VALUE</text>
<pin name="GND" x="20.32" y="-25.4" length="middle" direction="pwr" rot="R180"/>
<pin name="3V3" x="20.32" y="25.4" length="middle" direction="pwr" rot="R180"/>
<pin name="EN" x="-20.32" y="20.32" length="middle" direction="in"/>
<pin name="SENSOR_VP" x="-20.32" y="17.78" length="middle" direction="in"/>
<pin name="SENSOR_VN" x="-20.32" y="15.24" length="middle" direction="in"/>
<pin name="IO34" x="20.32" y="-17.78" length="middle" rot="R180"/>
<pin name="IO35" x="20.32" y="-20.32" length="middle" rot="R180"/>
<pin name="TXD0" x="-20.32" y="12.7" length="middle"/>
<pin name="RXD0" x="-20.32" y="10.16" length="middle"/>
<pin name="SWP/SD3" x="-20.32" y="-5.08" length="middle"/>
<pin name="SHD/SD2" x="-20.32" y="-2.54" length="middle"/>
<pin name="SCS/CMD" x="-20.32" y="7.62" length="middle"/>
<pin name="SCK/CLK" x="-20.32" y="5.08" length="middle" function="clk"/>
<pin name="SDO/SD0" x="-20.32" y="2.54" length="middle"/>
<pin name="SDI/SD1" x="-20.32" y="0" length="middle"/>
<pin name="IO0" x="-20.32" y="-10.16" length="middle"/>
<pin name="IO2" x="-20.32" y="-12.7" length="middle"/>
<pin name="IO4" x="-20.32" y="-15.24" length="middle"/>
<pin name="IO5" x="-20.32" y="-17.78" length="middle"/>
<pin name="IO12" x="-20.32" y="-20.32" length="middle"/>
<pin name="IO13" x="20.32" y="20.32" length="middle" rot="R180"/>
<pin name="IO14" x="20.32" y="17.78" length="middle" rot="R180"/>
<pin name="IO15" x="20.32" y="15.24" length="middle" rot="R180"/>
<pin name="IO16" x="20.32" y="12.7" length="middle" rot="R180"/>
<pin name="IO17" x="20.32" y="10.16" length="middle" rot="R180"/>
<pin name="IO18" x="20.32" y="7.62" length="middle" rot="R180"/>
<pin name="IO19" x="20.32" y="5.08" length="middle" rot="R180"/>
<pin name="IO21" x="20.32" y="2.54" length="middle" rot="R180"/>
<pin name="IO22" x="20.32" y="0" length="middle" rot="R180"/>
<pin name="IO23" x="20.32" y="-2.54" length="middle" rot="R180"/>
<pin name="IO25" x="20.32" y="-5.08" length="middle" rot="R180"/>
<pin name="IO26" x="20.32" y="-7.62" length="middle" rot="R180"/>
<pin name="IO27" x="20.32" y="-10.16" length="middle" rot="R180"/>
<pin name="IO32" x="20.32" y="-12.7" length="middle" rot="R180"/>
<pin name="IO33" x="20.32" y="-15.24" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ESP-WROOM-32" prefix="U">
<description>Module: combo; GPIO, I2C x2, I2S x2, SDIO, SPI x3, UART x3; 19.5dBm &lt;a href="https://snapeda.com/parts/ESP-WROOM-32/Olimex%20LTD/view-part/?ref=eda"&gt;Buy Part&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="ESP-WROOM-32" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MODULE_ESP-WROOM-32">
<connects>
<connect gate="G$1" pin="3V3" pad="2"/>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="1 1.1 15 38"/>
<connect gate="G$1" pin="IO0" pad="25"/>
<connect gate="G$1" pin="IO12" pad="14"/>
<connect gate="G$1" pin="IO13" pad="16"/>
<connect gate="G$1" pin="IO14" pad="13"/>
<connect gate="G$1" pin="IO15" pad="23"/>
<connect gate="G$1" pin="IO16" pad="27"/>
<connect gate="G$1" pin="IO17" pad="28"/>
<connect gate="G$1" pin="IO18" pad="30"/>
<connect gate="G$1" pin="IO19" pad="31"/>
<connect gate="G$1" pin="IO2" pad="24"/>
<connect gate="G$1" pin="IO21" pad="33"/>
<connect gate="G$1" pin="IO22" pad="36"/>
<connect gate="G$1" pin="IO23" pad="37"/>
<connect gate="G$1" pin="IO25" pad="10"/>
<connect gate="G$1" pin="IO26" pad="11"/>
<connect gate="G$1" pin="IO27" pad="12"/>
<connect gate="G$1" pin="IO32" pad="8"/>
<connect gate="G$1" pin="IO33" pad="9"/>
<connect gate="G$1" pin="IO34" pad="6"/>
<connect gate="G$1" pin="IO35" pad="7"/>
<connect gate="G$1" pin="IO4" pad="26"/>
<connect gate="G$1" pin="IO5" pad="29"/>
<connect gate="G$1" pin="RXD0" pad="34"/>
<connect gate="G$1" pin="SCK/CLK" pad="20"/>
<connect gate="G$1" pin="SCS/CMD" pad="19"/>
<connect gate="G$1" pin="SDI/SD1" pad="22"/>
<connect gate="G$1" pin="SDO/SD0" pad="21"/>
<connect gate="G$1" pin="SENSOR_VN" pad="5"/>
<connect gate="G$1" pin="SENSOR_VP" pad="4"/>
<connect gate="G$1" pin="SHD/SD2" pad="17"/>
<connect gate="G$1" pin="SWP/SD3" pad="18"/>
<connect gate="G$1" pin="TXD0" pad="35"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Bad"/>
<attribute name="DESCRIPTION" value=" WiFi 802.11b/g/n/d/e/i Transceiver Module 2.4GHz Surface Mount "/>
<attribute name="MF" value="Olimex LTD"/>
<attribute name="MP" value="ESP-WROOM-32"/>
<attribute name="PACKAGE" value="Module Olimex LTD"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ACS712">
<packages>
<package name="SO08">
<description>&lt;b&gt;8 Lead SOIC&lt;/b&gt;&lt;p&gt;
Data Sheet No. PD60212 Rev A&lt;br&gt;
Source: www.irf.com .. ir2520.pdf</description>
<wire x1="2.4" y1="1.9" x2="2.4" y2="-1.4" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-1.4" x2="2.4" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-1.9" x2="-2.4" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="-1.9" x2="-2.4" y2="-1.4" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="-1.4" x2="-2.4" y2="1.9" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="1.9" x2="2.4" y2="1.9" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-1.4" x2="-2.4" y2="-1.4" width="0.2032" layer="51"/>
<smd name="2" x="-0.635" y="-2.35" dx="0.72" dy="1.78" layer="1"/>
<smd name="7" x="-0.635" y="2.35" dx="0.72" dy="1.78" layer="1"/>
<smd name="1" x="-1.905" y="-2.35" dx="0.72" dy="1.78" layer="1"/>
<smd name="3" x="0.635" y="-2.35" dx="0.72" dy="1.78" layer="1"/>
<smd name="4" x="1.905" y="-2.35" dx="0.72" dy="1.78" layer="1"/>
<smd name="8" x="-1.905" y="2.35" dx="0.72" dy="1.78" layer="1"/>
<smd name="6" x="0.635" y="2.35" dx="0.72" dy="1.78" layer="1"/>
<smd name="5" x="1.905" y="2.35" dx="0.72" dy="1.78" layer="1"/>
<text x="-2.667" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.937" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.15" y1="-3.1" x2="-1.66" y2="-2" layer="51"/>
<rectangle x1="-0.88" y1="-3.1" x2="-0.39" y2="-2" layer="51"/>
<rectangle x1="0.39" y1="-3.1" x2="0.88" y2="-2" layer="51"/>
<rectangle x1="1.66" y1="-3.1" x2="2.15" y2="-2" layer="51"/>
<rectangle x1="1.66" y1="2" x2="2.15" y2="3.1" layer="51"/>
<rectangle x1="0.39" y1="2" x2="0.88" y2="3.1" layer="51"/>
<rectangle x1="-0.88" y1="2" x2="-0.39" y2="3.1" layer="51"/>
<rectangle x1="-2.15" y1="2" x2="-1.66" y2="3.1" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="ACS712_SYM">
<wire x1="-10.16" y1="7.62" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="5.08" x2="10.16" y2="2.54" width="0.254" layer="94"/>
<wire x1="10.16" y1="2.54" x2="10.16" y2="-2.54" width="0.254" layer="94"/>
<wire x1="10.16" y1="-2.54" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<pin name="I+_0" x="-15.24" y="5.08" visible="off" length="middle"/>
<pin name="I+_1" x="-15.24" y="2.54" visible="off" length="middle"/>
<pin name="I-_0" x="-15.24" y="-2.54" visible="off" length="middle"/>
<pin name="I-_1" x="-15.24" y="-5.08" visible="off" length="middle"/>
<pin name="GND" x="15.24" y="-5.08" visible="off" length="middle" direction="pwr" rot="R180"/>
<pin name="FILTER" x="15.24" y="-2.54" visible="off" length="middle" rot="R180"/>
<pin name="VOUT" x="15.24" y="2.54" visible="off" length="middle" direction="out" rot="R180"/>
<pin name="VCC" x="15.24" y="5.08" visible="off" length="middle" direction="pwr" rot="R180"/>
<wire x1="-15.24" y1="5.08" x2="-15.24" y2="2.54" width="0.254" layer="91"/>
<wire x1="-15.24" y1="-2.54" x2="-15.24" y2="-5.08" width="0.254" layer="91"/>
<text x="5.08" y="10.16" size="1.778" layer="95" font="vector" rot="R180">&gt;NAME</text>
<text x="-5.08" y="-10.16" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<wire x1="-9.525" y1="5.08" x2="-9.525" y2="3.81" width="1.016" layer="94"/>
<wire x1="-9.525" y1="3.81" x2="-9.525" y2="2.54" width="1.016" layer="94"/>
<wire x1="-9.525" y1="3.81" x2="-7.3025" y2="3.81" width="1.016" layer="94"/>
<wire x1="-7.3025" y1="3.81" x2="-7.3025" y2="-3.81" width="1.016" layer="94"/>
<wire x1="-7.3025" y1="-3.81" x2="-9.525" y2="-3.81" width="1.016" layer="94"/>
<wire x1="-9.525" y1="-3.81" x2="-9.525" y2="-2.54" width="1.016" layer="94"/>
<wire x1="-9.525" y1="-3.81" x2="-9.525" y2="-5.08" width="1.016" layer="94"/>
<wire x1="-5.715" y1="1.27" x2="-5.715" y2="0" width="0.127" layer="94"/>
<wire x1="-5.715" y1="0" x2="-5.715" y2="-1.27" width="0.127" layer="94"/>
<wire x1="-5.715" y1="-1.27" x2="-4.445" y2="-1.27" width="0.127" layer="94"/>
<wire x1="-4.445" y1="-1.27" x2="-3.175" y2="-1.27" width="0.127" layer="94"/>
<wire x1="-3.175" y1="-1.27" x2="-3.175" y2="0" width="0.127" layer="94"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="1.27" width="0.127" layer="94"/>
<wire x1="-3.175" y1="1.27" x2="-4.445" y2="1.27" width="0.127" layer="94"/>
<wire x1="-4.445" y1="1.27" x2="-5.715" y2="1.27" width="0.127" layer="94"/>
<wire x1="-5.08" y1="-0.635" x2="-4.445" y2="0" width="0.127" layer="94"/>
<wire x1="-4.445" y1="0" x2="-3.81" y2="0.635" width="0.127" layer="94"/>
<wire x1="-5.08" y1="0.635" x2="-4.445" y2="0" width="0.127" layer="94"/>
<wire x1="-4.445" y1="0" x2="-3.81" y2="-0.635" width="0.127" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.635" width="0.127" layer="94"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.127" layer="94"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="-1.27" width="0.127" layer="94"/>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.127" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.127" layer="94"/>
<wire x1="-1.27" y1="0.635" x2="-1.905" y2="0.635" width="0.127" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="-1.905" y2="1.905" width="0.127" layer="94"/>
<wire x1="-1.905" y1="1.905" x2="-4.445" y2="1.905" width="0.127" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-4.445" y2="1.27" width="0.127" layer="94"/>
<wire x1="-4.445" y1="-1.27" x2="-4.445" y2="-1.905" width="0.127" layer="94"/>
<wire x1="-4.445" y1="-1.905" x2="-1.905" y2="-1.905" width="0.127" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-1.905" y2="-0.635" width="0.127" layer="94"/>
<wire x1="-1.905" y1="-0.635" x2="-1.27" y2="-0.635" width="0.127" layer="94"/>
<wire x1="-5.715" y1="0" x2="-6.35" y2="0" width="0.127" layer="94"/>
<wire x1="-6.35" y1="0" x2="-6.35" y2="5.08" width="0.127" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="10.16" y2="5.08" width="0.127" layer="94"/>
<wire x1="-3.175" y1="0" x2="-2.54" y2="0" width="0.127" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="-5.08" width="0.127" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="10.16" y2="-5.08" width="0.127" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.127" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.127" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="3.81" y2="-2.54" width="0.127" layer="94"/>
<wire x1="6.0325" y1="-2.54" x2="8.255" y2="-2.54" width="0.127" layer="94"/>
<wire x1="8.255" y1="-2.54" x2="10.16" y2="-2.54" width="0.127" layer="94"/>
<wire x1="8.255" y1="-2.54" x2="8.255" y2="-1.27" width="0.127" layer="94"/>
<wire x1="8.255" y1="-1.27" x2="9.525" y2="-1.27" width="0.127" layer="94"/>
<wire x1="9.525" y1="-1.27" x2="8.255" y2="1.27" width="0.127" layer="94"/>
<wire x1="8.255" y1="1.27" x2="6.985" y2="-1.27" width="0.127" layer="94"/>
<wire x1="6.985" y1="-1.27" x2="8.255" y2="-1.27" width="0.127" layer="94"/>
<wire x1="8.255" y1="1.27" x2="8.255" y2="2.54" width="0.127" layer="94"/>
<wire x1="8.255" y1="2.54" x2="10.16" y2="2.54" width="0.127" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="4.1275" y2="-1.905" width="0.127" layer="94"/>
<wire x1="4.1275" y1="-1.905" x2="4.445" y2="-3.175" width="0.127" layer="94"/>
<wire x1="4.445" y1="-3.175" x2="4.7625" y2="-1.905" width="0.127" layer="94"/>
<wire x1="4.7625" y1="-1.905" x2="5.08" y2="-3.175" width="0.127" layer="94"/>
<wire x1="5.08" y1="-3.175" x2="5.3975" y2="-1.905" width="0.127" layer="94"/>
<wire x1="5.3975" y1="-1.905" x2="5.715" y2="-3.175" width="0.127" layer="94"/>
<wire x1="5.715" y1="-3.175" x2="6.0325" y2="-2.54" width="0.127" layer="94"/>
<wire x1="-0.889" y1="0.762" x2="-0.889" y2="0.635" width="0.127" layer="94"/>
<wire x1="-0.889" y1="0.635" x2="-0.889" y2="0.508" width="0.127" layer="94"/>
<wire x1="-1.016" y1="0.635" x2="-0.889" y2="0.635" width="0.127" layer="94"/>
<wire x1="-0.889" y1="0.635" x2="-0.762" y2="0.635" width="0.127" layer="94"/>
<wire x1="-1.016" y1="-0.635" x2="-0.762" y2="-0.635" width="0.127" layer="94"/>
<text x="10.4775" y="5.3975" size="1.016" layer="97" font="vector">VCC</text>
<text x="10.4775" y="2.8575" size="1.016" layer="97" font="vector">VOUT</text>
<text x="10.4775" y="-2.2225" size="1.016" layer="97" font="vector">FILTER</text>
<text x="10.4775" y="-4.7625" size="1.016" layer="97" font="vector">GND</text>
<text x="-12.065" y="3.175" size="1.016" layer="97" font="vector">I+</text>
<text x="-12.065" y="-4.445" size="1.016" layer="97" font="vector">I-</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="ACS712" prefix="IC">
<gates>
<gate name="A" symbol="ACS712_SYM" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SO08">
<connects>
<connect gate="A" pin="FILTER" pad="6"/>
<connect gate="A" pin="GND" pad="5"/>
<connect gate="A" pin="I+_0" pad="1"/>
<connect gate="A" pin="I+_1" pad="2"/>
<connect gate="A" pin="I-_0" pad="3"/>
<connect gate="A" pin="I-_1" pad="4"/>
<connect gate="A" pin="VCC" pad="8"/>
<connect gate="A" pin="VOUT" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ams1117-3.3v">
<description>&lt;1A LOW DROPOUT VOLTAGE REGULATOR, SOT-223&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="SOT229P700X180-4N">
<description>&lt;b&gt;3 Lead SOT-223 Plastic Package&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-3.35" y="2.29" dx="1.3" dy="0.95" layer="1"/>
<smd name="2" x="-3.35" y="0" dx="1.3" dy="0.95" layer="1"/>
<smd name="3" x="-3.35" y="-2.29" dx="1.3" dy="0.95" layer="1"/>
<smd name="4" x="3.35" y="0" dx="3.25" dy="1.3" layer="1" rot="R90"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-4.25" y1="3.605" x2="4.25" y2="3.605" width="0.05" layer="51"/>
<wire x1="4.25" y1="3.605" x2="4.25" y2="-3.605" width="0.05" layer="51"/>
<wire x1="4.25" y1="-3.605" x2="-4.25" y2="-3.605" width="0.05" layer="51"/>
<wire x1="-4.25" y1="-3.605" x2="-4.25" y2="3.605" width="0.05" layer="51"/>
<wire x1="-1.752" y1="3.252" x2="1.752" y2="3.252" width="0.1" layer="51"/>
<wire x1="1.752" y1="3.252" x2="1.752" y2="-3.252" width="0.1" layer="51"/>
<wire x1="1.752" y1="-3.252" x2="-1.752" y2="-3.252" width="0.1" layer="51"/>
<wire x1="-1.752" y1="-3.252" x2="-1.752" y2="3.252" width="0.1" layer="51"/>
<wire x1="-1.752" y1="0.962" x2="0.538" y2="3.252" width="0.1" layer="51"/>
<wire x1="-1.752" y1="3.252" x2="1.752" y2="3.252" width="0.2" layer="21"/>
<wire x1="1.752" y1="3.252" x2="1.752" y2="-3.252" width="0.2" layer="21"/>
<wire x1="1.752" y1="-3.252" x2="-1.752" y2="-3.252" width="0.2" layer="21"/>
<wire x1="-1.752" y1="-3.252" x2="-1.752" y2="3.252" width="0.2" layer="21"/>
<wire x1="-4" y1="3.115" x2="-2.7" y2="3.115" width="0.2" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="AMS1117-3.3V">
<wire x1="5.08" y1="2.54" x2="45.72" y2="2.54" width="0.254" layer="94"/>
<wire x1="45.72" y1="-7.62" x2="45.72" y2="2.54" width="0.254" layer="94"/>
<wire x1="45.72" y1="-7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<text x="46.99" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="46.99" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="GROUND/ADJUST" x="0" y="0" length="middle"/>
<pin name="VOUT" x="0" y="-2.54" length="middle"/>
<pin name="VIN" x="0" y="-5.08" length="middle"/>
<pin name="TAB_(OUTPUT)" x="50.8" y="-2.54" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AMS1117-3.3V" prefix="IC">
<description>&lt;b&gt;1A LOW DROPOUT VOLTAGE REGULATOR, SOT-223&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.advanced-monolithic.com/pdf/ds1117.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="AMS1117-3.3V" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT229P700X180-4N">
<connects>
<connect gate="G$1" pin="GROUND/ADJUST" pad="1"/>
<connect gate="G$1" pin="TAB_(OUTPUT)" pad="4"/>
<connect gate="G$1" pin="VIN" pad="3"/>
<connect gate="G$1" pin="VOUT" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="1A LOW DROPOUT VOLTAGE REGULATOR, SOT-223" constant="no"/>
<attribute name="HEIGHT" value="1.8mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Advanced Monolithic Systems" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="ams1117-3.3v" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE-STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="HLK-PM01">
<description>&lt;h5&gt;Hi-Link Power Supply&lt;/h5&gt;
&lt;br/&gt;

HLK-PM01 3W
&lt;br /&gt;
&lt;br/&gt;
Author: &lt;a href="https://github.com/AhmedSaid/EAGLELibraries"&gt;Ahmed Said&lt;/a&gt;</description>
<packages>
<package name="HLK-PM01">
<description>&lt;h5&gt;Hi-Link Power Supply&lt;/h5&gt;
&lt;br/&gt;

HLK-PM01 3W
&lt;br /&gt;
&lt;br/&gt;
Author: &lt;a href="https://github.com/AhmedSaid/EAGLELibraries"&gt;Ahmed Said&lt;/a&gt;</description>
<wire x1="-17" y1="10.1" x2="17" y2="10.1" width="0.127" layer="51"/>
<wire x1="17" y1="10.1" x2="17" y2="-10.1" width="0.127" layer="51"/>
<wire x1="17" y1="-10.1" x2="-17" y2="-10.1" width="0.127" layer="51"/>
<wire x1="-17" y1="-10.1" x2="-17" y2="10.1" width="0.127" layer="51"/>
<pad name="P$1" x="-14.7" y="2.5" drill="1.016" shape="square"/>
<pad name="P$2" x="-14.7" y="-2.5" drill="1.016" shape="square"/>
<pad name="P$3" x="14.7" y="7.7" drill="1.016" shape="square"/>
<pad name="P$4" x="14.7" y="-7.7" drill="1.016" shape="square"/>
<text x="-15.24" y="7.62" size="1.27" layer="21">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="PS">
<wire x1="-15.24" y1="10.16" x2="15.24" y2="10.16" width="0.254" layer="94"/>
<wire x1="15.24" y1="10.16" x2="15.24" y2="-10.16" width="0.254" layer="94"/>
<wire x1="15.24" y1="-10.16" x2="-15.24" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-10.16" x2="-15.24" y2="10.16" width="0.254" layer="94"/>
<pin name="AC1" x="-20.32" y="2.54" length="middle"/>
<pin name="AC2" x="-20.32" y="-2.54" length="middle"/>
<pin name="-VO" x="20.32" y="7.62" length="middle" rot="R180"/>
<pin name="+VO" x="20.32" y="-7.62" length="middle" rot="R180"/>
<text x="-12.7" y="7.62" size="1.27" layer="94">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="HLK-PM01" prefix="HLK">
<description>&lt;h5&gt;Hi-Link Power Supply&lt;/h5&gt;
&lt;br/&gt;

HLK-PM01 3W
&lt;br /&gt;
&lt;br/&gt;
Author: &lt;a href="https://github.com/AhmedSaid/EAGLELibraries"&gt;Ahmed Said&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="PS" x="-10.16" y="2.54"/>
</gates>
<devices>
<device name="3W" package="HLK-PM01">
<connects>
<connect gate="G$1" pin="+VO" pad="P$4"/>
<connect gate="G$1" pin="-VO" pad="P$3"/>
<connect gate="G$1" pin="AC1" pad="P$1"/>
<connect gate="G$1" pin="AC2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U1" library="ESP-WROOM-32" deviceset="ESP-WROOM-32" device=""/>
<part name="IC1" library="ACS712" deviceset="ACS712" device=""/>
<part name="IC2" library="ACS712" deviceset="ACS712" device=""/>
<part name="IC3" library="ams1117-3.3v" deviceset="AMS1117-3.3V" device=""/>
<part name="HLK1" library="HLK-PM01" deviceset="HLK-PM01" device="3W"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U1" gate="G$1" x="66.04" y="60.96" smashed="yes">
<attribute name="NAME" x="50.7701" y="89.4637" size="2.54498125" layer="95"/>
<attribute name="VALUE" x="50.7628" y="29.8962" size="2.5462" layer="96"/>
</instance>
<instance part="IC1" gate="A" x="383.54" y="157.48" smashed="yes">
<attribute name="NAME" x="388.62" y="167.64" size="1.778" layer="95" font="vector" rot="R180"/>
<attribute name="VALUE" x="378.46" y="147.32" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="IC2" gate="A" x="383.54" y="134.62" smashed="yes">
<attribute name="NAME" x="388.62" y="144.78" size="1.778" layer="95" font="vector" rot="R180"/>
<attribute name="VALUE" x="378.46" y="124.46" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="IC3" gate="G$1" x="48.26" y="134.62" smashed="yes">
<attribute name="NAME" x="95.25" y="142.24" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="95.25" y="139.7" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="HLK1" gate="G$1" x="71.12" y="177.8" smashed="yes">
<attribute name="NAME" x="58.42" y="185.42" size="1.27" layer="94"/>
</instance>
</instances>
<busses>
</busses>
<nets>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
