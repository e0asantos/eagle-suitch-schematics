(.../Documents/EAGLE/ulps/pcb-gcode.ulp)
(Copyright 2005 - 2012 by John Johnson)
(See readme.txt for licensing terms.)
(This file generated from the board:)
(.../EAGLE/projects/shapeoko/1shape.brd)
(Current profile is .../EAGLE/ulps/profiles/generic.pp  )
(This file generated 25/11/19 15:07)
(Settings from pcb-machine.h)
(spindle on time = 0.0000)
(tool change at 0.0000 0.0000 30.0000 )
(feed rate xy = F0.00  )
(feed rate z  = F0.00  )
(Z Axis Settings)
(  High     Up        Down     Drill)
(3.0000 	2.0000 	-0.0800 	-1.6700 )
(Settings from pcb-defaults.h)
(isolate min = 0.0254)
(isolate max = 1.0000)
(isolate step = 0.5000)
(Generated top outlines, top drill, top stencil, )
(Unit of measure: mm)
(Metric Mode)
G21
(Absolute Coordinates)
G90
S24000
G00 Z3.0000 
G00 X0.0000 Y0.0000 
M03
S10000
G04 P0.000000
G00 Z2.0000 
G00 Z3.0000 
M05
M02
