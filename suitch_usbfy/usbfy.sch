<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="16" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="17" fill="1" visible="no" active="no"/>
<layer number="4" name="Route4" color="18" fill="1" visible="no" active="no"/>
<layer number="5" name="Route5" color="19" fill="1" visible="no" active="no"/>
<layer number="6" name="Route6" color="25" fill="1" visible="no" active="no"/>
<layer number="7" name="Route7" color="26" fill="1" visible="no" active="no"/>
<layer number="8" name="Route8" color="27" fill="1" visible="no" active="no"/>
<layer number="9" name="Route9" color="28" fill="1" visible="no" active="no"/>
<layer number="10" name="Route10" color="29" fill="1" visible="no" active="no"/>
<layer number="11" name="Route11" color="30" fill="1" visible="no" active="no"/>
<layer number="12" name="Route12" color="20" fill="1" visible="no" active="no"/>
<layer number="13" name="Route13" color="21" fill="1" visible="no" active="no"/>
<layer number="14" name="Route14" color="22" fill="1" visible="no" active="no"/>
<layer number="15" name="Route15" color="23" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="58" name="bCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="no" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="117" name="mPads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="mUnrouted" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="mDimension" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="no" active="no"/>
<layer number="130" name="mbStop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="133" name="mtFinish" color="7" fill="1" visible="yes" active="yes"/>
<layer number="134" name="mbFinish" color="7" fill="1" visible="yes" active="yes"/>
<layer number="135" name="mtGlue" color="7" fill="1" visible="yes" active="yes"/>
<layer number="136" name="mbGlue" color="7" fill="1" visible="yes" active="yes"/>
<layer number="137" name="mtTest" color="7" fill="1" visible="yes" active="yes"/>
<layer number="138" name="mbTest" color="7" fill="1" visible="yes" active="yes"/>
<layer number="139" name="mtKeepout" color="7" fill="1" visible="yes" active="yes"/>
<layer number="140" name="mbKeepout" color="7" fill="1" visible="yes" active="yes"/>
<layer number="141" name="mtRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="142" name="mbRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="143" name="mvRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="145" name="mHoles" color="7" fill="1" visible="yes" active="yes"/>
<layer number="146" name="mMilling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="147" name="mMeasures" color="7" fill="1" visible="yes" active="yes"/>
<layer number="148" name="mDocument" color="7" fill="1" visible="yes" active="yes"/>
<layer number="149" name="mReference" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="no" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="no" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="no" active="yes"/>
<layer number="191" name="mNets" color="7" fill="1" visible="yes" active="yes"/>
<layer number="192" name="mBusses" color="7" fill="1" visible="yes" active="yes"/>
<layer number="193" name="mPins" color="7" fill="1" visible="yes" active="yes"/>
<layer number="194" name="mSymbols" color="7" fill="1" visible="yes" active="yes"/>
<layer number="195" name="mNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="196" name="mValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="231" name="Eagle3D_PG1" color="14" fill="1" visible="no" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="14" fill="2" visible="no" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="14" fill="4" visible="no" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="esp32_s3">
<packages>
<package name="ESP32-S3-WROOM-1-N8_EXP">
<smd name="1" x="-8.5809" y="5.2537" dx="0.889" dy="0.8382" layer="1" rot="R270"/>
<smd name="2" x="-8.5809" y="3.9837" dx="0.889" dy="0.8382" layer="1" rot="R270"/>
<smd name="3" x="-8.5809" y="2.7137" dx="0.889" dy="0.8382" layer="1" rot="R270"/>
<smd name="4" x="-8.5809" y="1.4437" dx="0.889" dy="0.8382" layer="1" rot="R270"/>
<smd name="5" x="-8.5809" y="0.1737" dx="0.889" dy="0.8382" layer="1" rot="R270"/>
<smd name="6" x="-8.5809" y="-1.0963" dx="0.889" dy="0.8382" layer="1" rot="R270"/>
<smd name="7" x="-8.5809" y="-2.3663" dx="0.889" dy="0.8382" layer="1" rot="R270"/>
<smd name="8" x="-8.5809" y="-3.6363" dx="0.889" dy="0.8382" layer="1" rot="R270"/>
<smd name="9" x="-8.5809" y="-4.9063" dx="0.889" dy="0.8382" layer="1" rot="R270"/>
<smd name="10" x="-8.5809" y="-6.1763" dx="0.889" dy="0.8382" layer="1" rot="R270"/>
<smd name="11" x="-8.5809" y="-7.4463" dx="0.889" dy="0.8382" layer="1" rot="R270"/>
<smd name="12" x="-8.5809" y="-8.7163" dx="0.889" dy="0.8382" layer="1" rot="R270"/>
<smd name="13" x="-8.5809" y="-9.9863" dx="0.889" dy="0.8382" layer="1" rot="R270"/>
<smd name="14" x="-8.5809" y="-11.2563" dx="0.889" dy="0.8382" layer="1" rot="R270"/>
<smd name="15" x="-6.985" y="-12.3309" dx="0.889" dy="0.8382" layer="1" rot="R180"/>
<smd name="16" x="-5.715" y="-12.3309" dx="0.889" dy="0.8382" layer="1" rot="R180"/>
<smd name="17" x="-4.445" y="-12.3309" dx="0.889" dy="0.8382" layer="1" rot="R180"/>
<smd name="18" x="-3.175" y="-12.3309" dx="0.889" dy="0.8382" layer="1" rot="R180"/>
<smd name="19" x="-1.905" y="-12.3309" dx="0.889" dy="0.8382" layer="1" rot="R180"/>
<smd name="20" x="-0.635" y="-12.3309" dx="0.889" dy="0.8382" layer="1" rot="R180"/>
<smd name="21" x="0.635" y="-12.3309" dx="0.889" dy="0.8382" layer="1" rot="R180"/>
<smd name="22" x="1.905" y="-12.3309" dx="0.889" dy="0.8382" layer="1" rot="R180"/>
<smd name="23" x="3.175" y="-12.3309" dx="0.889" dy="0.8382" layer="1" rot="R180"/>
<smd name="24" x="4.445" y="-12.3309" dx="0.889" dy="0.8382" layer="1" rot="R180"/>
<smd name="25" x="5.715" y="-12.3309" dx="0.889" dy="0.8382" layer="1" rot="R180"/>
<smd name="26" x="6.985" y="-12.3309" dx="0.889" dy="0.8382" layer="1" rot="R180"/>
<smd name="27" x="8.5809" y="-11.2563" dx="0.889" dy="0.8382" layer="1" rot="R270"/>
<smd name="28" x="8.5809" y="-9.9863" dx="0.889" dy="0.8382" layer="1" rot="R270"/>
<smd name="29" x="8.5809" y="-8.7163" dx="0.889" dy="0.8382" layer="1" rot="R270"/>
<smd name="30" x="8.5809" y="-7.4463" dx="0.889" dy="0.8382" layer="1" rot="R270"/>
<smd name="31" x="8.5809" y="-6.1763" dx="0.889" dy="0.8382" layer="1" rot="R270"/>
<smd name="32" x="8.5809" y="-4.9063" dx="0.889" dy="0.8382" layer="1" rot="R270"/>
<smd name="33" x="8.5809" y="-3.6363" dx="0.889" dy="0.8382" layer="1" rot="R270"/>
<smd name="34" x="8.5809" y="-2.3663" dx="0.889" dy="0.8382" layer="1" rot="R270"/>
<smd name="35" x="8.5809" y="-1.0963" dx="0.889" dy="0.8382" layer="1" rot="R270"/>
<smd name="36" x="8.5809" y="0.1737" dx="0.889" dy="0.8382" layer="1" rot="R270"/>
<smd name="37" x="8.5809" y="1.4437" dx="0.889" dy="0.8382" layer="1" rot="R270"/>
<smd name="38" x="8.5809" y="2.7137" dx="0.889" dy="0.8382" layer="1" rot="R270"/>
<smd name="39" x="8.5809" y="3.9837" dx="0.889" dy="0.8382" layer="1" rot="R270"/>
<smd name="40" x="8.5809" y="5.2537" dx="0.889" dy="0.8382" layer="1" rot="R270"/>
<smd name="41" x="-1.4957" y="-2.4608" dx="3.7084" dy="3.7084" layer="1" cream="no"/>
<wire x1="9.144" y1="12.8778" x2="-9.144" y2="12.8778" width="0.1524" layer="21"/>
<wire x1="9.144" y1="-12.8778" x2="9.144" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-9.144" y1="12.8778" x2="-9.144" y2="9.017" width="0.1524" layer="21"/>
<wire x1="-9.144" y1="-9.017" x2="-9.144" y2="-12.8778" width="0.1524" layer="21"/>
<wire x1="-9.144" y1="-12.8778" x2="-7.747" y2="-12.8778" width="0.1524" layer="21"/>
<wire x1="7.747" y1="-12.8778" x2="9.144" y2="-12.8778" width="0.1524" layer="21"/>
<wire x1="9.144" y1="9.017" x2="9.144" y2="12.8778" width="0.1524" layer="21"/>
<polygon width="0.0254" layer="21">
<vertex x="-9.508" y="-2.9845"/>
<vertex x="-9.508" y="-3.3655"/>
<vertex x="-9.254" y="-3.3655"/>
<vertex x="-9.254" y="-2.9845"/>
</polygon>
<polygon width="0.0254" layer="21">
<vertex x="-0.8255" y="-13.004"/>
<vertex x="-0.8255" y="-13.258"/>
<vertex x="-0.4445" y="-13.258"/>
<vertex x="-0.4445" y="-13.004"/>
</polygon>
<polygon width="0.0254" layer="21">
<vertex x="9.508" y="-4.2545"/>
<vertex x="9.508" y="-4.6355"/>
<vertex x="9.254" y="-4.6355"/>
<vertex x="9.254" y="-4.2545"/>
</polygon>
<polygon width="0.0254" layer="21">
<vertex x="9.508" y="8.4455"/>
<vertex x="9.508" y="8.0645"/>
<vertex x="9.254" y="8.0645"/>
<vertex x="9.254" y="8.4455"/>
</polygon>
<polygon width="0.0254" layer="21">
<vertex x="-4.6355" y="13.004"/>
<vertex x="-4.6355" y="13.258"/>
<vertex x="-4.2545" y="13.258"/>
<vertex x="-4.2545" y="13.004"/>
</polygon>
<text x="-10.2108" y="8.255" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<text x="-1.7272" y="-0.635" size="1.27" layer="21" ratio="6" rot="SR0">&gt;Value</text>
<wire x1="-8.9916" y1="-12.7508" x2="8.9916" y2="-12.7508" width="0.1524" layer="51"/>
<wire x1="8.9916" y1="-12.7508" x2="8.9916" y2="6.5532" width="0.1524" layer="51"/>
<wire x1="8.9916" y1="7.4168" x2="8.9916" y2="12.7508" width="0.1524" layer="51"/>
<wire x1="8.9916" y1="12.7508" x2="-7.7216" y2="12.7508" width="0.1524" layer="51"/>
<wire x1="-7.7216" y1="12.7508" x2="-8.9916" y2="12.7508" width="0.1524" layer="51"/>
<wire x1="-8.9916" y1="12.7508" x2="-8.9916" y2="11.4808" width="0.1524" layer="51"/>
<wire x1="-8.9916" y1="11.4808" x2="-8.9916" y2="-12.7508" width="0.1524" layer="51"/>
<wire x1="-8.9916" y1="11.4808" x2="-7.7216" y2="12.7508" width="0.1524" layer="51"/>
<wire x1="8.9916" y1="6.5532" x2="8.9916" y2="7.4168" width="0.1524" layer="51"/>
<text x="-8.7376" y="8.255" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<polygon width="0.0254" layer="31">
<vertex x="-1.7542" y="1.7542"/>
<vertex x="-1.7542" y="0.1"/>
<vertex x="-0.1" y="0.1"/>
<vertex x="-0.1" y="1.7542"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="-1.7542" y="-0.1"/>
<vertex x="-1.7542" y="-1.7542"/>
<vertex x="-0.1" y="-1.7542"/>
<vertex x="-0.1" y="-0.1"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="0.1" y="1.7542"/>
<vertex x="0.1" y="0.1"/>
<vertex x="1.7542" y="0.1"/>
<vertex x="1.7542" y="1.7542"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="0.1" y="-0.1"/>
<vertex x="0.1" y="-1.7542"/>
<vertex x="1.7542" y="-1.7542"/>
<vertex x="1.7542" y="-0.1"/>
</polygon>
<wire x1="8.5852" y1="8.255" x2="8.9916" y2="8.255" width="0.1524" layer="47"/>
<wire x1="8.9916" y1="8.255" x2="11.5316" y2="8.255" width="0.1524" layer="47"/>
<wire x1="11.5316" y1="8.255" x2="11.9126" y2="8.255" width="0.1524" layer="47"/>
<wire x1="8.5852" y1="6.985" x2="11.5316" y2="6.985" width="0.1524" layer="47"/>
<wire x1="11.5316" y1="6.985" x2="11.9126" y2="6.985" width="0.1524" layer="47"/>
<wire x1="11.5316" y1="8.255" x2="11.5316" y2="9.525" width="0.1524" layer="47"/>
<wire x1="11.5316" y1="6.985" x2="11.5316" y2="5.715" width="0.1524" layer="47"/>
<wire x1="11.5316" y1="8.255" x2="11.4046" y2="8.509" width="0.1524" layer="47"/>
<wire x1="11.5316" y1="8.255" x2="11.6586" y2="8.509" width="0.1524" layer="47"/>
<wire x1="11.4046" y1="8.509" x2="11.6586" y2="8.509" width="0.1524" layer="47"/>
<wire x1="11.5316" y1="6.985" x2="11.4046" y2="6.731" width="0.1524" layer="47"/>
<wire x1="11.5316" y1="6.985" x2="11.6586" y2="6.731" width="0.1524" layer="47"/>
<wire x1="11.4046" y1="6.731" x2="11.6586" y2="6.731" width="0.1524" layer="47"/>
<wire x1="8.1534" y1="8.255" x2="8.1534" y2="15.2908" width="0.1524" layer="47"/>
<wire x1="8.1534" y1="15.2908" x2="8.1534" y2="15.6718" width="0.1524" layer="47"/>
<wire x1="8.9916" y1="8.255" x2="8.9916" y2="12.7508" width="0.1524" layer="47"/>
<wire x1="8.9916" y1="12.7508" x2="8.9916" y2="15.2908" width="0.1524" layer="47"/>
<wire x1="8.1534" y1="15.2908" x2="6.8834" y2="15.2908" width="0.1524" layer="47"/>
<wire x1="8.9916" y1="15.2908" x2="10.2616" y2="15.2908" width="0.1524" layer="47"/>
<wire x1="8.1534" y1="15.2908" x2="7.8994" y2="15.4178" width="0.1524" layer="47"/>
<wire x1="8.1534" y1="15.2908" x2="7.8994" y2="15.1638" width="0.1524" layer="47"/>
<wire x1="7.8994" y1="15.4178" x2="7.8994" y2="15.1638" width="0.1524" layer="47"/>
<wire x1="8.9916" y1="15.2908" x2="9.2456" y2="15.4178" width="0.1524" layer="47"/>
<wire x1="8.9916" y1="15.2908" x2="9.2456" y2="15.1638" width="0.1524" layer="47"/>
<wire x1="9.2456" y1="15.4178" x2="9.2456" y2="15.1638" width="0.1524" layer="47"/>
<wire x1="-8.9916" y1="12.7508" x2="-8.9916" y2="17.1958" width="0.1524" layer="47"/>
<wire x1="-8.9916" y1="17.1958" x2="-8.9916" y2="17.5768" width="0.1524" layer="47"/>
<wire x1="8.9916" y1="15.2908" x2="8.9916" y2="17.1958" width="0.1524" layer="47"/>
<wire x1="8.9916" y1="17.1958" x2="8.9916" y2="17.5768" width="0.1524" layer="47"/>
<wire x1="-8.9916" y1="17.1958" x2="8.9916" y2="17.1958" width="0.1524" layer="47"/>
<wire x1="-8.9916" y1="17.1958" x2="-8.7376" y2="17.3228" width="0.1524" layer="47"/>
<wire x1="-8.9916" y1="17.1958" x2="-8.7376" y2="17.0688" width="0.1524" layer="47"/>
<wire x1="-8.7376" y1="17.3228" x2="-8.7376" y2="17.0688" width="0.1524" layer="47"/>
<wire x1="8.9916" y1="17.1958" x2="8.7376" y2="17.3228" width="0.1524" layer="47"/>
<wire x1="8.9916" y1="17.1958" x2="8.7376" y2="17.0688" width="0.1524" layer="47"/>
<wire x1="8.7376" y1="17.3228" x2="8.7376" y2="17.0688" width="0.1524" layer="47"/>
<wire x1="8.9916" y1="12.7508" x2="13.4366" y2="12.7508" width="0.1524" layer="47"/>
<wire x1="13.4366" y1="12.7508" x2="13.8176" y2="12.7508" width="0.1524" layer="47"/>
<wire x1="13.4366" y1="-12.7508" x2="13.8176" y2="-12.7508" width="0.1524" layer="47"/>
<wire x1="13.4366" y1="12.7508" x2="13.4366" y2="-12.7508" width="0.1524" layer="47"/>
<wire x1="13.4366" y1="12.7508" x2="13.3096" y2="12.4968" width="0.1524" layer="47"/>
<wire x1="13.4366" y1="12.7508" x2="13.5636" y2="12.4968" width="0.1524" layer="47"/>
<wire x1="13.3096" y1="12.4968" x2="13.5636" y2="12.4968" width="0.1524" layer="47"/>
<wire x1="13.4366" y1="-12.7508" x2="13.3096" y2="-12.4968" width="0.1524" layer="47"/>
<wire x1="13.4366" y1="-12.7508" x2="13.5636" y2="-12.4968" width="0.1524" layer="47"/>
<wire x1="13.3096" y1="-12.4968" x2="13.5636" y2="-12.4968" width="0.1524" layer="47"/>
<wire x1="8.9916" y1="12.7508" x2="-8.9916" y2="12.7508" width="0.1524" layer="47"/>
<wire x1="-8.9916" y1="12.7508" x2="-12.1666" y2="12.7508" width="0.1524" layer="47"/>
<wire x1="-12.1666" y1="12.7508" x2="-12.5476" y2="12.7508" width="0.1524" layer="47"/>
<wire x1="13.4366" y1="-12.7508" x2="-12.1666" y2="-12.7508" width="0.1524" layer="47"/>
<wire x1="-12.1666" y1="-12.7508" x2="-12.5476" y2="-12.7508" width="0.1524" layer="47"/>
<wire x1="-12.1666" y1="12.7508" x2="-12.1666" y2="-12.7508" width="0.1524" layer="47"/>
<wire x1="-12.1666" y1="12.7508" x2="-12.2936" y2="12.4968" width="0.1524" layer="47"/>
<wire x1="-12.1666" y1="12.7508" x2="-12.0396" y2="12.4968" width="0.1524" layer="47"/>
<wire x1="-12.2936" y1="12.4968" x2="-12.0396" y2="12.4968" width="0.1524" layer="47"/>
<wire x1="-12.1666" y1="-12.7508" x2="-12.2936" y2="-12.4968" width="0.1524" layer="47"/>
<wire x1="-12.1666" y1="-12.7508" x2="-12.0396" y2="-12.4968" width="0.1524" layer="47"/>
<wire x1="-12.2936" y1="-12.4968" x2="-12.0396" y2="-12.4968" width="0.1524" layer="47"/>
<wire x1="-8.9916" y1="12.7508" x2="-8.9916" y2="-15.9258" width="0.1524" layer="47"/>
<wire x1="-8.9916" y1="-15.9258" x2="-8.9916" y2="-16.3068" width="0.1524" layer="47"/>
<wire x1="8.9916" y1="8.255" x2="8.9916" y2="-15.9258" width="0.1524" layer="47"/>
<wire x1="8.9916" y1="-15.9258" x2="8.9916" y2="-16.3068" width="0.1524" layer="47"/>
<wire x1="-8.9916" y1="-15.9258" x2="8.9916" y2="-15.9258" width="0.1524" layer="47"/>
<wire x1="-8.9916" y1="-15.9258" x2="-8.7376" y2="-15.7988" width="0.1524" layer="47"/>
<wire x1="-8.9916" y1="-15.9258" x2="-8.7376" y2="-16.0528" width="0.1524" layer="47"/>
<wire x1="-8.7376" y1="-15.7988" x2="-8.7376" y2="-16.0528" width="0.1524" layer="47"/>
<wire x1="8.9916" y1="-15.9258" x2="8.7376" y2="-15.7988" width="0.1524" layer="47"/>
<wire x1="8.9916" y1="-15.9258" x2="8.7376" y2="-16.0528" width="0.1524" layer="47"/>
<wire x1="8.7376" y1="-15.7988" x2="8.7376" y2="-16.0528" width="0.1524" layer="47"/>
<text x="-15.2146" y="-20.6248" size="1.27" layer="47" ratio="6" rot="SR0">Default Padstyle: RX35Y33D0T</text>
<text x="-17.2974" y="-22.1488" size="1.27" layer="47" ratio="6" rot="SR0">Heat Tab Padstyle: RX146Y146D0T</text>
<text x="-14.8082" y="-25.1968" size="1.27" layer="47" ratio="6" rot="SR0">Alt 1 Padstyle: OX60Y90D30P</text>
<text x="-14.8082" y="-26.7208" size="1.27" layer="47" ratio="6" rot="SR0">Alt 2 Padstyle: OX90Y60D30P</text>
<text x="12.0396" y="7.3152" size="0.635" layer="47" ratio="4" rot="SR0">0.05in/1.27mm</text>
<text x="4.5466" y="15.7988" size="0.635" layer="47" ratio="4" rot="SR0">0.033in/0.838mm</text>
<text x="-3.2766" y="17.7038" size="0.635" layer="47" ratio="4" rot="SR0">0.709in/18mm</text>
<text x="13.9446" y="-0.3048" size="0.635" layer="47" ratio="4" rot="SR0">1.004in/25.5mm</text>
<text x="-21.336" y="-0.3048" size="0.635" layer="47" ratio="4" rot="SR0">1.004in/25.502mm</text>
<text x="-4.318" y="-17.0688" size="0.635" layer="47" ratio="4" rot="SR0">0.709in/18.009mm</text>
<text x="-3.2766" y="-0.635" size="1.27" layer="27" ratio="6" rot="SR0">&gt;Name</text>
</package>
</packages>
<symbols>
<symbol name="ESP32-S3-WROOM-1-N8">
<pin name="GND_2" x="2.54" y="0" length="middle" direction="pas"/>
<pin name="3V3" x="2.54" y="-2.54" length="middle" direction="pwr"/>
<pin name="EN" x="2.54" y="-5.08" length="middle" direction="in"/>
<pin name="IO4" x="2.54" y="-7.62" length="middle"/>
<pin name="IO5" x="2.54" y="-10.16" length="middle"/>
<pin name="IO6" x="2.54" y="-12.7" length="middle"/>
<pin name="IO7" x="2.54" y="-15.24" length="middle"/>
<pin name="IO15" x="2.54" y="-17.78" length="middle"/>
<pin name="IO16" x="2.54" y="-20.32" length="middle"/>
<pin name="IO17" x="2.54" y="-22.86" length="middle"/>
<pin name="IO18" x="2.54" y="-25.4" length="middle"/>
<pin name="IO8" x="2.54" y="-27.94" length="middle"/>
<pin name="IO19" x="2.54" y="-30.48" length="middle"/>
<pin name="IO20" x="2.54" y="-33.02" length="middle"/>
<pin name="IO3" x="2.54" y="-35.56" length="middle"/>
<pin name="IO46" x="2.54" y="-38.1" length="middle"/>
<pin name="IO9" x="2.54" y="-40.64" length="middle"/>
<pin name="IO10" x="2.54" y="-43.18" length="middle"/>
<pin name="IO11" x="2.54" y="-45.72" length="middle"/>
<pin name="IO12" x="2.54" y="-48.26" length="middle"/>
<pin name="IO13" x="38.1" y="-50.8" length="middle" rot="R180"/>
<pin name="IO14" x="38.1" y="-48.26" length="middle" rot="R180"/>
<pin name="IO21" x="38.1" y="-45.72" length="middle" rot="R180"/>
<pin name="IO47" x="38.1" y="-43.18" length="middle" rot="R180"/>
<pin name="IO48" x="38.1" y="-40.64" length="middle" rot="R180"/>
<pin name="IO45" x="38.1" y="-38.1" length="middle" rot="R180"/>
<pin name="IO0" x="38.1" y="-35.56" length="middle" rot="R180"/>
<pin name="IO35" x="38.1" y="-33.02" length="middle" rot="R180"/>
<pin name="IO36" x="38.1" y="-30.48" length="middle" rot="R180"/>
<pin name="IO37" x="38.1" y="-27.94" length="middle" rot="R180"/>
<pin name="IO38" x="38.1" y="-25.4" length="middle" rot="R180"/>
<pin name="IO39" x="38.1" y="-22.86" length="middle" rot="R180"/>
<pin name="IO40" x="38.1" y="-20.32" length="middle" rot="R180"/>
<pin name="IO41" x="38.1" y="-17.78" length="middle" rot="R180"/>
<pin name="IO42" x="38.1" y="-15.24" length="middle" rot="R180"/>
<pin name="RXD0" x="38.1" y="-12.7" length="middle" rot="R180"/>
<pin name="TXD0" x="38.1" y="-10.16" length="middle" rot="R180"/>
<pin name="IO2" x="38.1" y="-7.62" length="middle" rot="R180"/>
<pin name="IO1" x="38.1" y="-5.08" length="middle" rot="R180"/>
<pin name="GND_3" x="38.1" y="-2.54" length="middle" direction="pas" rot="R180"/>
<pin name="GND" x="38.1" y="0" length="middle" direction="pas" rot="R180"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-55.88" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-55.88" x2="33.02" y2="-55.88" width="0.1524" layer="94"/>
<wire x1="33.02" y1="-55.88" x2="33.02" y2="5.08" width="0.1524" layer="94"/>
<wire x1="33.02" y1="5.08" x2="7.62" y2="5.08" width="0.1524" layer="94"/>
<text x="15.5956" y="9.1186" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="14.9606" y="6.5786" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="ESP32-S3-WROOM-1-N8" prefix="U">
<gates>
<gate name="A" symbol="ESP32-S3-WROOM-1-N8" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ESP32-S3-WROOM-1-N8_EXP">
<connects>
<connect gate="A" pin="3V3" pad="2"/>
<connect gate="A" pin="EN" pad="3"/>
<connect gate="A" pin="GND" pad="41"/>
<connect gate="A" pin="GND_2" pad="1"/>
<connect gate="A" pin="GND_3" pad="40"/>
<connect gate="A" pin="IO0" pad="27"/>
<connect gate="A" pin="IO1" pad="39"/>
<connect gate="A" pin="IO10" pad="18"/>
<connect gate="A" pin="IO11" pad="19"/>
<connect gate="A" pin="IO12" pad="20"/>
<connect gate="A" pin="IO13" pad="21"/>
<connect gate="A" pin="IO14" pad="22"/>
<connect gate="A" pin="IO15" pad="8"/>
<connect gate="A" pin="IO16" pad="9"/>
<connect gate="A" pin="IO17" pad="10"/>
<connect gate="A" pin="IO18" pad="11"/>
<connect gate="A" pin="IO19" pad="13"/>
<connect gate="A" pin="IO2" pad="38"/>
<connect gate="A" pin="IO20" pad="14"/>
<connect gate="A" pin="IO21" pad="23"/>
<connect gate="A" pin="IO3" pad="15"/>
<connect gate="A" pin="IO35" pad="28"/>
<connect gate="A" pin="IO36" pad="29"/>
<connect gate="A" pin="IO37" pad="30"/>
<connect gate="A" pin="IO38" pad="31"/>
<connect gate="A" pin="IO39" pad="32"/>
<connect gate="A" pin="IO4" pad="4"/>
<connect gate="A" pin="IO40" pad="33"/>
<connect gate="A" pin="IO41" pad="34"/>
<connect gate="A" pin="IO42" pad="35"/>
<connect gate="A" pin="IO45" pad="26"/>
<connect gate="A" pin="IO46" pad="16"/>
<connect gate="A" pin="IO47" pad="24"/>
<connect gate="A" pin="IO48" pad="25"/>
<connect gate="A" pin="IO5" pad="5"/>
<connect gate="A" pin="IO6" pad="6"/>
<connect gate="A" pin="IO7" pad="7"/>
<connect gate="A" pin="IO8" pad="12"/>
<connect gate="A" pin="IO9" pad="17"/>
<connect gate="A" pin="RXD0" pad="36"/>
<connect gate="A" pin="TXD0" pad="37"/>
</connects>
<technologies>
<technology name="">
<attribute name="COPYRIGHT" value="Copyright (C) 2022 Ultra Librarian. All rights reserved." constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_1" value="1965-ESP32-S3-WROOM-1-N8TR-ND" constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_2" value="1965-ESP32-S3-WROOM-1-N8CT-ND" constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_3" value="1965-ESP32-S3-WROOM-1-N8DKR-ND" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="ESP32-S3-WROOM-1-N8" constant="no"/>
<attribute name="MFR_NAME" value="Espressif Systems" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="usb-c connector">
<packages>
<package name="AMPHENOL_10133475-10001LF">
<wire x1="-4.55" y1="0.55" x2="4.55" y2="0.55" width="0.127" layer="51"/>
<text x="5.027" y="-0.673" size="0.635" layer="51">PCB Edge</text>
<wire x1="-4.125" y1="-11.4" x2="4.125" y2="-11.4" width="0.127" layer="51"/>
<wire x1="4.125" y1="-11.4" x2="4.125" y2="-3.1" width="0.127" layer="51"/>
<wire x1="-4.125" y1="-3.1" x2="-4.125" y2="-11.4" width="0.127" layer="51"/>
<wire x1="-4.55" y1="0.55" x2="-4.55" y2="-0.95" width="0.127" layer="51"/>
<wire x1="-4.55" y1="-0.95" x2="-4.55" y2="-3.1" width="0.127" layer="51"/>
<wire x1="-4.55" y1="-3.1" x2="-4.125" y2="-3.1" width="0.127" layer="51"/>
<wire x1="4.125" y1="-3.1" x2="4.55" y2="-3.1" width="0.127" layer="51"/>
<wire x1="4.55" y1="-3.1" x2="4.55" y2="0.55" width="0.127" layer="51"/>
<text x="-4.572" y="3.745" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.572" y="2.024" size="1.27" layer="27">&gt;VALUE</text>
<circle x="2.75" y="1.427" radius="0.1" width="0.2" layer="51"/>
<circle x="2.75" y="1.427" radius="0.1" width="0.2" layer="21"/>
<wire x1="-4.8" y1="1.05" x2="4.8" y2="1.05" width="0.05" layer="39"/>
<wire x1="4.8" y1="1.05" x2="4.8" y2="-11.65" width="0.05" layer="39"/>
<wire x1="4.8" y1="-11.65" x2="-4.8" y2="-11.65" width="0.05" layer="39"/>
<wire x1="-4.8" y1="-11.65" x2="-4.8" y2="1.05" width="0.05" layer="39"/>
<wire x1="-4.8" y1="-11.65" x2="-4.8" y2="1.05" width="0.05" layer="40"/>
<wire x1="-4.8" y1="1.05" x2="4.8" y2="1.05" width="0.05" layer="40"/>
<wire x1="4.8" y1="1.05" x2="4.8" y2="-11.65" width="0.05" layer="40"/>
<wire x1="4.8" y1="-11.65" x2="-4.8" y2="-11.65" width="0.05" layer="40"/>
<wire x1="-3.75" y1="-0.60116875" x2="-3.54795" y2="-0.95" width="0" layer="46"/>
<wire x1="-4.4" y1="0.65" x2="-4.4" y2="-1.93836875" width="0" layer="46"/>
<wire x1="-3.75" y1="-0.60116875" x2="-3.75" y2="0.47246875" width="0" layer="46"/>
<wire x1="-3.54795" y1="-0.95" x2="3.55126875" y2="-0.95" width="0" layer="46"/>
<wire x1="4.4" y1="0.65" x2="3.929809375" y2="0.65" width="0" layer="46"/>
<wire x1="-4.4" y1="0.65" x2="-3.93078125" y2="0.65" width="0" layer="46"/>
<wire x1="3.75" y1="-0.60116875" x2="3.75" y2="0.47246875" width="0" layer="46"/>
<wire x1="4.4" y1="-1.93836875" x2="-4.4" y2="-1.93836875" width="0" layer="46"/>
<wire x1="4.4" y1="0.65" x2="4.4" y2="-1.93836875" width="0" layer="46"/>
<wire x1="3.55126875" y1="-0.95" x2="3.75" y2="-0.60116875" width="0" layer="46"/>
<wire x1="3.929809375" y1="0.65" x2="3.87166875" y2="0.6363" width="0" layer="46"/>
<wire x1="3.87166875" y1="0.6363" x2="3.80841875" y2="0.59371875" width="0" layer="46"/>
<wire x1="3.80841875" y1="0.59371875" x2="3.76585" y2="0.530609375" width="0" layer="46"/>
<wire x1="3.76585" y1="0.530609375" x2="3.75" y2="0.47246875" width="0" layer="46"/>
<wire x1="-3.75" y1="0.47246875" x2="-3.76693125" y2="0.530609375" width="0" layer="46"/>
<wire x1="-3.76693125" y1="0.530609375" x2="-3.8095" y2="0.59371875" width="0" layer="46"/>
<wire x1="-3.8095" y1="0.59371875" x2="-3.87261875" y2="0.6363" width="0" layer="46"/>
<wire x1="-3.87261875" y1="0.6363" x2="-3.93078125" y2="0.65" width="0" layer="46"/>
<wire x1="-4.55" y1="-0.95" x2="6.75" y2="-0.95" width="0.127" layer="51"/>
<smd name="A1" x="2.75" y="0" dx="0.25" dy="1.5" layer="1"/>
<smd name="A2" x="2.25" y="0" dx="0.25" dy="1.5" layer="1"/>
<smd name="A3" x="1.75" y="0" dx="0.25" dy="1.5" layer="1"/>
<smd name="A4" x="1.25" y="0" dx="0.25" dy="1.5" layer="1"/>
<smd name="A5" x="0.75" y="0" dx="0.25" dy="1.5" layer="1"/>
<smd name="A6" x="0.25" y="0" dx="0.25" dy="1.5" layer="1"/>
<smd name="A7" x="-0.25" y="0" dx="0.25" dy="1.5" layer="1"/>
<smd name="A8" x="-0.75" y="0" dx="0.25" dy="1.5" layer="1"/>
<smd name="A9" x="-1.25" y="0" dx="0.25" dy="1.5" layer="1"/>
<smd name="A10" x="-1.75" y="0" dx="0.25" dy="1.5" layer="1"/>
<smd name="A11" x="-2.25" y="0" dx="0.25" dy="1.5" layer="1"/>
<smd name="A12" x="-2.75" y="0" dx="0.25" dy="1.5" layer="1"/>
<smd name="GND_1" x="-3.375" y="0" dx="0.5" dy="1.5" layer="16" rot="R180"/>
<smd name="B1" x="-2.75" y="0" dx="0.25" dy="1.5" layer="16"/>
<smd name="B2" x="-2.25" y="0" dx="0.25" dy="1.5" layer="16"/>
<smd name="B3" x="-1.75" y="0" dx="0.25" dy="1.5" layer="16"/>
<smd name="B4" x="-1.25" y="0" dx="0.25" dy="1.5" layer="16"/>
<smd name="B5" x="-0.75" y="0" dx="0.25" dy="1.5" layer="16"/>
<smd name="B6" x="-0.25" y="0" dx="0.25" dy="1.5" layer="16"/>
<smd name="B7" x="0.25" y="0" dx="0.25" dy="1.5" layer="16"/>
<smd name="B8" x="0.75" y="0" dx="0.25" dy="1.5" layer="16"/>
<smd name="B9" x="1.25" y="0" dx="0.25" dy="1.5" layer="16"/>
<smd name="B10" x="1.75" y="0" dx="0.25" dy="1.5" layer="16"/>
<smd name="B11" x="2.25" y="0" dx="0.25" dy="1.5" layer="16"/>
<smd name="B12" x="2.75" y="0" dx="0.25" dy="1.5" layer="16"/>
<smd name="GND_2" x="3.375" y="0" dx="0.5" dy="1.5" layer="1" rot="R180"/>
</package>
</packages>
<symbols>
<symbol name="10133475-10001LF">
<wire x1="-12.7" y1="15.24" x2="12.7" y2="15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="15.24" x2="12.7" y2="-17.78" width="0.254" layer="94"/>
<wire x1="12.7" y1="-17.78" x2="-12.7" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-17.78" x2="-12.7" y2="15.24" width="0.254" layer="94"/>
<text x="-12.7" y="16.002" size="1.778" layer="95">&gt;NAME</text>
<text x="-12.7" y="-18.542" size="1.778" layer="96" rot="MR180">&gt;VALUE</text>
<pin name="GND_A" x="-17.78" y="-10.16" length="middle" direction="pwr"/>
<pin name="SSTXN1" x="-17.78" y="7.62" length="middle"/>
<pin name="SSTXP1" x="-17.78" y="10.16" length="middle"/>
<pin name="VBUS_A" x="-17.78" y="12.7" length="middle" direction="pwr"/>
<pin name="DP1" x="-17.78" y="2.54" length="middle"/>
<pin name="CC1" x="-17.78" y="5.08" length="middle"/>
<pin name="SBU1" x="-17.78" y="-2.54" length="middle"/>
<pin name="DN1" x="-17.78" y="0" length="middle"/>
<pin name="SSRXN2" x="-17.78" y="-5.08" length="middle"/>
<pin name="SSRXP2" x="-17.78" y="-7.62" length="middle"/>
<pin name="SHIELD" x="-17.78" y="-15.24" length="middle" direction="pas"/>
<pin name="GND_B" x="17.78" y="-10.16" length="middle" direction="pwr" rot="R180"/>
<pin name="SSTXN2" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="SSTXP2" x="17.78" y="-7.62" length="middle" rot="R180"/>
<pin name="VBUS_B" x="17.78" y="12.7" length="middle" direction="pwr" rot="R180"/>
<pin name="DP2" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="CC2" x="17.78" y="-2.54" length="middle" rot="R180"/>
<pin name="SBU2" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="DN2" x="17.78" y="2.54" length="middle" rot="R180"/>
<pin name="SSRXN1" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="SSRXP1" x="17.78" y="10.16" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="10133475-10001LF" prefix="J">
<description> &lt;a href="https://pricing.snapeda.com/parts/10133475-10001LF/FCI/view-part?ref=eda"&gt;Check availability&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="10133475-10001LF" x="0" y="0"/>
</gates>
<devices>
<device name="" package="AMPHENOL_10133475-10001LF">
<connects>
<connect gate="G$1" pin="CC1" pad="A5"/>
<connect gate="G$1" pin="CC2" pad="B5"/>
<connect gate="G$1" pin="DN1" pad="A7"/>
<connect gate="G$1" pin="DN2" pad="B7"/>
<connect gate="G$1" pin="DP1" pad="A6"/>
<connect gate="G$1" pin="DP2" pad="B6"/>
<connect gate="G$1" pin="GND_A" pad="A1 A12"/>
<connect gate="G$1" pin="GND_B" pad="B1 B12"/>
<connect gate="G$1" pin="SBU1" pad="A8"/>
<connect gate="G$1" pin="SBU2" pad="B8"/>
<connect gate="G$1" pin="SHIELD" pad="GND_1 GND_2"/>
<connect gate="G$1" pin="SSRXN1" pad="B10"/>
<connect gate="G$1" pin="SSRXN2" pad="A10"/>
<connect gate="G$1" pin="SSRXP1" pad="B11"/>
<connect gate="G$1" pin="SSRXP2" pad="A11"/>
<connect gate="G$1" pin="SSTXN1" pad="A3"/>
<connect gate="G$1" pin="SSTXN2" pad="B3"/>
<connect gate="G$1" pin="SSTXP1" pad="A2"/>
<connect gate="G$1" pin="SSTXP2" pad="B2"/>
<connect gate="G$1" pin="VBUS_A" pad="A4 A9"/>
<connect gate="G$1" pin="VBUS_B" pad="B4 B9"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="In Stock"/>
<attribute name="DESCRIPTION" value="  USB-C (USB TYPE-C) USB 3.2 Gen 2 (USB 3.1 Gen 2, Superspeed + (USB 3.1)) Plug Connector 24 Position Board Edge, Straddle Mount "/>
<attribute name="MF" value="FCI"/>
<attribute name="MP" value="10133475-10001LF"/>
<attribute name="PACKAGE" value="None"/>
<attribute name="PRICE" value="None"/>
<attribute name="PURCHASE-URL" value="https://pricing.snapeda.com/search/part/10133475-10001LF/?ref=eda"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ams1117-3.3v">
<description>&lt;1A LOW DROPOUT VOLTAGE REGULATOR, SOT-223&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="SOT229P700X180-4N">
<description>&lt;b&gt;3 Lead SOT-223 Plastic Package&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-3.35" y="2.29" dx="1.3" dy="0.95" layer="1"/>
<smd name="2" x="-3.35" y="0" dx="1.3" dy="0.95" layer="1"/>
<smd name="3" x="-3.35" y="-2.29" dx="1.3" dy="0.95" layer="1"/>
<smd name="4" x="3.35" y="0" dx="3.25" dy="1.3" layer="1" rot="R90"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-4.25" y1="3.605" x2="4.25" y2="3.605" width="0.05" layer="51"/>
<wire x1="4.25" y1="3.605" x2="4.25" y2="-3.605" width="0.05" layer="51"/>
<wire x1="4.25" y1="-3.605" x2="-4.25" y2="-3.605" width="0.05" layer="51"/>
<wire x1="-4.25" y1="-3.605" x2="-4.25" y2="3.605" width="0.05" layer="51"/>
<wire x1="-1.752" y1="3.252" x2="1.752" y2="3.252" width="0.1" layer="51"/>
<wire x1="1.752" y1="3.252" x2="1.752" y2="-3.252" width="0.1" layer="51"/>
<wire x1="1.752" y1="-3.252" x2="-1.752" y2="-3.252" width="0.1" layer="51"/>
<wire x1="-1.752" y1="-3.252" x2="-1.752" y2="3.252" width="0.1" layer="51"/>
<wire x1="-1.752" y1="0.962" x2="0.538" y2="3.252" width="0.1" layer="51"/>
<wire x1="-1.752" y1="3.252" x2="1.752" y2="3.252" width="0.2" layer="21"/>
<wire x1="1.752" y1="3.252" x2="1.752" y2="-3.252" width="0.2" layer="21"/>
<wire x1="1.752" y1="-3.252" x2="-1.752" y2="-3.252" width="0.2" layer="21"/>
<wire x1="-1.752" y1="-3.252" x2="-1.752" y2="3.252" width="0.2" layer="21"/>
<wire x1="-4" y1="3.115" x2="-2.7" y2="3.115" width="0.2" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="AMS1117-3.3V">
<wire x1="5.08" y1="2.54" x2="45.72" y2="2.54" width="0.254" layer="94"/>
<wire x1="45.72" y1="-7.62" x2="45.72" y2="2.54" width="0.254" layer="94"/>
<wire x1="45.72" y1="-7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<text x="46.99" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="46.99" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="GROUND/ADJUST" x="0" y="0" length="middle"/>
<pin name="VOUT" x="0" y="-2.54" length="middle"/>
<pin name="VIN" x="0" y="-5.08" length="middle"/>
<pin name="TAB_(OUTPUT)" x="50.8" y="-2.54" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AMS1117-3.3V" prefix="IC">
<description>&lt;b&gt;1A LOW DROPOUT VOLTAGE REGULATOR, SOT-223&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.advanced-monolithic.com/pdf/ds1117.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="AMS1117-3.3V" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT229P700X180-4N">
<connects>
<connect gate="G$1" pin="GROUND/ADJUST" pad="1"/>
<connect gate="G$1" pin="TAB_(OUTPUT)" pad="4"/>
<connect gate="G$1" pin="VIN" pad="3"/>
<connect gate="G$1" pin="VOUT" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="1A LOW DROPOUT VOLTAGE REGULATOR, SOT-223" constant="no"/>
<attribute name="HEIGHT" value="1.8mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Advanced Monolithic Systems" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="ams1117-3.3v" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE-STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="CH340E">
<description>A library for CH340E</description>
<packages>
<package name="MSOP10" urn="urn:adsk.eagle:footprint:3661277/1">
<description>&lt;b&gt;10-Lead Mini Small Outline Package [MSOP]&lt;/b&gt; (RM-10)&lt;p&gt;
Source: http://www.analog.com/UploadedFiles/Data_Sheets/35641221898805SSM2167_b.pdf&lt;br&gt;
COMPLIANT TO JEDEC STANDARDS MO-187BA</description>
<wire x1="1.4" y1="1.4" x2="1.4" y2="-1.4" width="0.2032" layer="21"/>
<wire x1="1.4" y1="-1.4" x2="-1.4" y2="-1.4" width="0.2032" layer="21"/>
<wire x1="-1.4" y1="-1.4" x2="-1.4" y2="1.4" width="0.2032" layer="21"/>
<wire x1="-1.4" y1="1.4" x2="1.4" y2="1.4" width="0.2032" layer="21"/>
<wire x1="-1.1" y1="0.8" x2="-0.5" y2="0.8" width="0.2032" layer="21" curve="-180"/>
<wire x1="-0.5" y1="0.8" x2="-1.1" y2="0.8" width="0.2032" layer="21"/>
<smd name="1" x="-2.1131" y="1" dx="0.25" dy="1" layer="1" rot="R270"/>
<smd name="2" x="-2.1131" y="0.5" dx="0.25" dy="1" layer="1" rot="R270"/>
<smd name="3" x="-2.1131" y="0" dx="0.25" dy="1" layer="1" rot="R270"/>
<smd name="4" x="-2.1131" y="-0.5" dx="0.25" dy="1" layer="1" rot="R270"/>
<smd name="5" x="-2.1131" y="-1" dx="0.25" dy="1" layer="1" rot="R270"/>
<smd name="6" x="2.1131" y="-1" dx="0.25" dy="1" layer="1" rot="R90"/>
<smd name="7" x="2.1131" y="-0.5" dx="0.25" dy="1" layer="1" rot="R90"/>
<smd name="8" x="2.1131" y="0" dx="0.25" dy="1" layer="1" rot="R90"/>
<smd name="9" x="2.1131" y="0.5" dx="0.25" dy="1" layer="1" rot="R90"/>
<smd name="10" x="2.1131" y="1" dx="0.25" dy="1" layer="1" rot="R90"/>
<text x="-2.54" y="2.032" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<rectangle x1="-2.125" y1="0.4994" x2="-1.875" y2="1.4994" layer="51" rot="R270"/>
<rectangle x1="-2.125" y1="-0.0006" x2="-1.875" y2="0.9994" layer="51" rot="R270"/>
<rectangle x1="-2.125" y1="-0.5006" x2="-1.875" y2="0.4994" layer="51" rot="R270"/>
<rectangle x1="-2.125" y1="-1.0006" x2="-1.875" y2="-0.0006" layer="51" rot="R270"/>
<rectangle x1="-2.125" y1="-1.5006" x2="-1.875" y2="-0.5006" layer="51" rot="R270"/>
<rectangle x1="1.875" y1="-1.4994" x2="2.125" y2="-0.4994" layer="51" rot="R90"/>
<rectangle x1="1.875" y1="-0.9994" x2="2.125" y2="0.0006" layer="51" rot="R90"/>
<rectangle x1="1.875" y1="-0.4994" x2="2.125" y2="0.5006" layer="51" rot="R90"/>
<rectangle x1="1.875" y1="0.0006" x2="2.125" y2="1.0006" layer="51" rot="R90"/>
<rectangle x1="1.875" y1="0.5006" x2="2.125" y2="1.5006" layer="51" rot="R90"/>
</package>
</packages>
<packages3d>
<package3d name="MSOP10" urn="urn:adsk.eagle:package:3661278/1" type="box">
<description>&lt;b&gt;10-Lead Mini Small Outline Package [MSOP]&lt;/b&gt; (RM-10)&lt;p&gt;
Source: http://www.analog.com/UploadedFiles/Data_Sheets/35641221898805SSM2167_b.pdf&lt;br&gt;
COMPLIANT TO JEDEC STANDARDS MO-187BA</description>
<packageinstances>
<packageinstance name="MSOP10"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="CH340E">
<description>WCH.CN/CH340E 江苏沁恒股份有限公司 CH340E</description>
<pin name="UD+" x="-13.97" y="10.16" length="middle"/>
<pin name="UD-" x="-13.97" y="5.08" length="middle"/>
<pin name="THIS_GND" x="-13.97" y="0" length="middle" direction="pwr"/>
<pin name="RTS" x="-13.97" y="-5.08" length="middle" direction="out"/>
<pin name="CTS" x="-13.97" y="-10.16" length="middle" direction="in"/>
<pin name="TNOW" x="13.97" y="-10.16" length="middle" direction="out" rot="R180"/>
<pin name="VCC" x="13.97" y="-5.08" length="middle" direction="pwr" rot="R180"/>
<pin name="TX" x="13.97" y="0" length="middle" direction="out" rot="R180"/>
<pin name="RX" x="13.97" y="5.08" length="middle" direction="in" rot="R180"/>
<pin name="V3" x="13.97" y="10.16" length="middle" direction="pwr" rot="R180"/>
<wire x1="-8.89" y1="12.7" x2="-8.89" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-8.89" y1="-12.7" x2="8.89" y2="-12.7" width="0.254" layer="94"/>
<wire x1="8.89" y1="-12.7" x2="8.89" y2="12.7" width="0.254" layer="94"/>
<wire x1="8.89" y1="12.7" x2="-8.89" y2="12.7" width="0.254" layer="94"/>
<text x="-3.81" y="-15.24" size="1.778" layer="95">&gt;Name</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="CH340E" prefix="U">
<description>WCH.CN/CH340E

See DataSheet：
&lt;a href = "http://www.wch.cn/downloads/CH340DS1_PDF.html"&gt;wch.cn&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="CH340E" x="27.94" y="0"/>
</gates>
<devices>
<device name="SMD" package="MSOP10">
<connects>
<connect gate="G$1" pin="CTS" pad="5"/>
<connect gate="G$1" pin="RTS" pad="4"/>
<connect gate="G$1" pin="RX" pad="9"/>
<connect gate="G$1" pin="THIS_GND" pad="3"/>
<connect gate="G$1" pin="TNOW" pad="6"/>
<connect gate="G$1" pin="TX" pad="8"/>
<connect gate="G$1" pin="UD+" pad="1"/>
<connect gate="G$1" pin="UD-" pad="2"/>
<connect gate="G$1" pin="V3" pad="10"/>
<connect gate="G$1" pin="VCC" pad="7"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3661278/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="adafruit">
<packages>
<package name="SOT23-W">
<description>&lt;b&gt;SOT23&lt;/b&gt; - Wave soldering</description>
<wire x1="1.5724" y1="0.6604" x2="1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.5724" y1="-0.6604" x2="-1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="-0.6604" x2="-1.5724" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="0.6604" x2="1.5724" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="-0.3984" x2="-1.5724" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="1.5724" y1="0.6604" x2="1.5724" y2="-0.3984" width="0.1524" layer="21"/>
<wire x1="0.2954" y1="-0.6604" x2="-0.3094" y2="-0.6604" width="0.1524" layer="21"/>
<smd name="3" x="0" y="1.3" dx="2.8" dy="1.4" layer="1"/>
<smd name="2" x="1.1" y="-1.3" dx="1.2" dy="1.4" layer="1"/>
<smd name="1" x="-1.1" y="-1.3" dx="1.2" dy="1.4" layer="1"/>
<text x="2.032" y="0.254" size="0.4064" layer="25">&gt;NAME</text>
<text x="2.032" y="-0.508" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
<package name="SOT23">
<description>&lt;b&gt;SOT-23&lt;/b&gt;</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<smd name="3" x="0" y="1.1" dx="1" dy="1.4" layer="1"/>
<smd name="2" x="0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<smd name="1" x="-0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
<package name="TO220V">
<description>&lt;b&gt;TO 200 vertical&lt;/b&gt;</description>
<wire x1="5.08" y1="-1.143" x2="4.953" y2="-4.064" width="0.127" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="4.953" y2="-4.064" width="0.127" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="-4.699" y2="-4.318" width="0.127" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-4.699" y2="-4.318" width="0.127" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-5.08" y2="-1.143" width="0.127" layer="21"/>
<circle x="-4.4958" y="-3.7084" radius="0.254" width="0.127" layer="21"/>
<pad name="1" x="-2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="0" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<text x="-5.08" y="-6.0452" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-3.175" y="-3.175" size="1.27" layer="51" ratio="10">1</text>
<text x="-0.635" y="-3.175" size="1.27" layer="51" ratio="10">2</text>
<text x="1.905" y="-3.175" size="1.27" layer="51" ratio="10">3</text>
<rectangle x1="-5.334" y1="-0.762" x2="5.334" y2="0" layer="21"/>
<rectangle x1="-5.334" y1="-1.27" x2="-3.429" y2="-0.762" layer="21"/>
<rectangle x1="-1.651" y1="-1.27" x2="-0.889" y2="-0.762" layer="21"/>
<rectangle x1="-3.429" y1="-1.27" x2="-1.651" y2="-0.762" layer="51"/>
<rectangle x1="0.889" y1="-1.27" x2="1.651" y2="-0.762" layer="21"/>
<rectangle x1="3.429" y1="-1.27" x2="5.334" y2="-0.762" layer="21"/>
<rectangle x1="-0.889" y1="-1.27" x2="0.889" y2="-0.762" layer="51"/>
<rectangle x1="1.651" y1="-1.27" x2="3.429" y2="-0.762" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="MOSFET-N">
<wire x1="0.762" y1="0.762" x2="0.762" y2="0" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="0.762" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0.762" y1="3.175" x2="0.762" y2="2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.54" x2="0.762" y2="1.905" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-1.905" x2="0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="-2.54" x2="0.762" y2="-3.175" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0.762" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.508" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="2.54" x2="3.81" y2="2.54" width="0.1524" layer="94"/>
<wire x1="4.572" y1="0.762" x2="4.318" y2="0.508" width="0.1524" layer="94"/>
<wire x1="4.318" y1="0.508" x2="3.81" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.508" x2="3.302" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.302" y1="0.508" x2="3.048" y2="0.254" width="0.1524" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.3592" width="0" layer="94"/>
<circle x="2.54" y="2.54" radius="0.3592" width="0" layer="94"/>
<text x="5.08" y="0.635" size="1.27" layer="95">&gt;NAME</text>
<text x="5.08" y="-1.27" size="1.27" layer="96">&gt;VALUE</text>
<text x="3.175" y="3.175" size="0.8128" layer="93">D</text>
<text x="3.175" y="-3.81" size="0.8128" layer="93">S</text>
<text x="-1.27" y="-1.905" size="0.8128" layer="93">G</text>
<pin name="G" x="-2.54" y="-2.54" visible="pad" length="short"/>
<pin name="S" x="2.54" y="-5.08" visible="pad" length="short" rot="R90"/>
<pin name="D" x="2.54" y="5.08" visible="pad" length="short" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="3.81" y="0.508"/>
<vertex x="3.302" y="-0.254"/>
<vertex x="4.318" y="-0.254"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="1.016" y="0"/>
<vertex x="2.032" y="0.762"/>
<vertex x="2.032" y="-0.762"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="MOSFET-N" prefix="Q" uservalue="yes">
<description>&lt;b&gt;N-Channel Mosfet&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;2N7002E - 60V 260mA SOT23 [Digikey: 2N7002ET1GOSTR-ND] - &lt;b&gt;REEL&lt;/b&gt;&lt;/li&gt;
&lt;li&gt;BSH103 - 30V 850mA SOT23 [Digikey: 568-5013-1-ND]&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="MOSFET-N" x="-2.54" y="0"/>
</gates>
<devices>
<device name="WAVE" package="SOT23-W">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="REFLOW" package="SOT23">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="GDS_TO220V" package="TO220V">
<connects>
<connect gate="G$1" pin="D" pad="2"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="suitch-lcd-14-0.5">
<packages>
<package name="OLED_8PIN">
<smd name="_LEDA" x="1.3" y="0.2" dx="2.54" dy="0.3" layer="1"/>
<smd name="_GND" x="1.3" y="0.65" dx="2.54" dy="0.3" layer="1"/>
<smd name="_RESET" x="1.3" y="1.1" dx="2.54" dy="0.3" layer="1"/>
<smd name="_RS" x="1.3" y="1.55" dx="2.54" dy="0.3" layer="1"/>
<smd name="_SDA" x="1.3" y="2" dx="2.54" dy="0.3" layer="1"/>
<smd name="_SCL" x="1.3" y="2.45" dx="2.54" dy="0.3" layer="1"/>
<smd name="_VDD" x="1.3" y="2.9" dx="2.54" dy="0.3" layer="1"/>
<smd name="_CS" x="1.3" y="3.35" dx="2.54" dy="0.3" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="OLED_8PIN">
<pin name="LED_A" x="0" y="10.16" length="middle"/>
<pin name="GND" x="0" y="5.08" length="middle"/>
<pin name="RESET" x="0" y="0" length="middle"/>
<pin name="RS" x="0" y="-5.08" length="middle"/>
<pin name="SDA" x="0" y="-10.16" length="middle"/>
<pin name="SCL" x="0" y="-15.24" length="middle"/>
<pin name="VDD" x="0" y="-20.32" length="middle"/>
<pin name="CS" x="0" y="-25.4" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="OLED_8PIN">
<gates>
<gate name="G$1" symbol="OLED_8PIN" x="-2.54" y="7.62"/>
</gates>
<devices>
<device name="" package="OLED_8PIN">
<connects>
<connect gate="G$1" pin="CS" pad="_CS"/>
<connect gate="G$1" pin="GND" pad="_GND"/>
<connect gate="G$1" pin="LED_A" pad="_LEDA"/>
<connect gate="G$1" pin="RESET" pad="_RESET"/>
<connect gate="G$1" pin="RS" pad="_RS"/>
<connect gate="G$1" pin="SCL" pad="_SCL"/>
<connect gate="G$1" pin="SDA" pad="_SDA"/>
<connect gate="G$1" pin="VDD" pad="_VDD"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="XPT2046">
<packages>
<package name="TSSOP-16">
<wire x1="-2.5146" y1="-2.2828" x2="2.5146" y2="-2.2828" width="0.1" layer="21"/>
<wire x1="2.5146" y1="2.2828" x2="2.5146" y2="-2.2828" width="0.1" layer="21"/>
<wire x1="2.5146" y1="2.2828" x2="-2.5146" y2="2.2828" width="0.1" layer="21"/>
<wire x1="-2.5146" y1="-2.2828" x2="-2.5146" y2="2.2828" width="0.1" layer="21"/>
<text x="-3" y="-2.25" size="1" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-4.25" y="-2.25" size="1" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.3766" y1="-3.121" x2="-2.1734" y2="-2.2828" layer="51"/>
<rectangle x1="-1.7266" y1="-3.121" x2="-1.5234" y2="-2.2828" layer="51"/>
<rectangle x1="-1.0766" y1="-3.121" x2="-0.8734" y2="-2.2828" layer="51"/>
<rectangle x1="-0.4266" y1="-3.121" x2="-0.2234" y2="-2.2828" layer="51"/>
<rectangle x1="0.2234" y1="-3.121" x2="0.4266" y2="-2.2828" layer="51"/>
<rectangle x1="0.8734" y1="-3.121" x2="1.0766" y2="-2.2828" layer="51"/>
<rectangle x1="1.5234" y1="-3.121" x2="1.7266" y2="-2.2828" layer="51"/>
<rectangle x1="2.1734" y1="-3.121" x2="2.3766" y2="-2.2828" layer="51"/>
<rectangle x1="2.1734" y1="2.2828" x2="2.3766" y2="3.121" layer="51"/>
<rectangle x1="1.5234" y1="2.2828" x2="1.7266" y2="3.121" layer="51"/>
<rectangle x1="0.8734" y1="2.2828" x2="1.0766" y2="3.121" layer="51"/>
<rectangle x1="0.2234" y1="2.2828" x2="0.4266" y2="3.121" layer="51"/>
<rectangle x1="-0.4266" y1="2.2828" x2="-0.2234" y2="3.121" layer="51"/>
<rectangle x1="-1.0766" y1="2.2828" x2="-0.8734" y2="3.121" layer="51"/>
<rectangle x1="-1.7266" y1="2.2828" x2="-1.5234" y2="3.121" layer="51"/>
<rectangle x1="-2.3766" y1="2.2828" x2="-2.1734" y2="3.121" layer="51"/>
<wire x1="-2.75" y1="3.75" x2="2.75" y2="3.75" width="0.0508" layer="39"/>
<wire x1="2.75" y1="3.75" x2="2.75" y2="-3.75" width="0.0508" layer="39"/>
<wire x1="2.75" y1="-3.75" x2="-2.75" y2="-3.75" width="0.0508" layer="39"/>
<wire x1="-2.75" y1="-3.75" x2="-2.75" y2="3.75" width="0.0508" layer="39"/>
<polygon width="0.1" layer="21">
<vertex x="-3" y="-2.75"/>
<vertex x="-3" y="-3.25"/>
<vertex x="-2.75" y="-3"/>
</polygon>
<polygon width="0.1" layer="51">
<vertex x="-3" y="-2.75"/>
<vertex x="-3" y="-3.25"/>
<vertex x="-2.75" y="-3"/>
</polygon>
<wire x1="-2.5" y1="-2" x2="2.5" y2="-2" width="0.1" layer="21"/>
<wire x1="-2.5" y1="-2" x2="2.5" y2="-2" width="0.1" layer="51"/>
<wire x1="-2.5" y1="-2.25" x2="2.5" y2="-2.25" width="0.1" layer="51"/>
<wire x1="2.5" y1="-2.25" x2="2.5" y2="2.25" width="0.1" layer="51"/>
<wire x1="2.5" y1="2.25" x2="-2.5" y2="2.25" width="0.1" layer="51"/>
<wire x1="-2.5" y1="2.25" x2="-2.5" y2="-2.25" width="0.1" layer="51"/>
<smd name="1" x="-2.275" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="2" x="-1.625" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="3" x="-0.975" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="4" x="-0.325" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="5" x="0.325" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="6" x="0.975" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="7" x="1.625" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="8" x="2.275" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="9" x="2.275" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="10" x="1.625" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="11" x="0.975" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="12" x="0.325" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="13" x="-0.325" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="14" x="-0.975" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="15" x="-1.625" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="16" x="-2.275" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="XPT2046">
<wire x1="-12.7" y1="12.7" x2="-12.7" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-12.7" x2="12.7" y2="-12.7" width="0.254" layer="94"/>
<wire x1="12.7" y1="-12.7" x2="12.7" y2="12.7" width="0.254" layer="94"/>
<wire x1="12.7" y1="12.7" x2="-12.7" y2="12.7" width="0.254" layer="94"/>
<text x="-3.175" y="-6.985" size="1" layer="95">&gt;NAME</text>
<text x="-3.175" y="-10.16" size="1" layer="96">&gt;VALUE</text>
<pin name="VCC" x="-15.24" y="10.16" length="short" direction="pwr"/>
<pin name="XP" x="-15.24" y="-2.54" length="short" direction="in"/>
<pin name="YP" x="-15.24" y="-5.08" length="short" direction="in"/>
<pin name="XN" x="-15.24" y="-7.62" length="short" direction="in"/>
<pin name="YN" x="-15.24" y="-10.16" length="short" direction="in"/>
<pin name="GND" x="15.24" y="-10.16" length="short" direction="pwr" rot="R180"/>
<pin name="VBAT" x="-15.24" y="7.62" length="short" direction="in"/>
<pin name="AUX" x="15.24" y="-5.08" length="short" direction="in" rot="R180"/>
<pin name="VREF" x="-15.24" y="5.08" length="short" direction="in"/>
<pin name="IOVDD" x="-15.24" y="2.54" length="short" direction="pwr"/>
<pin name="/PENIRQ" x="15.24" y="-2.54" length="short" direction="out" function="dot" rot="R180"/>
<pin name="DOUT" x="15.24" y="2.54" length="short" direction="out" rot="R180"/>
<pin name="BUSY" x="15.24" y="0" length="short" direction="out" rot="R180"/>
<pin name="DIN" x="15.24" y="5.08" length="short" direction="in" rot="R180"/>
<pin name="/CS" x="15.24" y="7.62" length="short" direction="in" function="dot" rot="R180"/>
<pin name="DCLK" x="15.24" y="10.16" length="short" direction="in" function="clk" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="XPT2046" prefix="U">
<description>&lt;h2&gt;XPT2046 touch screen controller&lt;/h2&gt;

&lt;h3&gt;General Description&lt;/h3&gt;
&lt;p&gt;
The XPT2046 is a 4-wire resistive touch screen controller that incorporates a 12-bit 125 kHz sampling SAR type
A/D converter.
The XPT2046 operates down to 2.2V supply voltage and supports digital I/O interface voltage from 1.5V to VCC
in order to connect low voltage uP.
The XPT2046 can detect the pressed screen location by performing two A/D conversions. In addition to location,
the XPT2046 also measures touch screen pressure.On-chip VREF can be utilized for analog auxiliary input,
temperature measurement and battery monitoring withthe ability to measure voltage from 0V to 5V.
The XPT2046 also has an on-chip temperature sensor
The XPT2046 is available in 16pin QFN thin package(0.75mm in height) and has the operating temperature range
of -40°C to +85°C
&lt;/p&gt;
&lt;h3&gt;Features&lt;/h3&gt;
&lt;p&gt;
12 bit SAR type A/D converter with S/H circuit
&lt;br&gt;Low voltage operation (VCC = 2.2V ∼ 3.6V)
&lt;br&gt;Low voltage digital I/F (1.5V ∼ VCC)
&lt;br&gt;4-wire I/F
&lt;br&gt;Sampling frequency: 125 kHz (max)
&lt;br&gt;On-Chip voltage reference (2.5V)
&lt;br&gt;Pen pressure measurement
&lt;br&gt;On-chip thermo sensor
&lt;br&gt;Direct battery masurement
&lt;br&gt;Low power consumption (260μA)
&lt;br&gt;Package 16pin QFN
&lt;/p&gt;  &lt;a href="https://pricing.snapeda.com/parts/XPT2046/Xptek/view-part?ref=eda"&gt;Check availability&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="XPT2046" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TSSOP-16">
<connects>
<connect gate="G$1" pin="/CS" pad="15"/>
<connect gate="G$1" pin="/PENIRQ" pad="11"/>
<connect gate="G$1" pin="AUX" pad="8"/>
<connect gate="G$1" pin="BUSY" pad="13"/>
<connect gate="G$1" pin="DCLK" pad="16"/>
<connect gate="G$1" pin="DIN" pad="14"/>
<connect gate="G$1" pin="DOUT" pad="12"/>
<connect gate="G$1" pin="GND" pad="6"/>
<connect gate="G$1" pin="IOVDD" pad="10"/>
<connect gate="G$1" pin="VBAT" pad="7"/>
<connect gate="G$1" pin="VCC" pad="1"/>
<connect gate="G$1" pin="VREF" pad="9"/>
<connect gate="G$1" pin="XN" pad="4"/>
<connect gate="G$1" pin="XP" pad="2"/>
<connect gate="G$1" pin="YN" pad="5"/>
<connect gate="G$1" pin="YP" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Not in stock"/>
<attribute name="DESCRIPTION" value=" RASPBERRY PI 3.5 TFT DISPLAY "/>
<attribute name="MF" value="Xptek"/>
<attribute name="MP" value="XPT2046"/>
<attribute name="PACKAGE" value="Package "/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="RHD2000_eval_board">
<description>Intan Technologies RHD2000 Eval Board</description>
<packages>
<package name="0201">
<wire x1="-0.254" y1="0.1524" x2="0.254" y2="0.1524" width="0.1016" layer="51"/>
<wire x1="0.254" y1="-0.1524" x2="-0.254" y2="-0.1524" width="0.1016" layer="51"/>
<wire x1="-0.8128" y1="0.4064" x2="0.8128" y2="0.4064" width="0.127" layer="21"/>
<wire x1="0.8128" y1="0.4064" x2="0.8128" y2="-0.4064" width="0.127" layer="21"/>
<wire x1="0.8128" y1="-0.4064" x2="-0.8128" y2="-0.4064" width="0.127" layer="21"/>
<wire x1="-0.8128" y1="-0.4064" x2="-0.8128" y2="0.4064" width="0.127" layer="21"/>
<smd name="1" x="-0.3937" y="0" dx="0.4318" dy="0.4318" layer="1"/>
<smd name="2" x="0.3937" y="0" dx="0.4318" dy="0.4318" layer="1"/>
<text x="-1.0668" y="0.5588" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1.1938" y="-1.5748" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.3048" y1="-0.1524" x2="-0.1524" y2="0.1524" layer="51"/>
<rectangle x1="0.1524" y1="-0.1524" x2="0.3048" y2="0.1524" layer="51"/>
</package>
<package name="0402">
<wire x1="-0.4572" y1="0.254" x2="0.4572" y2="0.254" width="0.1016" layer="51"/>
<wire x1="0.4572" y1="-0.254" x2="-0.4572" y2="-0.254" width="0.1016" layer="51"/>
<wire x1="-1.0668" y1="0.5588" x2="1.0668" y2="0.5588" width="0.127" layer="21"/>
<wire x1="1.0668" y1="0.5588" x2="1.0668" y2="-0.5588" width="0.127" layer="21"/>
<wire x1="1.0668" y1="-0.5588" x2="-1.0668" y2="-0.5588" width="0.127" layer="21"/>
<wire x1="-1.0668" y1="-0.5588" x2="-1.0668" y2="0.5588" width="0.127" layer="21"/>
<smd name="1" x="-0.5334" y="0" dx="0.6858" dy="0.762" layer="1"/>
<smd name="2" x="0.5334" y="0" dx="0.6858" dy="0.762" layer="1"/>
<text x="-1.143" y="0.762" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-1.778" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.508" y1="-0.254" x2="-0.254" y2="0.254" layer="51"/>
<rectangle x1="0.254" y1="-0.254" x2="0.508" y2="0.254" layer="51"/>
</package>
<package name="0603">
<wire x1="-0.762" y1="0.4064" x2="0.762" y2="0.4064" width="0.1016" layer="51"/>
<wire x1="0.762" y1="-0.4064" x2="-0.762" y2="-0.4064" width="0.1016" layer="51"/>
<wire x1="-1.524" y1="0.762" x2="1.524" y2="0.762" width="0.127" layer="21"/>
<wire x1="1.524" y1="0.762" x2="1.524" y2="-0.762" width="0.127" layer="21"/>
<wire x1="1.524" y1="-0.762" x2="-1.524" y2="-0.762" width="0.127" layer="21"/>
<wire x1="-1.524" y1="-0.762" x2="-1.524" y2="0.762" width="0.127" layer="21"/>
<smd name="1" x="-0.8636" y="0" dx="0.889" dy="1.0668" layer="1"/>
<smd name="2" x="0.8636" y="0" dx="0.889" dy="1.0668" layer="1"/>
<text x="-1.778" y="0" size="0.762" layer="25" font="vector" ratio="10" align="center-right">&gt;NAME</text>
<text x="1.778" y="0" size="0.762" layer="27" font="vector" ratio="10" align="center-left">&gt;VALUE</text>
<rectangle x1="-0.8128" y1="-0.4064" x2="-0.508" y2="0.4064" layer="51"/>
<rectangle x1="0.508" y1="-0.4064" x2="0.8128" y2="0.4064" layer="51"/>
</package>
<package name="0805">
<wire x1="-0.9652" y1="0.635" x2="0.9652" y2="0.635" width="0.1016" layer="51"/>
<wire x1="0.9652" y1="-0.635" x2="-0.9652" y2="-0.635" width="0.1016" layer="51"/>
<wire x1="-1.778" y1="1.016" x2="1.778" y2="1.016" width="0.127" layer="21"/>
<wire x1="1.778" y1="1.016" x2="1.778" y2="-1.016" width="0.127" layer="21"/>
<wire x1="1.778" y1="-1.016" x2="-1.778" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-1.778" y1="-1.016" x2="-1.778" y2="1.016" width="0.127" layer="21"/>
<smd name="1" x="-1.0414" y="0" dx="0.9652" dy="1.524" layer="1"/>
<smd name="2" x="1.0414" y="0" dx="0.9652" dy="1.524" layer="1"/>
<text x="-1.778" y="1.27" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-2.032" y="-2.286" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-1.016" y1="-0.635" x2="-0.6096" y2="0.635" layer="51"/>
<rectangle x1="0.6096" y1="-0.635" x2="1.016" y2="0.635" layer="51"/>
</package>
<package name="1206">
<wire x1="-1.5494" y1="0.8128" x2="1.5494" y2="0.8128" width="0.1016" layer="51"/>
<wire x1="1.5494" y1="-0.8128" x2="-1.5494" y2="-0.8128" width="0.1016" layer="51"/>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.127" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.2192" dy="1.8796" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.2192" dy="1.8796" layer="1"/>
<text x="-2.54" y="1.524" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-1.6002" y1="-0.8128" x2="-1.0922" y2="0.8128" layer="51"/>
<rectangle x1="1.0922" y1="-0.8128" x2="1.6002" y2="0.8128" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="C">
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="C" prefix="C" uservalue="yes">
<description>Capacitor</description>
<gates>
<gate name="C" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="_0201" package="0201">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0402" package="0402">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0603" package="0603">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0805" package="0805">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_1206" package="1206">
<connects>
<connect gate="C" pin="1" pad="1"/>
<connect gate="C" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U1" library="esp32_s3" deviceset="ESP32-S3-WROOM-1-N8" device="" override_package3d_urn="urn:adsk.eagle:package:38381603/2" override_package_urn="urn:adsk.eagle:footprint:38381604/1"/>
<part name="J1" library="usb-c connector" deviceset="10133475-10001LF" device="" override_package3d_urn="urn:adsk.eagle:package:38398809/2" override_package_urn="urn:adsk.eagle:footprint:38398810/1"/>
<part name="IC1" library="ams1117-3.3v" deviceset="AMS1117-3.3V" device="" override_package3d_urn="urn:adsk.eagle:package:38406889/2" override_package_urn="urn:adsk.eagle:footprint:38406890/1"/>
<part name="U2" library="CH340E" deviceset="CH340E" device="SMD" package3d_urn="urn:adsk.eagle:package:3661278/1" override_package3d_urn="urn:adsk.eagle:package:38419454/2" override_package_urn="urn:adsk.eagle:footprint:38419455/1"/>
<part name="U3" library="CH340E" deviceset="CH340E" device="SMD" package3d_urn="urn:adsk.eagle:package:3661278/1" override_package3d_urn="urn:adsk.eagle:package:38419406/2" override_package_urn="urn:adsk.eagle:footprint:38419407/1"/>
<part name="U4" library="CH340E" deviceset="CH340E" device="SMD" package3d_urn="urn:adsk.eagle:package:3661278/1" override_package3d_urn="urn:adsk.eagle:package:38419459/2" override_package_urn="urn:adsk.eagle:footprint:38419460/1"/>
<part name="U5" library="CH340E" deviceset="CH340E" device="SMD" package3d_urn="urn:adsk.eagle:package:3661278/1" override_package3d_urn="urn:adsk.eagle:package:38419459/2" override_package_urn="urn:adsk.eagle:footprint:38419460/1"/>
<part name="Q1" library="adafruit" deviceset="MOSFET-N" device="REFLOW"/>
<part name="U$1" library="suitch-lcd-14-0.5" deviceset="OLED_8PIN" device=""/>
<part name="U6" library="XPT2046" deviceset="XPT2046" device=""/>
<part name="C2" library="RHD2000_eval_board" deviceset="C" device="_0402"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="-5.08" y="-139.7" size="1.778" layer="91"> * Serial COM / hardware original
 * gpio.43 Used for USB/Serial U0TXD
 * gpio.44 Used for USB Serial U0RXD
 * 
 * Serial1 COM1 / hardware
 * RX1 15
 * TX1 16
 * 
 * Serial2 COM2 / hardware
 * RX2 19 
 * TX2 20
 * 
 * Serial3 COM3 / virtualized by software
 * 
 * Serial4 COM4 / USB native
 * gpio.19 Used for native USB D-
 * gpio.20 Used for native USB D+</text>
</plain>
<instances>
<instance part="U1" gate="A" x="33.02" y="73.66" smashed="yes">
<attribute name="NAME" x="48.6156" y="82.7786" size="2.0828" layer="95" ratio="6" rot="SR0"/>
<attribute name="VALUE" x="47.9806" y="80.2386" size="2.0828" layer="96" ratio="6" rot="SR0"/>
</instance>
<instance part="J1" gate="G$1" x="114.3" y="50.8" smashed="yes">
<attribute name="NAME" x="101.6" y="66.802" size="1.778" layer="95"/>
<attribute name="VALUE" x="101.6" y="32.258" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="IC1" gate="G$1" x="170.18" y="93.98" smashed="yes">
<attribute name="NAME" x="217.17" y="101.6" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="217.17" y="99.06" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="U2" gate="G$1" x="-15.24" y="-58.42" smashed="yes">
<attribute name="NAME" x="-19.05" y="-73.66" size="1.778" layer="95"/>
</instance>
<instance part="U3" gate="G$1" x="40.64" y="-58.42" smashed="yes">
<attribute name="NAME" x="36.83" y="-73.66" size="1.778" layer="95"/>
</instance>
<instance part="U4" gate="G$1" x="99.06" y="-58.42" smashed="yes">
<attribute name="NAME" x="95.25" y="-73.66" size="1.778" layer="95"/>
</instance>
<instance part="U5" gate="G$1" x="165.1" y="-58.42" smashed="yes">
<attribute name="NAME" x="161.29" y="-73.66" size="1.778" layer="95"/>
</instance>
<instance part="Q1" gate="G$1" x="83.82" y="-10.16" smashed="yes">
<attribute name="NAME" x="88.9" y="-9.525" size="1.27" layer="95"/>
<attribute name="VALUE" x="88.9" y="-11.43" size="1.27" layer="96"/>
</instance>
<instance part="U$1" gate="G$1" x="223.52" y="43.18" smashed="yes"/>
<instance part="U6" gate="G$1" x="330.2" y="-63.5" smashed="yes">
<attribute name="NAME" x="327.025" y="-70.485" size="1" layer="95"/>
<attribute name="VALUE" x="327.025" y="-73.66" size="1" layer="96"/>
</instance>
<instance part="C2" gate="C" x="-10.16" y="-81.28" smashed="yes">
<attribute name="NAME" x="-8.636" y="-80.899" size="1.778" layer="95"/>
<attribute name="VALUE" x="-8.636" y="-85.979" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="GROUND/ADJUST"/>
<wire x1="170.18" y1="93.98" x2="167.64" y2="93.98" width="0.1524" layer="91"/>
<label x="165.1" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="GND_A"/>
<pinref part="J1" gate="G$1" pin="GND_B"/>
<wire x1="96.52" y1="40.64" x2="132.08" y2="40.64" width="0.1524" layer="91"/>
<wire x1="132.08" y1="40.64" x2="132.08" y2="38.1" width="0.1524" layer="91"/>
<junction x="132.08" y="40.64"/>
<label x="132.08" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q1" gate="G$1" pin="S"/>
<wire x1="86.36" y1="-15.24" x2="86.36" y2="-17.78" width="0.1524" layer="91"/>
<label x="86.36" y="-17.78" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="GND"/>
<wire x1="345.44" y1="-73.66" x2="347.98" y2="-73.66" width="0.1524" layer="91"/>
<label x="350.52" y="-73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C2" gate="C" pin="2"/>
<wire x1="-10.16" y1="-86.36" x2="-10.16" y2="-88.9" width="0.1524" layer="91"/>
<label x="-10.16" y="-88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="VIN"/>
<wire x1="170.18" y1="88.9" x2="167.64" y2="88.9" width="0.1524" layer="91"/>
<label x="165.1" y="88.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="VBUS_A"/>
<pinref part="J1" gate="G$1" pin="VBUS_B"/>
<wire x1="96.52" y1="63.5" x2="132.08" y2="63.5" width="0.1524" layer="91"/>
<wire x1="132.08" y1="63.5" x2="134.62" y2="63.5" width="0.1524" layer="91"/>
<junction x="132.08" y="63.5"/>
<label x="134.62" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="VCC33" class="0">
<segment>
<pinref part="U1" gate="A" pin="3V3"/>
<wire x1="35.56" y1="71.12" x2="30.48" y2="71.12" width="0.1524" layer="91"/>
<label x="27.94" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="VOUT"/>
<wire x1="170.18" y1="91.44" x2="167.64" y2="91.44" width="0.1524" layer="91"/>
<label x="160.02" y="91.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="VCC"/>
<wire x1="-1.27" y1="-63.5" x2="2.54" y2="-63.5" width="0.1524" layer="91"/>
<label x="2.54" y="-78.74" size="1.778" layer="95"/>
<wire x1="2.54" y1="-63.5" x2="2.54" y2="-78.74" width="0.1524" layer="91"/>
<pinref part="C2" gate="C" pin="1"/>
<wire x1="2.54" y1="-78.74" x2="-10.16" y2="-78.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="VCC"/>
<wire x1="54.61" y1="-63.5" x2="55.88" y2="-63.5" width="0.1524" layer="91"/>
<label x="53.34" y="-63.5" size="1.778" layer="95"/>
<wire x1="63.5" y1="-63.5" x2="54.61" y2="-63.5" width="0.1524" layer="91"/>
<junction x="54.61" y="-63.5"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="VCC"/>
<wire x1="113.03" y1="-63.5" x2="114.3" y2="-63.5" width="0.1524" layer="91"/>
<label x="106.68" y="-63.5" size="1.778" layer="95"/>
<wire x1="119.38" y1="-63.5" x2="113.03" y2="-63.5" width="0.1524" layer="91"/>
<junction x="113.03" y="-63.5"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="VCC"/>
<wire x1="179.07" y1="-63.5" x2="182.88" y2="-63.5" width="0.1524" layer="91"/>
<label x="182.88" y="-63.5" size="1.778" layer="95"/>
<wire x1="195.58" y1="-63.5" x2="179.07" y2="-63.5" width="0.1524" layer="91"/>
<junction x="179.07" y="-63.5"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="VCC"/>
<wire x1="314.96" y1="-53.34" x2="312.42" y2="-53.34" width="0.1524" layer="91"/>
<label x="307.34" y="-53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="D-" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="DN1"/>
<wire x1="96.52" y1="50.8" x2="114.3" y2="50.8" width="0.1524" layer="91"/>
<wire x1="114.3" y1="50.8" x2="114.3" y2="53.34" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="DN2"/>
<wire x1="114.3" y1="53.34" x2="132.08" y2="53.34" width="0.1524" layer="91"/>
<wire x1="132.08" y1="53.34" x2="137.16" y2="53.34" width="0.1524" layer="91"/>
<junction x="132.08" y="53.34"/>
<label x="137.16" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="IO19"/>
<wire x1="35.56" y1="43.18" x2="33.02" y2="43.18" width="0.1524" layer="91"/>
<label x="30.48" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="UD-"/>
<wire x1="-29.21" y1="-53.34" x2="-38.1" y2="-53.34" width="0.1524" layer="91"/>
<label x="-40.64" y="-53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="UD-"/>
<wire x1="26.67" y1="-53.34" x2="25.4" y2="-53.34" width="0.1524" layer="91"/>
<label x="22.86" y="-53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="UD-"/>
<wire x1="85.09" y1="-53.34" x2="83.82" y2="-53.34" width="0.1524" layer="91"/>
<label x="88.9" y="-53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="UD-"/>
<wire x1="151.13" y1="-53.34" x2="147.32" y2="-53.34" width="0.1524" layer="91"/>
<label x="144.78" y="-53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="D+" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="DP1"/>
<wire x1="96.52" y1="53.34" x2="111.76" y2="53.34" width="0.1524" layer="91"/>
<wire x1="111.76" y1="53.34" x2="111.76" y2="48.26" width="0.1524" layer="91"/>
<wire x1="111.76" y1="48.26" x2="119.38" y2="48.26" width="0.1524" layer="91"/>
<wire x1="119.38" y1="48.26" x2="119.38" y2="50.8" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="DP2"/>
<wire x1="119.38" y1="50.8" x2="132.08" y2="50.8" width="0.1524" layer="91"/>
<wire x1="96.52" y1="53.34" x2="93.98" y2="53.34" width="0.1524" layer="91"/>
<junction x="96.52" y="53.34"/>
<label x="91.44" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="IO20"/>
<wire x1="35.56" y1="40.64" x2="33.02" y2="40.64" width="0.1524" layer="91"/>
<label x="30.48" y="40.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="UD+"/>
<wire x1="-29.21" y1="-48.26" x2="-38.1" y2="-48.26" width="0.1524" layer="91"/>
<label x="-40.64" y="-48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="UD+"/>
<wire x1="26.67" y1="-48.26" x2="25.4" y2="-48.26" width="0.1524" layer="91"/>
<label x="25.4" y="-48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="UD+"/>
<wire x1="85.09" y1="-48.26" x2="83.82" y2="-48.26" width="0.1524" layer="91"/>
<label x="83.82" y="-45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="UD+"/>
<wire x1="151.13" y1="-48.26" x2="147.32" y2="-48.26" width="0.1524" layer="91"/>
<label x="144.78" y="-48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="COM1_TX" class="0">
<segment>
<pinref part="U1" gate="A" pin="IO4"/>
<wire x1="35.56" y1="66.04" x2="33.02" y2="66.04" width="0.1524" layer="91"/>
<label x="22.86" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="RX"/>
<wire x1="-1.27" y1="-53.34" x2="0" y2="-53.34" width="0.1524" layer="91"/>
<label x="-7.62" y="-53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="COM1_RX" class="0">
<segment>
<pinref part="U1" gate="A" pin="IO5"/>
<wire x1="35.56" y1="63.5" x2="33.02" y2="63.5" width="0.1524" layer="91"/>
<label x="22.86" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="TX"/>
<wire x1="-1.27" y1="-58.42" x2="0" y2="-58.42" width="0.1524" layer="91"/>
<label x="-7.62" y="-58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="COM1_RST" class="0">
<segment>
<pinref part="U1" gate="A" pin="IO6"/>
<wire x1="35.56" y1="60.96" x2="33.02" y2="60.96" width="0.1524" layer="91"/>
<label x="22.86" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="RTS"/>
<wire x1="-29.21" y1="-63.5" x2="-30.48" y2="-63.5" width="0.1524" layer="91"/>
<label x="-40.64" y="-63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="COM1_DTR" class="0">
<segment>
<pinref part="U1" gate="A" pin="IO7"/>
<wire x1="35.56" y1="58.42" x2="33.02" y2="58.42" width="0.1524" layer="91"/>
<label x="22.86" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="CTS"/>
<wire x1="-29.21" y1="-68.58" x2="-30.48" y2="-68.58" width="0.1524" layer="91"/>
<label x="-40.64" y="-68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="COM2_RX" class="0">
<segment>
<pinref part="U1" gate="A" pin="IO18"/>
<wire x1="35.56" y1="48.26" x2="33.02" y2="48.26" width="0.1524" layer="91"/>
<label x="22.86" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="TX"/>
<wire x1="54.61" y1="-58.42" x2="55.88" y2="-58.42" width="0.1524" layer="91"/>
<label x="53.34" y="-58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="COM2_TX" class="0">
<segment>
<pinref part="U1" gate="A" pin="IO17"/>
<wire x1="35.56" y1="50.8" x2="33.02" y2="50.8" width="0.1524" layer="91"/>
<label x="22.86" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="RX"/>
<wire x1="54.61" y1="-53.34" x2="55.88" y2="-53.34" width="0.1524" layer="91"/>
<label x="50.8" y="-53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="COM2_DTR" class="0">
<segment>
<pinref part="U1" gate="A" pin="IO16"/>
<wire x1="35.56" y1="53.34" x2="33.02" y2="53.34" width="0.1524" layer="91"/>
<label x="22.86" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="CTS"/>
<wire x1="26.67" y1="-68.58" x2="25.4" y2="-68.58" width="0.1524" layer="91"/>
<label x="20.32" y="-68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="COM2_RST" class="0">
<segment>
<pinref part="U1" gate="A" pin="IO15"/>
<wire x1="35.56" y1="55.88" x2="33.02" y2="55.88" width="0.1524" layer="91"/>
<label x="22.86" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="RTS"/>
<wire x1="26.67" y1="-63.5" x2="25.4" y2="-63.5" width="0.1524" layer="91"/>
<label x="20.32" y="-63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="COM3_RX" class="0">
<segment>
<pinref part="U1" gate="A" pin="IO8"/>
<wire x1="35.56" y1="45.72" x2="33.02" y2="45.72" width="0.1524" layer="91"/>
<label x="22.86" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="TX"/>
<wire x1="113.03" y1="-58.42" x2="114.3" y2="-58.42" width="0.1524" layer="91"/>
<label x="109.22" y="-58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="COM3_TX" class="0">
<segment>
<pinref part="U1" gate="A" pin="IO3"/>
<wire x1="35.56" y1="38.1" x2="33.02" y2="38.1" width="0.1524" layer="91"/>
<label x="22.86" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="RX"/>
<wire x1="113.03" y1="-53.34" x2="114.3" y2="-53.34" width="0.1524" layer="91"/>
<label x="109.22" y="-53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<wire x1="158.75" y1="-63.5" x2="160.02" y2="-63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SPI_SS" class="0">
<segment>
<pinref part="U1" gate="A" pin="IO42"/>
<wire x1="71.12" y1="58.42" x2="73.66" y2="58.42" width="0.1524" layer="91"/>
<label x="73.66" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI_MOSI" class="0">
<segment>
<pinref part="U1" gate="A" pin="IO35"/>
<wire x1="71.12" y1="40.64" x2="73.66" y2="40.64" width="0.1524" layer="91"/>
<label x="73.66" y="40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI_SCK" class="0">
<segment>
<pinref part="U1" gate="A" pin="IO36"/>
<wire x1="71.12" y1="43.18" x2="73.66" y2="43.18" width="0.1524" layer="91"/>
<label x="73.66" y="43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI_MISO" class="0">
<segment>
<pinref part="U1" gate="A" pin="IO37"/>
<wire x1="71.12" y1="45.72" x2="73.66" y2="45.72" width="0.1524" layer="91"/>
<label x="73.66" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C_SDA" class="0">
<segment>
<pinref part="U1" gate="A" pin="IO45"/>
<wire x1="71.12" y1="35.56" x2="73.66" y2="35.56" width="0.1524" layer="91"/>
<label x="73.66" y="35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C_SCL" class="0">
<segment>
<pinref part="U1" gate="A" pin="IO46"/>
<wire x1="35.56" y1="35.56" x2="33.02" y2="35.56" width="0.1524" layer="91"/>
<label x="27.94" y="35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="COM3_RST" class="0">
<segment>
<pinref part="U1" gate="A" pin="IO9"/>
<wire x1="35.56" y1="33.02" x2="33.02" y2="33.02" width="0.1524" layer="91"/>
<label x="22.86" y="33.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="RTS"/>
<wire x1="85.09" y1="-63.5" x2="83.82" y2="-63.5" width="0.1524" layer="91"/>
<label x="83.82" y="-63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="COM3_DTR" class="0">
<segment>
<pinref part="U1" gate="A" pin="IO10"/>
<wire x1="35.56" y1="30.48" x2="33.02" y2="30.48" width="0.1524" layer="91"/>
<label x="20.32" y="30.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="CTS"/>
<wire x1="85.09" y1="-68.58" x2="83.82" y2="-68.58" width="0.1524" layer="91"/>
<label x="83.82" y="-68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="COM4_TX" class="0">
<segment>
<pinref part="U1" gate="A" pin="IO11"/>
<wire x1="35.56" y1="27.94" x2="33.02" y2="27.94" width="0.1524" layer="91"/>
<label x="22.86" y="27.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="RX"/>
<wire x1="179.07" y1="-53.34" x2="180.34" y2="-53.34" width="0.1524" layer="91"/>
<label x="180.34" y="-53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="COM4_RX" class="0">
<segment>
<pinref part="U1" gate="A" pin="IO12"/>
<wire x1="35.56" y1="25.4" x2="33.02" y2="25.4" width="0.1524" layer="91"/>
<label x="22.86" y="25.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="TX"/>
<wire x1="179.07" y1="-58.42" x2="180.34" y2="-58.42" width="0.1524" layer="91"/>
<label x="180.34" y="-58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="COM4_RST" class="0">
<segment>
<pinref part="U1" gate="A" pin="IO13"/>
<wire x1="71.12" y1="22.86" x2="73.66" y2="22.86" width="0.1524" layer="91"/>
<label x="73.66" y="22.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="RTS"/>
<wire x1="151.13" y1="-63.5" x2="147.32" y2="-63.5" width="0.1524" layer="91"/>
<label x="144.78" y="-63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="COM4_DTR" class="0">
<segment>
<pinref part="U1" gate="A" pin="IO14"/>
<wire x1="71.12" y1="25.4" x2="73.66" y2="25.4" width="0.1524" layer="91"/>
<label x="73.66" y="25.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="CTS"/>
<wire x1="151.13" y1="-68.58" x2="147.32" y2="-68.58" width="0.1524" layer="91"/>
<label x="144.78" y="-68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="ON_OFF_PORTS" class="0">
<segment>
<pinref part="U1" gate="A" pin="IO21"/>
<wire x1="71.12" y1="27.94" x2="73.66" y2="27.94" width="0.1524" layer="91"/>
<label x="73.66" y="27.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q1" gate="G$1" pin="G"/>
<wire x1="81.28" y1="-12.7" x2="78.74" y2="-12.7" width="0.1524" layer="91"/>
<label x="63.5" y="-12.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART_VCC" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="D"/>
<wire x1="86.36" y1="-5.08" x2="86.36" y2="-2.54" width="0.1524" layer="91"/>
<label x="86.36" y="-2.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="THIS_GND"/>
<wire x1="151.13" y1="-58.42" x2="144.78" y2="-58.42" width="0.1524" layer="91"/>
<label x="137.16" y="-58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="THIS_GND"/>
<wire x1="85.09" y1="-58.42" x2="83.82" y2="-58.42" width="0.1524" layer="91"/>
<label x="73.66" y="-58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="THIS_GND"/>
<wire x1="26.67" y1="-58.42" x2="22.86" y2="-58.42" width="0.1524" layer="91"/>
<label x="15.24" y="-58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="THIS_GND"/>
<wire x1="-29.21" y1="-58.42" x2="-35.56" y2="-58.42" width="0.1524" layer="91"/>
<label x="-45.72" y="-58.42" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
<note version="9.4" severity="warning">
Since Version 9.4, EAGLE supports the overriding of 3D packages
in schematics and board files. Those overridden 3d packages
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
